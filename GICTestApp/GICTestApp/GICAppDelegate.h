//
//  GICAppDelegate.h
//  GICTestApp
//
//  Created by denn on 3/21/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GIStompChannelDispatcher.h"

@class GICTableViewController;

@interface GICAppDelegate : UIResponder <UIApplicationDelegate, GIStompEventDispatchHandler, GIStompErrorDispatchHandler>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) GICTableViewController *viewController;
@property(readonly) GIStompChannelDispatcher *dispatcher;
@property(readonly) GIConfig *config;

+ (GICAppDelegate*) sharedInstance;

@end
