//
//  GICAppDelegate.m
//  GICTestApp
//
//  Created by denn on 3/21/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GICAppDelegate.h"

@interface GICAppDelegate()
@property GIStompChannelDispatcher *dispatcher;
@property GIConfig *config;
@end

GICAppDelegate *__shared_instance = nil;

@implementation GICAppDelegate

+ (GICAppDelegate*) sharedInstance{
    return __shared_instance;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"didFinishLaunchingWithOptions");
    
    self.config = [[GIConfig alloc] initWithUrl:@"https://goinvest2.beta.micex.ru"];
    self.config.login = [[GIAuth alloc] initWithUser:@"vasya@superbroker" andPassword:@"12345"];
    
//    self.config = [[GIConfig alloc] initWithUrl:@"https://goinvest.micex.ru"];
//    self.config.login = [[GIAuth alloc] initWithUser:@"MXSE.MU0000600009" andPassword:@""];    


    self.config.validatesSecureCertificate = NO;

    self.dispatcher = [[GIStompChannelDispatcher alloc] initWithConfig:self.config];
    
    __shared_instance = self;
    
    self.dispatcher.eventHandler = self;
    self.dispatcher.errorHandler = self;
    
    return YES;
}

- (void) channelWillActive{
    NSLog(@"Channel will active....");
}

- (void) channelClosed{
    NSLog(@"Closed.....");
}

- (void) channelOpened{
    NSLog(@"Opened");
}


- (void) channelConnectionRestored{
    NSLog(@"Channel connection restored.");
}

- (void) channelReconnecting{
    NSLog(@"Channel reconnecting... ");
}

- (void) didStompError:(GIError *)error{
    NSLog(@"Stomp: %@", error);
}

- (void) didSystemError:(GIError *)error{
    NSLog(@"System: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog(@"Will inactive...");
    [self.dispatcher stop];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    NSLog(@"Background...");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"Will active...");
    [self.dispatcher start];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    NSLog(@"Exiting...");
    
}

- (void) applicationDidFinishLaunching:(UIApplication *)application{
    NSLog(@"Launched");
}

@end
