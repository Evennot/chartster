//
//  GILastPriceView.h
//  GICTestApp
//
//  Created by denn on 3/27/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GIStompChannelDispatcher.h"

@interface GILastPriceView : UITableView <GIStompDispatcherProtocol, UITableViewDataSource, UITableViewDelegate>

- (void) setDispatcher:(GIStompChannelDispatcher*)aDispatcher;
- (void) subscribeTicker: (NSString*)ticker;

@end
