//
//  GILastPriceView.m
//  GICTestApp
//
//  Created by denn on 3/27/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GILastPriceView.h"

@interface GILastPriceView()
@property NSString *ticker, *tickerName, *tickerCaption, *tickerBoardName;
@property NSString *market;
@end

@implementation GILastPriceView
{
    GIStompChannelDispatcher *dispatcher;
    GIStompSecurity        *securityFrame;
    NSDictionary             *securitiesUpdates;
    NSString                 *lastBackLight;
    NSInteger                updatesCount;
    NSIndexPath              *backLightPath;
    NSIndexPath              *securityBoardPath;
    NSIndexPath              *securityPricePath;
    
    NSNumber  *lastChange;
    NSString  *lastChangeStr;
    NSString  *lastPrice;
    NSString  *lastHigh;
    NSString  *lastLow;
    NSAttributedString  *lastValToday;
    NSAttributedString  *lastChangeStrAttr;

    UIColor *upTrendColor;
    UIColor *downTrendColor;
    
    NSNumber *bidDepth;
    NSNumber *askDepth;
    NSString *bidDepthStr;
    NSString *askDepthStr;
    
    UIImage *bidProgressImg;
    UIImage *askProgressImg;
    UIImage *trackImg;
    
}


- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    NSLog(@"INIT PRICE VIEW by CODER");
    
    
    self.delegate = self;
    self.dataSource = self;
    
    lastBackLight = @"backLightWhite";
    updatesCount  = 0;
    lastChangeStr = @"";
    
    backLightPath = [NSIndexPath indexPathForRow:0 inSection:0];
    securityBoardPath =[NSIndexPath indexPathForRow:1 inSection:0];
    securityPricePath =[NSIndexPath indexPathForRow:2 inSection:0];

    
    upTrendColor = [UIColor colorWithRed:.1 green:0.5 blue:0 alpha:1 ];
    downTrendColor = [UIColor colorWithRed:.5 green:0.1 blue:0 alpha:1 ];

    
    bidProgressImg = [[UIImage imageNamed:@"bidProgressImg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 10, 10, 3)];
    askProgressImg = [[UIImage imageNamed:@"askProgressImg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 10, 10, 3)];
    trackImg = [[UIImage imageNamed:@"trackImg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 10, 3, 10)];

    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) setDispatcher:(GIStompChannelDispatcher *)aDispatcher{
    dispatcher = aDispatcher;
}

- (void) subscribeTicker:(NSString *)ticker{
    
    self.ticker = ticker;
    self.market = [self.ticker componentsSeparatedByString:@"."][0];
    self.tickerName = [self.ticker componentsSeparatedByString:@"."][2];
    
    securityFrame = [GIStompSecurity select:self.ticker];
    
    [dispatcher registerMessage:securityFrame];
    
    securityFrame.delegate = self;
    
}



- (void) updateLable:(UILabel *)label byValue:(NSString*)value{
    if(value)
        label.text = value;
}


- (NSAttributedString *) valToday:(NSNumber*) val {
    
    if (!val) {
        return nil;
    }
    
    float _f = val.floatValue;    
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];    
    [numberFormatter setMaximumFractionDigits:3];
    [numberFormatter setMinimumFractionDigits:3];
    
    NSString *_ff;
    NSMutableAttributedString *_abv = [NSMutableAttributedString alloc];
    
    if (_f/1000000000.0>100.0) {
        _f/=1000000000.0;
        _abv = [_abv initWithString:@"billion"];
    }
    else if (_f/1000000.0>100.0){
        _f/=1000000.0;
        _abv = [_abv initWithString:@"million"];
    }
    else if (_f/1000.0>100.0){
        _f/=1000.0;   
        _abv = [_abv initWithString:@"thousand"];
    }
    else return nil;
        
    _ff = [NSString stringWithFormat:@"%@ ", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:_f]]];

    [_abv addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:9] range:NSMakeRange(0, _abv.length)];
    [_abv addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, _abv.length)];
    NSMutableAttributedString *_ret = [[NSMutableAttributedString alloc] initWithString:_ff];
    
    [_ret appendAttributedString:_abv];
    
    return _ret;
}


- (NSAttributedString*) coloredPrice:(NSString*)newPrice withOldText: (NSAttributedString*)text{
    if (!lastPrice){
        if (newPrice){
            lastPrice=newPrice;
            return [[NSAttributedString alloc] initWithString:newPrice];
        }
        else{
            return text;
        }
    }
    else if(!newPrice)
        return text;
    
    
    NSNumberFormatter *_f = [[NSNumberFormatter alloc] init];
    NSNumber *_last = [_f numberFromString:lastPrice];
    NSNumber *_current = [_f numberFromString:newPrice];
    
    if (_current.floatValue==_last.floatValue) {
        return text;
    }
    
    UIColor *_trendColor= _current.floatValue>_last.floatValue?upTrendColor:downTrendColor;
    
    NSUInteger from = 0;
    for (; from<[newPrice length] ; from++) {
        if ([newPrice characterAtIndex:from]!=[lastPrice characterAtIndex:from]) {
            break;
        }
    }

    NSMutableAttributedString *_ret = [[NSMutableAttributedString alloc] initWithString: newPrice];
    [_ret addAttribute:NSForegroundColorAttributeName value: _trendColor range:NSMakeRange(from,[_ret length]-from)];
    
    lastPrice=newPrice;
    return _ret;
}

//
// Draw CLIP
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *_change = [securityFrame.lastResponse.body numberForColumn:@"CHANGE" whereKey:@"TICKER" isEqualToString:self.ticker];
    lastChange = _change==nil?lastChange:_change;
        
    if (indexPath.row==0){
        
        if (lastChange.floatValue >0)
            lastBackLight = @"backLightGreen";

        else if (lastChange.floatValue<0)
            lastBackLight = @"backLightRed";
        else
            lastBackLight = @"backLightWhite";
                
        return  [tableView dequeueReusableCellWithIdentifier:lastBackLight forIndexPath:indexPath];
    }
    else if (indexPath.row==1){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"securityTitle" forIndexPath:indexPath];
        
        UILabel *_sharesName   = (UILabel*)[cell viewWithTag:1];
        UILabel *_sharesTicker = (UILabel*)[cell viewWithTag:2];
                
        if (!self.tickerCaption) {
            self.tickerCaption = securitiesUpdates[@"CAPTION"];
        }
        
        [self updateLable:_sharesName byValue:self.tickerCaption];
        [self updateLable:_sharesTicker byValue:self.tickerName];

        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lastPriceCell" forIndexPath:indexPath];
    
    UILabel *price    = (UILabel*) [cell viewWithTag:1];
    UILabel *change   = (UILabel*) [cell viewWithTag:2];
    
    UIProgressView *bidDepthBar    = (UIProgressView*) [cell viewWithTag:103];

    float  _fp_=0;
    if(bidDepth && askDepth && bidDepth.floatValue){
        _fp_ =  bidDepth.floatValue/(bidDepth.floatValue+askDepth.floatValue) ;

        [bidDepthBar setProgressImage:bidProgressImg];
        [bidDepthBar setTrackImage:askProgressImg];
        [bidDepthBar setProgress: _fp_ animated:NO];
    }
    
    UILabel *valtoday = (UILabel*) [cell viewWithTag:5];
    NSNumber *_lastPrice = [securityFrame.lastResponse.body numberForColumn:@"LAST" whereKey:@"TICKER" isEqualToString:self.ticker];
    
    
    if (_lastPrice && _lastPrice.floatValue!=0) {
        float _changePrcnt = lastChange.floatValue/_lastPrice.floatValue*100.0;
        NSString *_sign = @"";
        if (lastChange.floatValue>0)
            _sign=@"+";
        lastChangeStr = [NSString stringWithFormat:@"%@%@ (%@%.2f%%)", _sign, securitiesUpdates[@"CHANGE"], _sign, _changePrcnt]; //securitiesUpdates[@"CHANGE"];
    }
    
    if(_lastPrice){
        lastChangeStrAttr = price.attributedText = [self coloredPrice:securitiesUpdates[@"LAST"] withOldText:price.attributedText];
    }
    else
        price.attributedText = lastChangeStrAttr;
    
    UIColor *_trendColor = [UIColor blackColor] ;
    
    if(lastChange.floatValue>0)
        _trendColor = [UIColor colorWithRed:.1 green:0.5 blue:0 alpha:1 ];
    else if (lastChange.floatValue<0)
        _trendColor = [UIColor colorWithRed:.5 green:0.1 blue:0 alpha:1 ];
    
    NSMutableAttributedString *_ch = [[NSMutableAttributedString alloc] initWithString: lastChangeStr];
    
    [_ch addAttribute:NSForegroundColorAttributeName value: _trendColor range:NSMakeRange(0,[_ch length])];

    change.attributedText = _ch;
        
    NSAttributedString *_lv = [self valToday:[securityFrame.lastResponse.body valueForColumn:@"VALTODAY" whereKey:@"TICKER" isEqualToString:self.ticker]];

    if (_lv)
        lastValToday = _lv;

    valtoday.attributedText = lastValToday;
    
    return cell;
}

//
// Draw header
//
- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{ return 1;}

//
// Draw rows
//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (float) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (float) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0)
        return 0;
    else if (indexPath.row==1)
        return 30;
    else
        return 81;
}


- (void) didFrameReceive:(id)frame{
    dispatch_async(dispatch_get_main_queue(), ^(void){
                
        GIStompSecurity *os = frame;
        
        NSLog(@" ---- secs count: %i props=%@", [os.responseQueue size], os.lastResponse.body.properties);
        
        for (GICommonStompFrame *s = [os.responseQueue pop]; s ; s=[os.responseQueue pop]) {                                        
                                    
            if ([frame isKindOfClass:[GIStompSecurity class]]){
                
                securitiesUpdates=[securityFrame getRowByTicker:self.ticker];
                
                bidDepth = [s.body numberForColumn:@"BIDDEPTHT" whereKey:@"TICKER" isEqualToString:self.ticker];
                askDepth = [s.body numberForColumn:@"OFFERDEPTHT" whereKey:@"TICKER" isEqualToString:self.ticker];
                
                if (securitiesUpdates) {
                                        
                    NSMutableArray *_paths_to_update = [NSMutableArray arrayWithCapacity:3];
                    
                    [_paths_to_update addObject:backLightPath];
                    [_paths_to_update addObject:securityPricePath];
                    [_paths_to_update addObject:securityBoardPath];
                    
                    [self beginUpdates];
                    [self reloadRowsAtIndexPaths:_paths_to_update withRowAnimation:UITableViewRowAnimationNone];
                    [self endUpdates];

                    
                    updatesCount++;
                }
            }
        }
    });
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}


@end
