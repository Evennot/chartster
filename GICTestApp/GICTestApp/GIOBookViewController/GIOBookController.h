//
//  GIOBookController.h
//  GICTestApp
//
//  Created by denn on 3/27/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GIOBookView.h"
#import "GIStompChannelDispatcher.h"
#import "GILastPriceView.h"

@interface GIOBookController : UIViewController

@property (weak, nonatomic) IBOutlet GIOBookView *orderBooksView;
@property (weak, nonatomic) IBOutlet GILastPriceView *lastPriceView;

@end
