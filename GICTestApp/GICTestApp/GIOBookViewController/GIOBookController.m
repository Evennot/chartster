//
//  GIOBookController.m
//  GICTestApp
//
//  Created by denn on 3/27/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GIOBookController.h"
#import "GICAppDelegate.h"

@interface GIOBookController ()
@property NSString *ticker;
@end

@implementation GIOBookController
{
    GIStompChannelDispatcher *_dispatcher;
    UIViewController *src;
    UIViewController *dst;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.ticker = @"MXSE.EQNE.GAZP";
//    self.ticker = @"MXSE.EQNL.FEES";
//    self.ticker = @"MXSE.EQBR.LKOH";
    
    _dispatcher = [[GICAppDelegate sharedInstance] dispatcher];
    
    //NSLog(@"OOOORDER: %@", self);

    [self.lastPriceView setDispatcher: _dispatcher];
    [self.lastPriceView subscribeTicker:self.ticker];
    
    //usleep(1000);
    
    [self.orderBooksView setDispatcher: _dispatcher];
    [self.orderBooksView subscribeTicker:self.ticker];
    
    NSLog(@"View Load:%@ %@", self, self.ticker);
    
//    self.navigationController.navigationBar. =self.ticker;
    
}


- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
}

- (BOOL) shouldAutorotate{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
