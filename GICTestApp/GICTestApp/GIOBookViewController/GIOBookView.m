//
//  GIOBookView.m
//  GICTestApp
//
//  Created by denn on 3/27/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GIOBookView.h"

@interface GIOBookView ()
@property NSString *ticker;
@property NSString *market;
@end

@implementation GIOBookView
{
    GIStompOrderbook         *orderbooksFrame;
    GIStompChannelDispatcher *dispatcher;

    UIImage                  *bidProgressImg;
    UIImage                  *trackImg;
    UIImage                  *askProgressImg;
    
    NSTimer *scrollToSplitTimer;
    NSInteger  lastSplitPosition;
    NSUInteger updateCounter;
}


- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    NSLog(@"INIT VIEW by CODER");

    [GIStompOrderbook setDepth:0];
    
    self.delegate = self;
    self.dataSource = self;
    
    bidProgressImg = [[UIImage imageNamed:@"bidProgressImg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 10, 10, 3)];
    trackImg = [[UIImage imageNamed:@"trackImg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 10, 3, 10)];
    askProgressImg = [[UIImage imageNamed:@"askProgressImg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 10, 10, 3)];
    
    lastSplitPosition = 0;
    updateCounter = 0;
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
        
    return self;
}


- (void) setDispatcher:(GIStompChannelDispatcher *)aDispatcher{
    dispatcher = aDispatcher;
}

- (void) subscribeTicker:(NSString *)ticker{
    
    self.ticker = ticker;
    self.market = [self.ticker componentsSeparatedByString:@"."][0];
    
    orderbooksFrame = [GIStompOrderbook select:self.ticker];
    
    [dispatcher registerMessage:orderbooksFrame];
    
    orderbooksFrame.delegate = self;
    
}

- (void) updateLable:(UILabel *)label byValue:(NSString*)value{
    if(value)
        label.text = value;
}


//
// What is the cell type?
//
- (UITableViewCell*) bidAsk: (UITableView*) tableView path: (NSIndexPath*) indexPath withCellID: (NSString*) cellID invertProgress: (BOOL) doInvert index: (NSInteger) index{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    UIProgressView  *pv   = (UIProgressView*)[cell viewWithTag:3];
    
    if (doInvert) {
        [pv setProgressImage:bidProgressImg];
    }else{
        [pv setProgressImage:askProgressImg];
        pv.transform = CGAffineTransformMakeRotation(-M_PI);

    }
    [pv setTrackImage:trackImg];
    
    NSNumber *max =  [[orderbooksFrame.lastResponse.body maxValueForColumn:@"QUANTITY"] copy];
    NSNumber *q  =  [orderbooksFrame.lastResponse.body numberForIndex:index andKey:@"QUANTITY"];
    
    if (max.floatValue>0.0) {
        float ratio = log10f(q.floatValue*1.01)/log10f(max.floatValue*1.01);
        if (ratio>=0)
            [pv setProgress:ratio animated:YES];
    }
    
    // Configure the cell...
    
    UILabel *price = (UILabel*)[cell viewWithTag:1];
    UILabel *qty   = (UILabel*)[cell viewWithTag:2];
    
    price.text = [orderbooksFrame.lastResponse.body stringForIndex:index andKey:@"PRICE"];
    
    @try {
        if (q)
            qty.text = [q stringValue];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %@ %s:%i", exception, q, __FILE__, __LINE__);
    }
    
    return cell;
}


- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    NSLog(@"SCROLL START....");
    if (scrollToSplitTimer) {
        [scrollToSplitTimer invalidate];
    }
}

- (void) scrollToSplitPosition: (NSTimer*) timer{
    [self scrollToSplitPosition];
}


- (void) scrollToSplitPosition{
    [self scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastSplitPosition inSection:0]
                atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"SCROLL ENDED....");    
    if (!lastSplitPosition) return;
    [self scrollToSplitPositionAfterDelay];
}


- (void) scrollToSplitPositionAfterDelay{
    scrollToSplitTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollToSplitPosition:) userInfo:nil repeats:NO];
}

//
// Draw CLIP
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
//    if (indexPath.row==0) {
//        return [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath:indexPath];
//    }
    
    NSInteger _orderIndex = indexPath.row;
        
    BOOL isBid = [[orderbooksFrame.lastResponse.body stringForIndex:_orderIndex andKey:@"BUYSELL"] isEqualToString:@"B"];
    BOOL nextIsSell = NO;
    BOOL prevIsBid  = NO;
    
    @try {
        nextIsSell = [[orderbooksFrame.lastResponse.body stringForIndex:_orderIndex+1 andKey:@"BUYSELL"] isEqualToString:@"S"];
    }
    @catch (NSException *exception) {}
    @try {
        prevIsBid = [[orderbooksFrame.lastResponse.body stringForIndex:_orderIndex-1 andKey:@"BUYSELL"] isEqualToString:@"B"];
    }
    @catch (NSException *exception) {}
    
    NSString *bidName=@"bidCell";
    NSString *askName=@"askCell";
    if ((isBid && nextIsSell) || (prevIsBid && !isBid)) {
        bidName = @"bestBidCell";
        askName = @"bestAskCell";
    }
    
    NSString *cellID = isBid? bidName : askName;
    
    return [self bidAsk:tableView path:indexPath withCellID:cellID invertProgress:isBid index:_orderIndex];
}

//
// Draw header
//
- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{    return 1;}

//
// Draw rows
//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [orderbooksFrame.lastResponse.body countOfRows];
}

- (float) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (float) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 31;
}


- (void) didFrameReceive:(id)frame{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        if ([frame isKindOfClass:[GIStompOrderbook class]]) {
            GIStompOrderbook *ob = frame;
            if ([ob.lastResponse.body countOfRows]) {
                lastSplitPosition = ob.splitPosition;
                [self reloadData];
                
                if (updateCounter==0)
                [self scrollToSplitPosition];
                
                updateCounter++;

                [ob.responseQueue pop];
            }
        }
    });
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
