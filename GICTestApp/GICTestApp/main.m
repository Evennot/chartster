
//
//  main.m
//  GICTestApp
//
//  Created by denn on 3/21/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GICAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GICAppDelegate class]));
    }
}
