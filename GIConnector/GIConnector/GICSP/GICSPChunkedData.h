//
//  GICSPChunkedData.h
//  GIConnector
//
//  Created by denn on 3/20/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GICSPError.h"

@interface GICSPChunkedData : NSObject

@property(readonly) NSInteger currentSeqNum;

- (id) init;

//
// Append data to chain
//
- (void)     appendData: (NSData*)aData error:(GICSPError **) error;

//
// Get assembed data
//
- (NSData*) assembledData;

//
//
//
- (NSInteger) length;

//
//
//
- (NSInteger) count;

@end
