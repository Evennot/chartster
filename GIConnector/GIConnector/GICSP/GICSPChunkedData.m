//
//  GICSPChunkedData.m
//  GIConnector
//
//  Created by denn on 3/20/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GICSPChunkedData.h"
#import "GICSPStreamHeader.h"


//
// Chunked Raw data
//
@interface _GINSMutableData : NSObject
//
//data ssequential number
//
@property NSInteger seqNum, chunkDefinedLength;

//
// if the chunk is downloaded completely
//
@property BOOL      isCompleted;
@end

@implementation _GINSMutableData
{
    NSMutableData *data;
}

@synthesize seqNum, chunkDefinedLength, isCompleted;

- (id) initWithData:(NSData *)aData{
    
    if (!(self=[super init])) {
        return nil;
    }
    
    data = [NSMutableData dataWithData:aData];
        
    isCompleted = YES;
    seqNum = -1;
    chunkDefinedLength = 0;
    
    return self;
}

- (void) appendData: (NSData*)aData{
    [data appendData:aData];
}

- (NSInteger) length{
    return data.length;
}

- (NSData*) getData{
    return data;
}

- (NSString*) description{
    NSString *_s = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSUTF8StringEncoding];
    return [NSString stringWithFormat:@"Is Completed: %i\nSeqNUM: %i\nDefined length:%i\nReal length: %i\n----\n%@\n----\n", isCompleted, seqNum, chunkDefinedLength, [data length], _s];
}

@end


//
// Data
//
@interface GICSPChunkedData ()
@property NSInteger        currentSeqNum;
@end

@implementation GICSPChunkedData
{
    NSInteger        capacity;
    NSMutableArray  *dataChain;
}


- (NSString *) description{
    NSMutableString *_s = [[NSMutableString alloc] init];
    for (_GINSMutableData *d in dataChain) {
        [_s appendFormat:@"%@\n", d];
    }
    return _s;
}

- (NSInteger) length{
    NSInteger l=0;
    for (_GINSMutableData *d in dataChain) {
        l+=d.length;
    }
    return l;
}


- (NSInteger) count{
    return [dataChain count];
}

- (id) init{
    
    if(!(self=[super init]))
        return nil;
        
    self.currentSeqNum = -1;
    capacity = 0;
    dataChain = nil;
    
    return self;
}

- (void) appendNewChunk:(NSData *)aData from:(NSInteger)position withLength:(NSInteger) length seqnum:(NSInteger)seqnum  andCompletedFlag:(BOOL)isComleted andDefinedLength: (NSInteger) definedLength{

    _GINSMutableData *_chunk = [[_GINSMutableData alloc] initWithData:[aData subdataWithRange:NSMakeRange(position, length)]];
    
    _chunk.seqNum = seqnum;
    _chunk.isCompleted = isComleted;
    _chunk.chunkDefinedLength = definedLength;

    if(isComleted)
        //
        // current completed seqnum
        //
        self.currentSeqNum = seqnum;
    
    [dataChain addObject:_chunk];
}

//
// Append data to chain
//
- (void)     appendData: (NSData*)aData error:(GICSPError *__autoreleasing *)error{
    
    if (!aData || [aData length]==0) {
        return;
    }
    
    if ([aData length]<=sizeof(CSPStreamPacketHeader)) {
        if (error)
            *error =[GICSPError errorWithCode: GICSP_ERROR_SHORT_CSP_PACKET
                               andDescription:[NSString stringWithFormat:
                                               GILocalizedString(@"Connection problem has occurred. Too short packet received.", @"")]];
    }
    
    GICSPStreamHeader *_sheader = [[GICSPStreamHeader alloc] initWithData:aData];
    
    
    //NSLog(@"Header :::: %@", _sheader);
    
    if(!dataChain)
        dataChain = [NSMutableArray arrayWithCapacity:0];
    
    if(error)
        *error = nil;
    
    if(_sheader.header_size!=GICSP_HEADER_SIZE){
        //
        // data is not completed
        // add to previous chunk and make new
        //

        if([dataChain count]==0){
            if (error) {
                *error =[GICSPError errorWithCode: GICSP_ERROR_INCOMPLETED_CSP_PACKET
                                   andDescription:[NSString stringWithFormat:
                                                   GILocalizedString(@"Check your network connection. The incompleted packet has occurred.", @"")]];
            }
            return;
        }
        
        //
        // get the last chunk
        //
        _GINSMutableData *_last_incomleted_chunk = [dataChain lastObject];

        NSInteger _we_need_bytes   = _last_incomleted_chunk.chunkDefinedLength - [_last_incomleted_chunk length];
        NSInteger _chunk_diff_size = [aData length] - _we_need_bytes;

        if(_chunk_diff_size<=0){
            // just add
            
            //NSLog(@"1. add [::::%i]", [aData length]);
            
            [_last_incomleted_chunk appendData:aData];
            
            _last_incomleted_chunk.isCompleted = YES;
            self.currentSeqNum = _last_incomleted_chunk.seqNum;
            
            return;
        }
        else if (_chunk_diff_size>0){
            //
            // length of data > then we need, and we need (defined) - (what we have)
            //
            
            // just add the previous part to chunk 

            //NSLog(@"2. add [::::%i]", _we_need_bytes);
            [_last_incomleted_chunk appendData:[aData subdataWithRange:NSMakeRange(0, _we_need_bytes)]];
            _last_incomleted_chunk.isCompleted=YES;
            self.currentSeqNum = _last_incomleted_chunk.seqNum;
            
            //
            // next 
            //
            //NSLog(@"3. add [%i::::%i]", _we_need_bytes, _chunk_diff_size);
            NSMutableData *_nextData = [NSMutableData dataWithData:[aData subdataWithRange:NSMakeRange(_we_need_bytes, _chunk_diff_size)]];
            [self appendData:_nextData error:error];
            return;
        }
        
    }
    else{
        
        if(self.currentSeqNum>0 && self.currentSeqNum>=_sheader.seqnum)
        // we have those chunks
            return;
        
        NSInteger _chunk_diff_size = ([aData length] - (sizeof(CSPStreamPacketHeader) + _sheader.length));
        
        if (_chunk_diff_size == 0) {
            
            // completed chunk
            //NSLog(@"1. new [%li:%lli]", sizeof(CSPStreamPacketHeader), _sheader.length);
            [self appendNewChunk:aData from:sizeof(CSPStreamPacketHeader)  withLength:_sheader.length seqnum:_sheader.seqnum andCompletedFlag: YES andDefinedLength:_sheader.length];
            
            return;
        }
        
        //
        // now we should process the follow obviously moments:
        // 1. arrived data can be less then we have in the header defined
        // 2. data can be longer then it defined in header
        //        
        else if (_chunk_diff_size<0){            
            //
            // in this case we just append incompeted chunk to chain
            //
            NSInteger _len = [aData length]-sizeof(CSPStreamPacketHeader);

            //NSLog(@"2. new [%li:%li]", sizeof(CSPStreamPacketHeader), (long)_len) ;

            [self appendNewChunk:aData from:sizeof(CSPStreamPacketHeader)  withLength:_len seqnum:_sheader.seqnum andCompletedFlag: NO andDefinedLength:_sheader.length];
                        
            return;
        }
        else if (_chunk_diff_size>0){
            //
            // in this case we should append all chunks to chain
            //
            
            //
            // add competed data
            //
            //NSLog(@"3. new [%li:%li]", sizeof(CSPStreamPacketHeader), (long)_sheader.length) ;

            [self appendNewChunk:aData from:sizeof(CSPStreamPacketHeader)  withLength:_sheader.length seqnum:_sheader.seqnum andCompletedFlag: YES andDefinedLength:_sheader.length];
            
            //
            // scan rest of buffer
            //
            NSInteger _from = _sheader.length+sizeof(CSPStreamPacketHeader);
            NSInteger _len = [aData length] - _from;
            
            //NSLog(@"4. new [%li:%li]", (long)_from, (long)_len) ;

            NSData *_currentData = [NSData dataWithData:[aData subdataWithRange:NSMakeRange(_from, _len)]];
            //
            // use recursion patern (in this version) ##### i'm not like to use it, but i need a work solution #####
            //
            [self appendData:_currentData error:error];
            
            return;
        }
    }
}


//
// Get assembed data
//
- (NSData*) assembledData{
    if (!dataChain) {
        return nil;
    }    
    NSMutableData *ret = [NSMutableData dataWithCapacity:capacity];
    for (_GINSMutableData *chunk in dataChain) {
        if ([chunk isCompleted]) {
            [ret appendData: [chunk getData]];
        }
    }        
    return ret;
}

@end
