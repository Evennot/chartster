//
//  GICSPCommandExecutor.m
//  GIConnector
//
//  Created by denn on 3/13/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "../GICommon/GIConfig.h"

#import "GICSPCommandExecutor.h"

@interface GICSPCommandExecutor () <NSURLConnectionDataDelegate, NSStreamDelegate>

@property NSString *command;
@property NSData   *postData;     

@end

@implementation GICSPCommandExecutor
{
    //
    // absolute URL 
    //
    NSURL               *commandUrl;
    
    //
    // prepared http request
    //
    NSMutableURLRequest *request;
    
    //
    // http input stream reads data.
    // it uses when we request to execute csp command.
    // if the option of command is persistent connection we keep it and wait for data "inifinite time".
    // the "infinit time" is feature of GIConfig instance
    //
    NSInputStream       *stream;
    BOOL                 isChuncked;
    
    //
    // http response 
    //
    NSDictionary     *responseHeaders;
    NSInteger         contentLength;    
    NSInteger         statusCode;
    
    //
    // z_stream
    //
    z_stream          zStream;
    BOOL              readStreamContextReady;
    BOOL              zStreamIsready;
    
    //
    // ssl settings
    //
    BOOL              isHTTPS;
    NSDictionary     *sslSettings;
    
    
    dispatch_queue_t  processingQueue;
    dispatch_queue_t  errorQueue;
    dispatch_queue_t  mainQueue;
    
    //
    // Timers
    //
    NSTimer *connectionTimeoutTimer;
    NSTimer *readTimeoutTimer;
    NSTimer *persistTimeoutTimer;
    
    BOOL   _executing;
    BOOL   _finished;
    
    
    //
    //
    //
    BOOL      streamingInProgress;
}

- (BOOL)isExecuting{    return _executing; }

- (BOOL)isFinished{    return _finished; }

- (BOOL)isConcurrent {return NO;}

//
// Operations messages
//
- (void)start{
    [self main];
}


- (void) main{
    readStreamContextReady = zStreamIsready = NO;
    
    if (stream) {
        [stream close]; stream = nil;
    }
    
    [self pollingData];
        
    //
    // Current context is waiting for when the stream connection will close
    //
    CFRunLoopRun();
}


- (void) pollingData{
    
    stream= (__bridge NSInputStream *)(CFReadStreamCreateForHTTPRequest(CFAllocatorGetDefault(), assignNSURLRequestToCFHTTPMess(request)));
    
    //
    // update ssl setings if we need
    //    
    if(isHTTPS)
        CFReadStreamSetProperty((__bridge CFReadStreamRef)(stream), kCFStreamPropertySSLSettings, (CFTypeRef)sslSettings);
    
    [stream setDelegate: self];
    
    // it needs to be KVO compliant
    [self willChangeValueForKey:@"isExecuting"];
    _executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    //
    // run handler loop
    //
    [stream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    //
    // create peer
    //
    [stream open];
}

- (void) cancel{
    
    if([self isCancelled])
        return;
    
    [self stopConnection];
    [super cancel];
}


- (void) close{
    
    // remove it from handle loop
    if (!stream) 
        return
    
    [stream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    // close stream
    //[stream close];
    
    stream = nil;    
}

- (void) stopConnection{
    
    if ([self isFinished]) {
        return;
    }
    
    // stop current thread loop    
    CFRunLoopStop(CFRunLoopGetCurrent());
    
    [self close];
    
    //
    // Alert anyone that we are finished
    //
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    _executing = NO;
    _finished  = YES;        
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];   
    
    if (connectionTimeoutTimer) [connectionTimeoutTimer invalidate]; connectionTimeoutTimer = nil;
    if (readTimeoutTimer) [readTimeoutTimer invalidate]; readTimeoutTimer = nil;
    if (persistTimeoutTimer) [persistTimeoutTimer invalidate]; persistTimeoutTimer = nil;
}

- (void) stopConnectionByTimer: (NSTimer*) timer
{
    NSArray      *_error = [timer userInfo];
    dispatch_async(errorQueue, ^(void){
        [self.handler onError:[GICSPError errorWithCode:[[_error objectAtIndex:0 ] integerValue]
                                         andDescription:[_error objectAtIndex:1]]
                  fromCommand:self.command];    
    });
    [self stopConnection];
}

- (NSTimer*) startTimer:(time_t)timeout withErrorCode: (GICSP_ERRORS)code andDescription: (NSString*)description
{
    if(timeout){
        NSArray *_error_info = [NSArray arrayWithObjects:[NSNumber numberWithInt: code], description, nil];
        return [NSTimer scheduledTimerWithTimeInterval:timeout
                                                target: self
                                              selector: @selector(stopConnectionByTimer:)
                                              userInfo: _error_info
                                               repeats: NO];
    }
    return nil;
}

-(void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    
    BOOL   should_it_close =NO;
    
    @autoreleasepool {
        
        switch (eventCode) {
                //
                // Error handling
                //
            case NSStreamEventOpenCompleted:            
            {
                
                //
                // should execute in the the same context
                //
                [self.handler onOpenCompleted: self.command];
                
                //
                // set connection timer for request
                //
                if (![self.options isPersistent])
                    connectionTimeoutTimer = [self startTimer:self.config.connectionTimeout
                                                withErrorCode:GICSP_ERROR_CONNECTION_TIMEOUT
                                               andDescription:GILocalizedString(@"The CSP connection couldn’t be completed. Operation timed out.", @"CSP connection timed out") ];
                
                //
                // forced reconnect receiver
                //
                if ([self.options isPersistent] && self.config.persistTimeout>0)
                    //
                    // Set peer connection time limit
                    //
                    persistTimeoutTimer = [self startTimer:self.config.persistTimeout
                                             withErrorCode:GICSP_ERROR_PERSISTENT_TIMEOUT
                                            andDescription:GILocalizedString(@"The CSP receiver connection too long. Operation interrupted.", @"Opertaion interrupted")];
                
                streamingInProgress  = NO;
                break;
            }
                
            case NSStreamEventErrorOccurred:
            {   

                void (^_error_block)(void) = ^(void){
                    
                    if ([[stream streamError] code] <= errSSLProtocol) {
                        
                        [self.handler onError:[GICSPError errorWithCode:GICSP_ERROR_CERT_INVALID
                                                         andDescription:[NSString stringWithFormat:
                                                                         GILocalizedString(@"%@ certificate is not trusted for the receiver peer connection.", @"Bad certificate"), [self.config.serverUrl host]]]
                                  fromCommand:self.command];
                    }
                    else{
                        GICSPError *_error ;
                        if ([stream streamError])
                            _error = [[GICSPError alloc] initWithNSError:[stream streamError]];
                        else
                            _error = [GICSPError errorWithCode:GICSP_ERROR_CONNECTION_FAIL
                                                andDescription:[NSString stringWithFormat:
                                                                GILocalizedString(@"Connection to %@ could not be created.", @"Connection could not be created"), commandUrl]
                                      ];
                        [self.handler onError: _error fromCommand:self.command];
                }};
                
                if(![self.options isPersistent])
                    dispatch_async(errorQueue, _error_block);
                else
                    //
                    // receiver execute synchronously
                    //
                    _error_block();
                
                should_it_close = YES;
                streamingInProgress = NO;
                break;
            }
                
                //
                // Connection peer has closed
                //
            case NSStreamEventEndEncountered:
            {            
                if ([self.options isPersistent]) {
                    dispatch_async(processingQueue, ^(void){
                        [self.handler onError:[GICSPError errorWithCode:GICSP_ERROR_RECEIVER_CLOSED
                                                         andDescription:[NSString stringWithFormat:
                                                                         GILocalizedString(@"%@ connection closed or cancelled.", @""), commandUrl]]
                                  fromCommand:self.command];
                    });
                }
                should_it_close = YES;
                streamingInProgress = NO;
                break;
            }    
                
                //
                // Data processing
                //
            case NSStreamEventHasBytesAvailable:
            {
                
                if (connectionTimeoutTimer) [connectionTimeoutTimer invalidate]; connectionTimeoutTimer = nil;
                
                uint8_t _buffer[CSP_BUFFER_SIZE]; memset(_buffer, 0, CSP_BUFFER_SIZE);
                int     _buffer_size = sizeof(_buffer);
                
                if(![self.options isPersistent])
                    //
                    // request timeout. obviously peer connection has the infinit reading time 
                    //
                    readTimeoutTimer = [self startTimer:self.config.readTimeout withErrorCode:GICSP_ERROR_READ_TIMEOUT
                                         andDescription:GILocalizedString(@"The CSP connection read date timeout. Operation interrupted.", @"Connection interrupted")]; 
                
                int    len  = 0;
                
                //
                // Read body
                //
                
                //
                // tmp store
                //
                NSMutableData *_streamed_buffer = [NSMutableData dataWithCapacity:0];
                                
                do {
                    //
                    // read all bytes
                    //
                    len = [(NSInputStream *)stream read:_buffer maxLength:_buffer_size];
                    
                    
                    if(len<=0)
                        break;
                    
                    [_streamed_buffer appendBytes:_buffer length:len];
                    
                } while (stream && self.isExecuting && CFReadStreamHasBytesAvailable((CFReadStreamRef)stream));
                
                if (!stream || !self.isExecuting) {
                    streamingInProgress = NO;
                    break;
                }
                                                
                if(len<=0) {
                    streamingInProgress = NO;
                    break;
                }

                
                if (![self setupStreamContext:nil]) {
                    //
                    // wrong http header
                    //
                    should_it_close = YES;
                    streamingInProgress = NO;
                    break;
                };

                NSData *_data;
                if(zStreamIsready){
                    //
                    // We could not parse gzipped stream
                    //
                    
                    GIError *_error;
                    _data = [self uncompressBytes:[_streamed_buffer bytes] length:[_streamed_buffer length] error:&_error];
                    
                    if (_error){
                        //
                        // pass to error hanlder
                        //
                        dispatch_async(errorQueue, ^(void){
                            [self.handler onError: [[GIError alloc] initWithNSError:_error] fromCommand:self.command];
                        });  
                        should_it_close = YES;
                        return;
                    }
                }
                else
                    _data = _streamed_buffer;
                
                if (![self.options isPersistent]) {
                    //
                    // request and close connection
                    // 
                    dispatch_async(processingQueue, ^(void){
                        [self.handler onDataReceived: _data
                                         withHeaders: responseHeaders 
                                           andStatus: statusCode 
                                         fromCommand: self.command];
                    });
                }
                else if (_data) {                    
                    
                    if (!streamingInProgress || !self.completedData) {
                        //
                        // Start reading process
                        //
                        streamingInProgress = YES;
                        
                        //
                        // create new data store
                        //

                        self.completedData = [[GICSPChunkedData alloc] init];
                    }
                                        
                    //
                    // Win! pass data to connection handler
                    //
                    
                    GICSPError *_err;                                        
                    
                    [self.completedData appendData:_data error:&_err];
                    //
                    // keep to reconnect
                    //
                    self.seqnum = self.completedData.currentSeqNum;
                    
                    if (_err) {
                        NSLog(@"%@", _err);
                    }
                    
                    if ([self.handler isDataCompleted:_data]) {
                        //
                        // When all data has received and the end of data is marked -
                        // call handler
                        //
                        NSData *_d = [[self.completedData assembledData] copy];
                        dispatch_async(processingQueue, ^(void){
                            [self.handler onDataReceived: _d
                                             withHeaders: responseHeaders
                                               andStatus: statusCode
                                             fromCommand: self.command];
                        });                    
                        streamingInProgress = NO;
                        self.completedData = nil;
                    }
                    
                    should_it_close = NO;
                    break;
                }
                else
                    streamingInProgress = NO;
            }
                
                //
                // Just ignore
                //
            default:
                break;
        }    
    }
    
    if (readTimeoutTimer) [readTimeoutTimer invalidate]; readTimeoutTimer = nil;
    
    if (should_it_close) {
        [self stopConnection];
    }
}


- (BOOL) setupStreamContext:(CFHTTPMessageRef)cfResponse{
    
    if (!readStreamContextReady) {
        CFHTTPMessageRef _inresponse = cfResponse?cfResponse:(CFHTTPMessageRef)CFReadStreamCopyProperty((__bridge CFReadStreamRef)(stream), kCFStreamPropertyHTTPResponseHeader);
        
        statusCode     = CFHTTPMessageGetResponseStatusCode(_inresponse);  
        
        if (statusCode / 100 > 3){
            NSString        *_statusLine = (NSString*) CFBridgingRelease(CFHTTPMessageCopyResponseStatusLine(_inresponse));
            dispatch_async(errorQueue, ^(void){
                [self.handler onError:[GICSPError errorWithHTTPStatus:statusCode andDescription: _statusLine] fromCommand: self.command];
            });
            return NO;
        }
        
        responseHeaders = (__bridge NSDictionary *)(CFHTTPMessageCopyAllHeaderFields(_inresponse));
                
        contentLength   = [responseHeaders[@"Content-Length"] integerValue]; 
        NSString *_encoding  = responseHeaders[@"Content-Encoding"]; 
        
        if (_encoding && [_encoding rangeOfString:@"gzip"].location != NSNotFound){
            //
            // we need to decompress data
            //
            [self setupZstream];            
        }
        readStreamContextReady = YES;
        
        if (contentLength==0 && [responseHeaders[@"Transfer-Encoding"] hasSuffix:@"Identity"]) {
            isChuncked = YES;
        }
        else
            isChuncked = NO;
    }
    
    return YES;
}


- (void) makeNewRequest{
    //
    // Keep request context
    //
    request = [NSMutableURLRequest requestWithURL: commandUrl
                                      cachePolicy: NSURLRequestReloadIgnoringCacheData
                                  timeoutInterval: [self.options isPersistent]? self.config.persistTimeout : self.config.connectionTimeout
               ];
    
    //
    // GI-CSP specific HTTP headers
    //        
    [request setAllHTTPHeaderFields:[NSDictionary dictionaryWithObjectsAndKeys:
                                     [self.config.serverUrl host], @"Host",
                                     @"text/plain", @"Content-type",
                                     @"no-cache",   @"Pragma",
                                     @"no-cache",   @"Cache-Control",
                                     [NSString stringWithFormat:@"%@", @"gzip"], @"Accept-Encoding",
                                     nil]];
    
    if (self.seqnum > 0) {
        [request setValue: [NSString stringWithFormat:@"%ld", (long)self.seqnum] forHTTPHeaderField: @"X-CspHub-Seqnum"];
    }
    
    //
    // How to make request
    //
    if (self.postData){
        request.HTTPMethod = @"POST";
        request.HTTPBody   = self.postData;
        [request setValue: [NSString stringWithFormat:@"%d", [self.postData length]] forHTTPHeaderField: @"Content-Length"];
        
    }
    else
        request.HTTPMethod = @"GET";
    
    //
    // Init SSL options
    //
    [self _initSSL];

}

//
// init instance
//
- (id) initCommand:(NSString*)aCommand withData:(NSData*)aData andHandler: (id<GICSPCommandHandler>)aHandler andConfig: (GIConfig*)aConfig{
    
    if(!(self=[super init]))
        return nil;
    
    __block id __cleanuped_obj = self;
    [self setCompletionBlock: ^(void){
        [__cleanuped_obj _cleanUpAllInstances];
    }];
    
    
    self.seqnum   = 0;
    self.postData = aData;
    self.config   = aConfig;
    self.command  = aCommand; 
    self.handler  = aHandler; 
    
    //
    // queues
    //
    processingQueue = dispatch_queue_create("com.goinvestconnector.processing.queue", DISPATCH_QUEUE_SERIAL);
    errorQueue = dispatch_queue_create("com.goinvestconnector.error.queue", DISPATCH_QUEUE_SERIAL);
    mainQueue  = dispatch_queue_create("com.goinvestconnector.persistent.queue", DISPATCH_QUEUE_SERIAL);
        
    commandUrl = [self.config.serverUrl URLByAppendingPathComponent: self.command isDirectory:NO];
    
    [self makeNewRequest];
    
    return self;
}

- (void) _initSSL{
    if ([[self.config.serverUrl scheme] isEqualToString:@"https"]) {
        isHTTPS=YES;
        if (!(self.config.validatesSecureCertificate))
            sslSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [NSNumber numberWithBool:YES], kCFStreamSSLAllowsExpiredCertificates,
                           [NSNumber numberWithBool:YES], kCFStreamSSLAllowsAnyRoot,
                           [NSNumber numberWithBool:NO],  kCFStreamSSLValidatesCertificateChain,
                           kCFNull,kCFStreamSSLPeerName,
                           nil];
        else
            sslSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [NSNumber numberWithBool:NO], kCFStreamSSLAllowsExpiredCertificates,
                           [NSNumber numberWithBool:NO], kCFStreamSSLAllowsAnyRoot,
                           [NSNumber numberWithBool:YES],  kCFStreamSSLValidatesCertificateChain,
                           nil];
    }
    else
        isHTTPS=NO;
}

//
// Clean all 
//
- (void) _cleanUpAllInstances{
    // NSLog(@"CLEAN UP EXECUTIONS %@ at %li", commandUrl, time(nil));
    
    //
    // Stop all operations
    //
    if (stream)
        [stream close];
    stream = nil;
    
    [self stopConnection];
}


//
// The only one usefull code copy-paste from ASIHTTPRequest framework.
//
- (GIError*) setupZstream{
    
    if (zStreamIsready) {
        return nil;
    }
    
    // Setup the inflate stream
    zStream.zalloc   = Z_NULL;
    zStream.zfree    = Z_NULL;
    zStream.opaque   = Z_NULL;
    zStream.avail_in = 0;
    zStream.next_in  = 0;
    int status = inflateInit2(&zStream, (15+32));
    if (status != Z_OK) {
        return [[self class] inflateErrorWithCode:status];
    }
    zStreamIsready = YES;
    return nil;    
}

- (NSData *)uncompressBytes:(const void*)bytes length:(NSUInteger)length error:(GIError **)err{
    if (length == 0) return nil;
    
    NSUInteger halfLength = length/2;
    NSMutableData *outputData = [NSMutableData dataWithLength:length+halfLength];
    
    int status;
    
    zStream.next_in = (Bytef*)bytes;
    zStream.avail_in = (unsigned int)length;
    zStream.avail_out = 0;
    
    GIError *theError = nil;
    
    NSInteger bytesProcessedAlready = zStream.total_out;
    while (zStream.avail_in != 0) {
        
        if (zStream.total_out-bytesProcessedAlready >= [outputData length]) {
            [outputData increaseLengthBy:halfLength];
        }
        
        zStream.next_out = [outputData mutableBytes] + zStream.total_out-bytesProcessedAlready;
        zStream.avail_out = (unsigned int)([outputData length] - (zStream.total_out-bytesProcessedAlready));
        
        status = inflate(&zStream, Z_NO_FLUSH);
        
        if (status == Z_STREAM_END) {
            break;
        } else if (status != Z_OK) {
            if (err) {
                *err = [[self class] inflateErrorWithCode:status];
            }
            return nil;
        }
    }
    
    if (theError) {
        if (err) {
            *err = theError;
        }
        return nil;
    }
    
    // Set real length
    [outputData setLength: zStream.total_out-bytesProcessedAlready];
    return outputData;
}

+ (GIError *)inflateErrorWithCode:(int)code
{
    NSDictionary *userInfo = [
                              NSDictionary 
                              dictionaryWithObjects:[NSArray arrayWithObjects: @"Stream input error", 
                                                     [NSString stringWithFormat: 
                                                      GILocalizedString(@"Decompression of data failed with code %i", @"Decompretion error"), code ], nil] 
                              forKeys:[NSArray arrayWithObjects: NSLocalizedDescriptionKey, NSLocalizedFailureReasonErrorKey, nil]
                              ];        
    return [[GIError alloc] 
            initWithNSError: [NSError errorWithDomain:[NSString stringWithFormat:@"%@.http.stream", GI_ERROR_DOMAIN_PREFIX] 
                                                 code:code
                                             userInfo:userInfo]];
}


//
// Usefull convertor from CFNetwork C-structured to NSObject style 
//
CFHTTPMessageRef assignNSURLRequestToCFHTTPMess(NSURLRequest *request) {
    
    CFHTTPMessageRef message = CFHTTPMessageCreateRequest(kCFAllocatorDefault, (__bridge CFStringRef)[request HTTPMethod], (__bridge CFURLRef)[request URL], kCFHTTPVersion1_1);
    
    for (NSString *currentHeader in [request allHTTPHeaderFields]) {
        CFHTTPMessageSetHeaderFieldValue(message, (__bridge CFStringRef)currentHeader, (__bridge CFStringRef)[[request allHTTPHeaderFields] objectForKey:currentHeader]);
    }
    
    if ([request HTTPBody] != nil) {
        CFHTTPMessageSetBody(message, (__bridge CFDataRef)[request HTTPBody]);
    }
    
    return message;
}

CFHTTPMessageRef httpMessageCreateForResponse(NSHTTPURLResponse *response) {
    CFHTTPMessageRef message = CFHTTPMessageCreateResponse(kCFAllocatorDefault, [response statusCode], (__bridge CFStringRef)[NSHTTPURLResponse localizedStringForStatusCode:[response statusCode]], kCFHTTPVersion1_1);
    [[response allHeaderFields] enumerateKeysAndObjectsUsingBlock:^ (id key, id obj, BOOL *stop) {
        CFHTTPMessageSetHeaderFieldValue(message, (__bridge CFStringRef)key, (__bridge CFStringRef)obj);
    }];
    return message;
}

@end




