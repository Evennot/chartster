//
//  GIError+GICSPTunnelError.h
//  GIConnector
//
//  Created by denn on 3/11/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIError.h"


enum  _csp_errors {
    
    GISP_ERROR_HTTP_STATUS_SID_EXPIRED      = 410, // session id no longer exist
    GISP_ERROR_HTTP_STATUS_UNAVAILABLE      = 503, // service temporary unavailable
    
    GICSP_ERROR_CONNECTION_FAIL     = 4000, // could not connect "/connect"
    GICSP_ERROR_NOT_OPENED          = 4001, // connected but server is not opened yet
    GICSP_ERROR_SID_IS_NULL         = 4002, // connected but server is wrong or failed
    GICSP_ERROR_RETRIES_LIMIT       = 4003, // limit reconnections
    GICSP_ERROR_CONNECTION_TIMEOUT  = 4004, // connection timeout exchausted 
    GICSP_ERROR_READ_TIMEOUT        = 4005, // read timeout
    GICSP_ERROR_PERSISTENT_TIMEOUT  = 4006, // connection time is too long
    GICSP_ERROR_RECEIVER_CLOSED     = 4006, // receiver persistent connection closed, cancelled or broken
    GICSP_ERROR_CERT_INVALID        = 5000, // CERT invalid
    GICSP_ERROR_SHORT_CSP_PACKET    = 6000,  // CSP packet is too short
    GICSP_ERROR_INCOMPLETED_CSP_PACKET    = 6001  // CSP packet is incompleted
};

typedef enum _csp_errors GICSP_ERRORS;

@interface GICSPError: GIError
+ (id) errorWithCode:(int)code andDescription:(NSString*)description;
+ (id) errorWithHTTPStatus:(int)status andDescription:(NSString*)statusString;
@end
