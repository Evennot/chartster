//
//  GICSStreamHeader.h
//  GIConnector
//
//  Created by denn on 3/11/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef char _tp_header_size[4];

typedef struct {
    _tp_header_size header_size;
    char seqnum[8];
    char length[8];
    
} CSPStreamPacketHeader;

#define GICSP_HEADER_SIZE (sizeof(CSPStreamPacketHeader)-sizeof(_tp_header_size))

@interface GICSPStreamHeader : NSObject
@property(readonly)     long long int header_size, seqnum, length;
- (id) initWithCSPPacket: (CSPStreamPacketHeader)packet;
- (id) initWithData: (NSData*)data;
@end


@interface CSPStreamHeader : NSObject 
@end 



