//
//  GICSStreamHeader.m
//  GIConnector
//
//  Created by denn on 3/11/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GICSPStreamHeader.h"

@interface GICSPStreamHeader ()
@property(assign)     long long int header_size, seqnum, length;
@end

@implementation GICSPStreamHeader

@synthesize header_size, seqnum, length;

//
// Mojo code
//
static int _asc2int(char* buffer, int len)
{
    int result = 0;
    
    for (int i = 0; i < len; i++)
    {
        char ch = toupper(buffer[i]) - '0';
        if (ch > 9)
            ch += '0' - 'A' + 10;
        
        result = result * 16 + ch;
    }
    
    return result;
}

- (id) initWithCSPPacket: (CSPStreamPacketHeader)packet{
    
    if(!(self = [super init]))
        return nil;
    
    self.header_size = _asc2int(packet.header_size, sizeof(packet.header_size));
    self.seqnum = _asc2int(packet.seqnum, sizeof(packet.seqnum));
    self.length = _asc2int(packet.length, sizeof(packet.length));
    
    return self;
}


- (id) initWithData:(NSData *)data{
    
    CSPStreamPacketHeader packet; 
    memcpy(&packet, [data bytes], sizeof(packet));
    
    return [self initWithCSPPacket:packet];
}

- (NSString*) description{
    return [NSString stringWithFormat:@"header_size=%lli: seqnum=%lli: length=%lli", self.header_size, self.seqnum, self.length];
}

@end
