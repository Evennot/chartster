//
//  GICSPTunnel.h
//  GIConnector
//
//  Created by denn on 3/8/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GICSPTunnelHandler.h"
#import "GICSPError.h"
#import "../GICommon/GIConfig.h"


//
// Create CSP tunnel, establish connection and start async reader connection.
// Send message if you want.
// Receive messages and eval handler method if some event happens.
//
//
// Base class controls low-level behavior of connection. The first version based on CSP - Comet Session Protocol.
// Next release should support websockets. 
//
// At the level of abstraction we just handle connection commands, events and receive data which server replays asynchronously when it receives 
// client command. According to our GI-CSP extention specs we use 4 types of commands:
// 1. connect - get SESSION ID
// 2. ping    - check connection and force the server keep our session id
// 3. send    - send POST data, at the level it undefines what we send to server
// 4. receive - persistent connection, in technical terms, if we use CSP approach, it means long-polling connection
//              we sniff this connection and waitnig for any data which can receive from server. 
//              received data has special structer: [header:rawdata]. if header or data are corrupted 
//              receiver makes reconnection with last seq number of data and try to reread currupted data.
//              after success we pass data to the application businnes-level.
//
//
@interface GICSPTunnel : NSOperation 

//
// Connection delagate which handles data to next-level abstraction of an application uses Tunnel instance
//
@property(nonatomic,strong) id <GICSPTunnelHandler> connectionHandler;

//
// In our case it means RTD: round-trip-delay
// It is the length of time it takes for a signal to be sent plus the length of time it takes for an acknowledgment of that signal to be received. 
// This time delay therefore consists of the transmission times between the two points of a signal.
// We pursue two aims to run ping-pong and compute round-trip-time:
// 1. server keeps our session ID if it knows the client which makes the connection is alive
// 2. client app can use this value to indicate a user, who uses the app, about network quality
//
@property(readonly)     NSTimeInterval  pingRoundTripTime;

//
// Create tunnel instance
//
- (id)   initWithGIConfig: (GIConfig *) userConfig;

//
// Start connection. Make backgrounded inifinit loop dispatch all communication process.
//
- (void) startAsync;

//
// Close receiver connection.
//
- (void) closeReceiver;
- (void) restoreReceiver;

//
// Check state
//

// Session is opened, receiver can be opened or closed.
- (BOOL) isConnected;

// session is opened, receiver is opened.
- (BOOL) isOpened;

// session is closed, receiver is closed.
- (BOOL) isClosed;

//
// Send buffer to server.
//
- (void) send: (NSData*)data;
- (void) sendString: (NSString*)buffer;


@end
