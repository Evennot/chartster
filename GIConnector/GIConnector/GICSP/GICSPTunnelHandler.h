//
//  GICSPTunnelHandler.h
//  GIConnector
//
//  Created by denn on 3/8/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../GICommon/GIError.h"

typedef enum  {
    GICSP_UNKNOWN    = -1,
    GICSP_CONNECT    = 0,
    GICSP_DISCONNECT = 1,
    GICSP_PING       = 2,
    GICSP_RECEIVE    = 3,
    GICSP_SEND       = 4
} GICSPCommand;


//
// CSP interface to stream data connector.
// When some data, error, or event occurres we pass control to appropriated method.
//
@protocol GICSPTunnelHandler <NSObject>

// 
// Eval method when connection is opening.
// 
- (void) onConnect: (NSData*)  data;

//
// Eval method when receiver is opening.
//
- (void) onOpen: (NSData*)  data;

//
// Eval when connection has been closed by timeout, closed by serevr or connection state has been changed.
//
- (void) onClose: (NSData*)  data;

//
// When data has been recieved we pass control to onRead method.
//
- (void) onRead: (NSData*)  data;

//
// When we send data we can control what we replaied from server.
//
- (void) onSend: (NSData*)  data;

//
// When we maked ping-pong and data has been recieved as result ping command we pass control to onPing method.
//
- (void) onPing: (NSData*)data roundTrip:(NSTimeInterval)roundTrip;

//
// If some exception hapens error should catch the event.
//
- (void) onError: (GIError*)error in:(GICSPCommand)command;


//
// We want to have a "gear" that can tell the stream processed task to finish reading
// and after that we can decide to pass completely loaded data to main process.
// It returns true when we decide data is compited and we expect call onRead callback
//
- (BOOL) isDataCompleted: (NSData*)chainedData;

@end
