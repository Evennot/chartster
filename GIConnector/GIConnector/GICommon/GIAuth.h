//
//  GIAuth.h
//  GIConnector
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>


#define GIC_GUEST_USER    @""
#define GIC_GUEST_PASSWD  @""

@interface GIAuth : NSObject <NSCopying>

@property(nonatomic) NSString *user, *password;
@property(readonly) time_t lastTime;

- (id)   initWithUser:(NSString*)usr andPassword:(NSString*)pas;

- (void) saveInKeyChain:(NSString*) domain;
- (OSStatus) restoreFromKeyChain:(NSString*) domain;

@end


@interface GILogins : NSObject

- (id) initWithDictionary:(NSDictionary*)logins;

-(void) addChannelLogin:(GIAuth*)auth onChannel:(NSString*)channel;
-(GIAuth*) channelLogin:(NSString*)channel;
@end