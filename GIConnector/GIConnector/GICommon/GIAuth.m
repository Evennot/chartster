//
//  GIAuth.m
//  GIConnector
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Security/Security.h>
#import "GIAuth.h"

@interface GIAuth ()

@property time_t lastTime;

@end

@implementation GIAuth

@synthesize user=_user, password=_password;
@synthesize lastTime;

- (id) copyWithZone:(NSZone *)zone{
    GIAuth *_new_obj = [[[self class] allocWithZone:zone] init];
    if (_new_obj) {
        _new_obj->_password = [self->_password copyWithZone:zone];
        _new_obj->_user = [self->_user copyWithZone:zone];
        _new_obj->lastTime = self->lastTime;
    }
    return _new_obj;
}

- (id) initWithUser:(NSString *)usr andPassword:(NSString *)pas{
    
    if(!(self = [super init]))
        return nil;
    
    _user = usr;
    _password = pas;
    self.lastTime  = time(nil); 

    return self;    
}

- (id) init{
    
    if(!(self = [super init]))
        return nil;
    
    self.user = GIC_GUEST_USER;
    self.password = GIC_GUEST_PASSWD;
    
    return self;    
}

- (void) setUser:(NSString *)user{
    _user = user;
    self.lastTime  = time(nil); 
}

- (void)setPassword:(NSString *)password{
    _password = password;
    self.lastTime  = time(nil); 
}

- (NSString*) description{
    return [NSString stringWithFormat:@"User: %@, Password: %@, Last Access time: %ld", _user, _password, lastTime];
}

- (NSDictionary*) searchOptions:(NSString *) domain{
    return [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, domain, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
}

- (void) saveInKeyChain:(NSString*) domain{    
        
    NSDictionary *search = [self searchOptions:domain];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) search);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [self.password dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, domain, kSecAttrServer, passwordData, kSecValueData, self.user, kSecAttrAccount, nil];
    
    //NSLog(@"Save keychain: %@", dict);
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);    
}

- (OSStatus) restoreFromKeyChain:(NSString*) domain{
    
    // Create dictionary of search parameters
    NSDictionary* search = [self searchOptions:domain];
    
    // Look up server in the keychain
    CFTypeRef result = nil;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) search, &result);

    NSDictionary *found = (__bridge NSDictionary *)(result);

    //NSLog(@"Restore keychain: %@ for domain: %@", found, domain);

    if (!result) return err;
    
    // Found
    self.user     = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
    self.password = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
    
    return err;
}

@end


@implementation GILogins
{
    NSMutableDictionary *logins;
}

- (id) init{
    
    if(!(self = [super init]))
        return nil;
    
    logins = [NSMutableDictionary dictionaryWithCapacity:0];
    
    return self;
}


- (id) initWithDictionary:(NSDictionary *)aLogins{
    if(!(self = [self init]))
        return nil;

    for (NSString *channel in aLogins) {
        GIAuth *auth = [[GIAuth alloc] initWithUser:aLogins[channel][0] andPassword:aLogins[channel][1]];
        [self addChannelLogin:auth onChannel:channel];
    }
    
    return self;
}

- (void) addChannelLogin:(GIAuth *)auth onChannel:(NSString *)channel{
    [logins setValue:auth forKey:channel];
}

- (GIAuth*) channelLogin:(NSString *)channel{
    return logins[channel];
}

- (NSString*) description{
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    
    for (NSString *c in logins) {
        [s appendFormat:@"{%@: [%@]}", c, logins[c]];
    }
    return s;
}

@end