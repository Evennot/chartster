//
//  NSBundle+GIBundle.h
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>


//
// Customize localizations. 
//

#define GIBUNDLE_NAME @"GIConnector"
#define GILOCALIZABLE @"Localizable"
#define GILocalizedString(key,comment) NSLocalizedStringFromTableInBundle(key, GILOCALIZABLE, [GIBundle GIBundle], comment)

@interface GIBundle: NSBundle

+(NSBundle*) GIBundle;

@end
