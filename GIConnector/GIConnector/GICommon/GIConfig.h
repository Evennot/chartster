//
//  GIConfig.h
//  GIConnector
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIAuth.h"

//
// Default connection settings
//
#define GI_CONNECTION_TIMEOUT 10  // stop a connection if it established but not completed
#define GI_RECONNECT_COUNT    3   // reconnection retries if connection could not be established, refused for example
#define GI_RECONNECT_TIMEOUT  3   // the time out between connection retries if it could not be established, refused for example
#define GI_READ_TIMEOUT       10  // a connection estableshed but data could not be received after the timeout
#define GI_PERSIST_TIMEOUT    0   // a connection established, but data had received after the timeout
                                  // 0 - means infinit waitnig
#define GI_WRITE_TIMEOUT      10  // a connection established, data has been sent but the operation could not be completed

//
// GIConfig contains connection instance preferences, such as:
// * goInvest server url
// * login
// * password
// * timeouts options
// * etc...
//
@interface GIConfig : NSObject 

//
// Auth container
//
//@property(nonatomic) GILogins    *logins;
@property(nonatomic)   GIAuth  *login;

//
// Options
//
@property int       reconnectCount;
@property time_t    connectionTimeout,
                    reconnectTimeout,
                    readTimeout, 
                    persistTimeout, 
                    writeTimeout;
@property BOOL      doPing;

//
// Whether we trust invalide certificates or not.
// Use it just for DEBUG only!
//
@property           BOOL    validatesSecureCertificate;

@property(readonly) NSMutableArray *marketPlaces;
@property(readonly) NSMutableArray *channels;

//
// Init with an url.
//
- (id) initWithUrl: (NSString *)url;

//
// Set server url from string.
//
- (void)   setServerUrl:(NSString *)url;

//
// Get server URL
//
- (NSURL*) serverUrl;

//
// Get connection channel by market name
//
- (NSString*) getChannelByMarket:(NSString*)market;
- (NSString*) getMarketByChannel:(NSString*)channel;

- (NSString*) getUserPrefixByChannel:(NSString*)channel;


@end
