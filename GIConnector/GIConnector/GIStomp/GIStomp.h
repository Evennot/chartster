//
//  GIStompCommands.h
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompFrame.h"

//
// When we receive Frame dispatcher can pass frame to 
// model instance whih can be mamanged in View
//
@protocol GIStompDispatcherProtocol <NSObject>
- (void) didFrameReceive:(id)frame;
@end

//
// Dispatcher pass to STOMP frame session id 
//
@protocol GIStompRegisterDispatcherProtocol <NSObject>
- (void) setSession:(NSString*)sessionID;
@end


@class GIStompMessage;

//
// CONNECTION 
//
@interface GIStompConnect : GIStomp <GIStompDeclarationProtocol>

+ (id) toChannel:(NSString*)channel withLogin:(NSString*)login andPasscode:(NSString*)passcode;
- (NSString*) sessionID;
- (NSString*) responsedChannelID;
@end

//
// DISCONNECT
//
@interface GIStompDisconnect : GIStomp <GIStompDeclarationProtocol>
+ (id) make;
@end

//
// STOMP REQUEST
//
@interface GIRequestStompFrame : GIStomp <GIStompRegisterDispatcherProtocol,GIStompDeclarationProtocol>

// Unique request id
@property(readonly) NSNumber *requestID; // uniq id request
// request data
- (void) selectFor:(NSString *)selector;
- (id)init;

//
// Who will process response
//
@property id<GIStompDispatcherProtocol> delegate;

@end

//
// REQUEST TICKER LIST
//
@interface GIStompTickers : GIRequestStompFrame
// select list of instruments
+ (id) select:(NSString*)tickerMask onMarket:(NSString*)market;
@end


//
// STOMP SUBSCRIBERS
//
@interface GIStompMessage : GIStomp <GIStompRegisterDispatcherProtocol, GIStompDeclarationProtocol>
//
@property(readonly) NSNumber *subscriptionID;
@property(readonly) NSString *market;

//
// Who will process response
//
@property id<GIStompDispatcherProtocol> delegate;

// subscribe on messages
//- (void) selectFor:(NSString *)selector;
- (void) selectFor:(NSString *)selector andTicker: (NSString*)ticker;
- (id) init;

@property(readonly) dispatch_queue_t stomponReadQueue;
@end


//
// UNSUBSCRIBE
//
@interface GIStompUnsubscriber : GIStomp <GIStompDeclarationProtocol>
+ (id) makeForMessage:(GIStompMessage*)message;
@end


//
// SELECTOR
//
@interface GIStompSelector: GIStompMessage
+ (id) select:(NSString *)ticker;
@end

//
// SYSLOG
//
@interface GIStompSyslog : GIStompMessage
//
+ (id) select;
@end


//
// ORDERBOOK
//
@interface GIStompOrderbook : GIStompSelector
@property NSInteger splitPosition;
+ (void) setDepth:(NSInteger)depth;
@end


//
// LASTTRADES
//
@interface GIStompLastTrades : GIStompSelector
@end

//
// SECURTIES
//
@interface GIStompSecurity : GIStompSelector
- (NSDictionary*) getRowByTicker:(NSString*)ticker;
@end


//
// CANDLES
//
@interface GIStompCandles: GIStompSelector
+(id) select:(NSString *)ticker andInterval:(NSString*)interval;
@end


