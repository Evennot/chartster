//
//  GIStompCommands.m
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStomp.h"


//
// CONNECT
//
@implementation GIStompConnect

+ (id) toChannel:(NSString *)channel withLogin:(NSString *)login andPasscode:(NSString *)passcode{
    
    GIStompConnect *connect = [[GIStompConnect alloc] initWithRequestName:@"CONNECT" andResponseName:@"CONNECTED" ];
        
    if (![channel isEqualToString:@"any"]) {
        [connect.request setHeader:@"login" withValue: login];
        [connect.request setHeader:@"passcode" withValue:passcode];        
    }
    [connect.request setHeader:@"channel" withValue: channel];
    
    return connect;
}

- (NSString*) sessionID{ return self.lastResponse.headers[@"session"];}

- (NSString*) responsedChannelID{
    return self.lastResponse.headers[@"channel"];
}

@end

//
// DISCONNECT
//
@implementation GIStompDisconnect

+ (id) make{
    return [[GIStompDisconnect alloc] initWithRequestName:@"DISCONNECT" andResponseName:@""] ;
}
@end


//
// UNSUBSCRIBE
//
@implementation GIStompUnsubscriber

+ (id) makeForMessage:(GIStompMessage *)message{
    GIStompUnsubscriber *req = [[GIStompUnsubscriber alloc] initWithRequestName:@"UNSUBSCRIBE" andResponseName:@""] ;
    [req.request setHeader:@"destination" withValue: [[message class] destination] ];
    [req.request setHeader:@"sid" withValue: message.subscriptionID];
    return req;
}
@end

//
// STOMP REQUEST
//
@interface GIRequestStompFrame ()
@property NSNumber *requestID;
@end

static NSInteger _gistomp_request_id = 0;

@implementation GIRequestStompFrame
@synthesize requestID=_requestID;

- (void) setSession:(NSString *)sessionID{
    if (sessionID)
        [self.request setHeader:@"session-id" withValue: sessionID];
    else
        [self.request setHeader:@"session-id" withValue: @"0"];
}

- (id) init{
    return  [ self initWithRequestName:@"REQUEST" andResponseName:@"REPLY"];
}

- (void) selectFor:(NSString *)selector{
    
    self.requestID = [NSNumber numberWithInteger:_gistomp_request_id++];
    
    [self.request setHeader:@"destination" withValue:[[self class] destination]];
    
    if (select)
        [self.request setHeader:@"selector" withValue:selector];
    
    //[_stomp.request setHeader:@"id" withValue:[NSString stringWithFormat:@"%@:%@:%i",channel,sid,_stomp.requestID]];
    
    [self.request setHeader:@"id" withValue:[NSString stringWithFormat:@"%@", self.requestID]];
}
@end


//
// TICKER LIST
//
@implementation GIStompTickers

+ (NSString*) destination{
    return @"list";
}

+ (id) select:(NSString *)tickerMask onMarket:(NSString *)market{
    
    NSString  *pattern;
    
    if(tickerMask!=nil)
        pattern = [NSString stringWithFormat:@" and pattern=\"%@\"", tickerMask];
        
    NSString  *selector;
    if (market || pattern) {
        selector= [NSString stringWithFormat:@"marketplace=\"%@\"%@", market, pattern?pattern:@""];
    }
    
    GIStompTickers *tickers = [[GIStompTickers alloc] init];
    
    [tickers selectFor:selector];
    
    return tickers;
}

@end


//
// SUBSCRIBE
//
@interface GIStompMessage()
@property NSNumber *subscriptionID;
@property NSString *market;
@property dispatch_queue_t stomponReadQueue;
@end

static NSInteger _gistomp_subscribtion_id = 0;

@implementation GIStompMessage

- (void) setSession:(NSString *)sessionID{
if (sessionID) {
        [self.request setHeader:@"session" withValue:sessionID];
    }
}

- (id) init{
    return [super initWithRequestName:@"SUBSCRIBE" andResponseName:@"MESSAGE"];
}

- (void) selectFor:(NSString *)selector andTicker:(NSString *)ticker{
        
    self.subscriptionID = [NSNumber numberWithInteger: _gistomp_subscribtion_id++];
        
    [self.request setHeader:@"destination" withValue:[[self class] destination]];

    if(selector){
        [self.request setHeader:@"selector" withValue:selector];
    }

    @try {
        self.market = [ticker componentsSeparatedByString:@"."][0];
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
        self.market = @"any";
    }
    
    [self.request setHeader:@"id" withValue:self.subscriptionID];
    
    self.stomponReadQueue = dispatch_queue_create("com.goinvestconnector.message.queue", DISPATCH_QUEUE_SERIAL);
}


@end

//
// SYSLOG
//
@implementation GIStompSyslog
+ (NSString*) destination{
    return @"syslog";
}
+ (id)select{
    GIStompSyslog *s=[[GIStompSyslog alloc] init];
    [s selectFor:nil andTicker:nil];
    return s;
}
- (void) setSession:(NSString *)sessionID{
    // nothing to do
}
@end

//
// Selectors
//
@implementation GIStompSelector
+ (id) select:(NSString *)ticker{
    id m=[[[self class] alloc] init];
    [m selectFor:[NSString stringWithFormat:@"ticker=\"%@\"", ticker] andTicker:ticker];
    return m;
}
@end

//
// ORDERBOOK
//
static NSInteger __gi_stomp_orderbook_depth = 0;
@implementation GIStompOrderbook

- (id) init{
    self = [super init];
    if (self) {
        [self setResponseQueueMaxSize:1];
    }
    return self;
}

+ (NSString*)destination{    return @"orderbooks"; }

+ (void) setDepth:(NSInteger)depth{
    __gi_stomp_orderbook_depth = depth;
}

- (void) messageReceived:(GICommonStompFrame *)iresponse{
    
    if (!iresponse)
        return;
    
    self.splitPosition = 0;

    if (__gi_stomp_orderbook_depth==0 || [iresponse.body countOfRows]<=__gi_stomp_orderbook_depth*2) {
        [super messageReceived: iresponse];
        self.splitPosition = [iresponse.body countOfRows]/2;
        return;
    }
    
    NSUInteger bids=0, asks=0, bids_ask_index=0;
    for (NSInteger i=0; i<[iresponse.body countOfRows]; i++) {
        BOOL isBid = [[iresponse.body stringForIndex:i andKey:@"BUYSELL"] isEqualToString:@"B"];
        if (isBid) {
            bids++;
            bids_ask_index++;
        }
        else
            asks++;
    }
        
    if (bids>__gi_stomp_orderbook_depth) {
        if (asks<__gi_stomp_orderbook_depth ) {
            bids = 2*__gi_stomp_orderbook_depth-asks;
        }else
            bids=__gi_stomp_orderbook_depth;
    }
    if (asks>__gi_stomp_orderbook_depth) {
        if (bids<__gi_stomp_orderbook_depth) {
            asks = 2*__gi_stomp_orderbook_depth-bids;
        }
        else
            asks=__gi_stomp_orderbook_depth;
    }
    
    self.splitPosition = bids_ask_index;
    NSArray *new_bid_asks = [iresponse.body.data subarrayWithRange:NSMakeRange(bids_ask_index-bids,bids+asks)];
    
    [iresponse.body setData: new_bid_asks];
    [super messageReceived:iresponse];
}

@end

//
// LAST TRADES
//
@implementation GIStompLastTrades
+ (NSString*) destination{ return @"lasttrades"; }
@end


//
// SECURITY
//
@implementation GIStompSecurity
+ (NSString*) destination{    return @"securities"; }

- (NSDictionary*) getRowByTicker:(NSString *)ticker{
    if (!self.lastResponse) return nil;
    return [self.lastResponse.body rowByKeyValue:ticker forColumn:@"TICKER"];
}
@end

//
// Candles
//
@implementation GIStompCandles
+ (NSString*) destination{    return @"candles"; }

+(id) select:(NSString *)ticker andInterval:(NSString*)interval{
    id m=[[[self class] alloc] init];
    [m selectFor:[NSString stringWithFormat:@"ticker=\"%@\" and interval=%@", ticker, interval] andTicker:ticker];
    return m;
}

@end


