//
//  GIStompBody.h
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>




typedef enum __gistomp_property_types {
    
    GISTOMP_UNKNOWN  = 0,
    GISTOMP_SNAPSHOT,
    GISTOMP_UPDATES,
    GISTOMP_REMOVAL,    
    
} GIStompPropertyTypes;

//
// STOMP BODY PROPERTIES
//
@interface GIStompBodyProperties : NSObject

//
// Response message type
//
@property(readonly) GIStompPropertyTypes type;

//
// Number of response
//
@property UInt64 seqNum;

//
// Create instance from dictionary prepared by JSON
//
- (id) initFromDictionary:(NSDictionary*)propDict;

//
// Return JSON object
//
- (NSDictionary*) JSONObject;

@end





//
// STOMP BODY
//
@interface GIStompBody : NSObject <NSCopying>

//
// Create instance from STOMP buffered Frame
//
- (id) initFromData:(NSData*)aData;

//
// STOMP body properties
//
@property(readonly) GIStompBodyProperties *properties;

//
// STOMP Body columns
//
@property(readonly) NSArray        *columns;

//
// Responsed raw data
//
@property(readonly) NSArray        *data;

- (void) setData:(NSArray *)aData;

//
// Return JSON prepared data
//
- (NSData*) dataWithJSONObject;

//
// Return object from row by column name. Returnetd object is converted to right formating form
//
- (id) valueForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue;
- (NSString*) stringForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue;
- (NSNumber*) numberForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue;

- (id) valueForIndex:(NSInteger)row andKey:(NSString*)columnName;
- (NSNumber*) numberForIndex:(NSInteger)row andKey: (NSString*)columnName;
- (NSString*) stringForIndex:(NSInteger)row andKey: (NSString*)columnName;

//
// Retrun row as a dictionary of values by columns
//
- (NSDictionary*) rowByIndex:(NSInteger)indexRow;

//
// Return row as a dictionary by key
//
- (NSDictionary*) rowByKeyValue:(NSString*)columnValue forColumn:(NSString*)columnName;

//
// Sizing
//
// Count of columns
- (NSUInteger) countOfColumns;

//
// Number of rows (data)
//
- (NSUInteger) countOfRows;

//
// Get math from data
//
- (id) maxValueForColumn: (NSString *)columnName;
- (id) minValueForColumn: (NSString *)columnName;


@end
