//
//  GIStompBody.m
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompBody.h"

@interface GIStompBodyProperties ()
@property GIStompPropertyTypes type;
@end

static NSString *_types_string[] = { [GISTOMP_REMOVAL]=@"removal", [GISTOMP_SNAPSHOT]=@"snapshot", [GISTOMP_UPDATES]=@"updates"};

//
// PROPERTIES
//
@implementation GIStompBodyProperties

@synthesize type, seqNum;


- (id) initFromDictionary:(NSDictionary *)propDict{
    if(!(self=[super init]))
        return nil;
    
    self.seqNum = 0;
    
    NSString *_type = [propDict[@"type"] lowercaseString];
    
    //NSLog(@"%s:%i", __FILE__, __LINE__);
    
    if ([_type isEqualToString:@"snapshot"]) {
        self.type = GISTOMP_SNAPSHOT;
    }
    else if ([_type isEqualToString:@"updates"]){
        self.type = GISTOMP_UPDATES;
    }
    else if ([_type isEqualToString:@"removal"]){
        self.type = GISTOMP_REMOVAL;
    }
    else {
        self.type = GISTOMP_UNKNOWN;
    }
    
    self.seqNum = [propDict[@"seqnum"] integerValue];
    
    return self;
}

- (NSDictionary*) JSONObject{
    return [[NSDictionary alloc]
            initWithObjectsAndKeys:
            _types_string[self.type], @"type",
            [NSNumber numberWithInteger:self.seqNum], @"seqnum",
            nil
            ];
}


- (NSString*) description{
    return [NSString stringWithFormat:@"{type: '%@', seqnum: %llu}", _types_string[self.type], self.seqNum];
}
@end




//
// BODY
//
@interface GIStompBody ()
@property NSArray        *columns;
@property NSMutableArray *data;
@property GIStompBodyProperties *properties;
@end

@implementation GIStompBody
{
    //
    // We received data but it does not mean that we should process it immediately
    // Data well be processed on demand
    //
    NSData *_rawData;
    
    // indexed access to data position at columns
    NSMutableDictionary *columnsIndex;
    
    NSNumberFormatter    *numberFormatter;
    NSMutableDictionary  *maxTable;
    NSMutableDictionary  *minTable;
    
    NSMutableDictionary  *ratios;
}

@synthesize data=_data, columns=_columns, properties=_properties;

- (id) copyWithZone:(NSZone *)zone{
    GIStompBody *_new_obj = [[[self class] allocWithZone:zone] init];
    
    if (_new_obj) {
        [self renderOnDemand];
        _new_obj->_columns = [self->_columns copy];
        _new_obj->_properties = self->_properties ;
        _new_obj->_rawData = nil;
        _new_obj->columnsIndex = [self->columnsIndex copy];
        _new_obj->maxTable = self->maxTable;
        _new_obj->maxTable = self->minTable;
        _new_obj->ratios = [self->ratios copy];
        _new_obj->numberFormatter = [self->numberFormatter copy];
        _new_obj->_data = [self->_data copy];
    }
    
    return _new_obj;
}

- (id) initFromData:(NSData *)aData{
    
    if(!(self=[super self]))
        return self;
    
    //
    // parse on demand
    //
    _rawData = aData;
    
    // clear
    minTable = maxTable = nil;
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"{\
            %@,\
            columns: %@,\
            data: %@ \
            }", self.properties, self.columns, self.data];
}

- (NSUInteger) countOfColumns{ return self.columns.count; }
- (NSUInteger) countOfRows{ return self.data.count; }

- (NSData*) dataWithJSONObject{
    if (self.data){
        NSError *_error;
        
        NSMutableDictionary *_dict = [[NSMutableDictionary alloc] init];
        
        [_dict setObject:[self.properties JSONObject] forKey:@"properties"];
        [_dict setObject:self.columns forKey:@"columns"];
        [_dict setObject:self.data forKey:@"data"];
        
        NSData  *_json = [NSJSONSerialization dataWithJSONObject: _dict options: 0 /*NSJSONWritingPrettyPrinted*/ error: &_error];
        
        return _json;
    }
    else
        return nil;
}

- (void) setColumns:(NSArray *)columns{
    _columns = columns;
}

- (void) setProperties:(GIStompBodyProperties *)properties{
    _properties = properties;
}

- (GIStompBodyProperties*) properties{
    [self renderOnDemand];
    return _properties;
}

- (NSArray*) columns{
    [self renderOnDemand];
    return _columns;
}

- (void) setData:(NSArray *)aData{
    _data = aData;
    maxTable = minTable = nil;
}

- (NSArray*) data {
    
    [self renderOnDemand];
    return _data;
}

- (void) renderOnDemand{
    if(_rawData){
        NSError *_error;
        
        
        NSMutableDictionary *_dict = [NSJSONSerialization JSONObjectWithData:_rawData options: NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments /*|NSJSONWritingPrettyPrinted*/ error:&_error];
        
        if (_error) {
            NSLog(@"%@", _error);
        }
        else{
            _properties = [[GIStompBodyProperties alloc] initFromDictionary:_dict[@"properties"]];
            _columns    = _dict[@"columns"];
            _data       = _dict[@"data"];
            
            if(_columns){
                NSUInteger index=0;
                if (!columnsIndex) {
                    columnsIndex = [NSMutableDictionary dictionaryWithCapacity:[_columns count]];
                }
                for (NSString *column in _columns) {
                    [columnsIndex setValue:[NSNumber numberWithUnsignedInteger:index] forKey:column]; ++index;
                }
            }
            maxTable = minTable = nil;
        }
        _rawData = nil;
    }
}

- (NSDictionary*) rowByIndex:(NSInteger)indexRow{
    @try {
        [self renderOnDemand];
        if ([self.data count]==0) {
            return nil;
        }
        NSMutableDictionary * _rowd = [NSMutableDictionary dictionaryWithCapacity:[self.data count]];
        for (NSInteger i=0; i<[self.columns count];i++) {
            NSString *_cn = self.columns[i];
            [_rowd setValue:[self stringForIndex:indexRow andColumn:i] forKey:_cn];
        }
        return _rowd;
    }
    @catch (NSException *exception) {
        return nil;
    }
}


- (NSInteger) indexWhereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    NSNumber *_keyColumnIndex = columnsIndex[key] ;
    //
    // ! this version uses slow search,  in a future version we should use index to search key position
    //
    [self renderOnDemand];
    for (NSInteger i=0; i<[self.data count] ;i++) {
        NSString *keyObj = [self.data[i] objectAtIndex:_keyColumnIndex.integerValue];
        if ([keyObj isEqualToString:keyValue]) {
            return i;
        }
        i++;
    }
    return -1;
}

- (id) valueForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    @try {
        NSNumber *_ci = columnsIndex[columnName];
        if (!_ci)
            return nil;
        return [self valueForIndex:[self indexWhereKey:key isEqualToString:keyValue]
                         andColumn:_ci.integerValue];
    }
    @catch (NSException *exception) {
        return nil;
    }
    return nil;
}

- (NSString*) stringForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    @try {
        
        NSNumber *_ci = columnsIndex[columnName];
        
        if (!_ci)
            return nil;
        
        NSInteger _i=[self indexWhereKey:key isEqualToString:keyValue];
        NSString *_ret = [self stringForIndex: _i
                                    andColumn:_ci.integerValue];
        return _ret;
    }
    @catch (NSException *exception) {
        return nil;
    }
    return nil;
}

- (NSNumber*) numberForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    @try {
        NSNumber *_ci = columnsIndex[columnName];
        if (!_ci)
            return nil;
        return [self numberForIndex:[self indexWhereKey:key
                                         isEqualToString:keyValue]
                          andColumn:_ci.integerValue];
    }
    @catch (NSException *exception) {
        return nil;
    }
    return nil;
}


- (NSDictionary*) rowByKeyValue:(NSString *)columnValue forColumn:(NSString *)columnName{
    @try {
        NSNumber *_columnIndex = columnsIndex[columnName] ;
        //
        // ! this version uses slow search,  in a future version we should use index to search key position
        //
        [self renderOnDemand];
        for (NSInteger i=0; i<[self.data count] ;i++) {
            NSString *key = [ [self.data objectAtIndex:i]  objectAtIndex:[_columnIndex integerValue]];
            if ([key isEqualToString:columnValue]) {
                return [self rowByIndex:i];
            }
            i++;
        }
    }
    @catch (NSException *exception) {
        return nil;
    }
    return nil;
}

- (id) valueForIndex:(NSInteger)row andKey: (NSString*)columnName{
    NSNumber *_columnIndex = columnsIndex[columnName] ;
    return [self valueForIndex:row andColumn:_columnIndex.integerValue];
}

- (id) valueForIndex:(NSInteger)row andColumn: (NSInteger)column{
    @try { ;
        return [[self.data objectAtIndex:row] objectAtIndex: column];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (NSString*) stringForIndex:(NSInteger)row andColumn:(NSInteger)column{
    id ret = [self valueForIndex:row andColumn:column];
    
        
    if(!ret) return nil;
    
    if ([ret isKindOfClass:[NSArray class]]) {
        if (!numberFormatter) {
            
            numberFormatter = [[NSNumberFormatter alloc] init];
            
            [numberFormatter allowsFloats];
            [numberFormatter setLocale:[NSLocale currentLocale]];
        }
        
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        [numberFormatter setAlwaysShowsDecimalSeparator:YES];
        int prec = 2;
        if ([ret count]==2) {
            prec = [ret[1] integerValue];
        }
        [numberFormatter setMaximumSignificantDigits:prec];
        [numberFormatter setMinimumSignificantDigits:1];
        [numberFormatter setMaximumFractionDigits:prec];
        [numberFormatter setMinimumFractionDigits:prec];
        
        return [numberFormatter stringFromNumber:ret[0]];
    }
    else if ( [ret isKindOfClass:[NSString class]])
        return ret;
    else if ( [ret isKindOfClass:[NSNumber class]])
        return [numberFormatter stringFromNumber:ret];
    
    return [numberFormatter stringForObjectValue:ret];
}

- (NSString*) stringForIndex:(NSInteger)row andKey:(NSString *)columnName{
    NSNumber *_columnIndex = columnsIndex[columnName];
    return [self stringForIndex:row andColumn:_columnIndex.integerValue];
}

- (NSNumber*) numberForIndex:(NSInteger)row andColumn:(NSInteger)columnIndex{
    id ret = [self valueForIndex:row andColumn:columnIndex];
    
    if(!ret) return nil;
    
    if ([ret isKindOfClass:[NSArray class]]) {
        return ret[0];
    }
    else if ([ret isKindOfClass:[NSString class]]){
        return [numberFormatter numberFromString:ret];
    }
    else if ([ret isKindOfClass:[NSNumber class]])
        return ret;
    else
        return nil;
}

- (NSNumber*) numberForIndex:(NSInteger)row andKey:(NSString *)columnName{
    NSNumber *_columnIndex = columnsIndex[columnName];
    return [self numberForIndex:row andColumn:_columnIndex.integerValue];
 }


- (id) compareData: (NSString*)columnName withOrder:(NSComparisonResult)compartion forTable: (NSMutableDictionary* __strong *) table{
    
    // check table
    if (!*table)
        *table = [NSMutableDictionary dictionaryWithCapacity:0];
    
    // find value
    id _value = [*table objectForKey:columnName];
    
    if (_value)
        // ok
        return _value;
    
    // not found
    
    if (_rawData) [self renderOnDemand];
    
    for (NSUInteger i=0; i < [_data count]; i++){
        id _next_value = [self numberForIndex:i andKey:columnName];
        if (!_value)
            _value = _next_value;
        else if ([_next_value compare:_value] == compartion)
            _value = _next_value;
    }
    // new value
    
    [*table setValue:_value forKey:columnName];
    
    return _value;
}

- (id) maxValueForColumn: (NSString *)columnName{
    return [self compareData:columnName withOrder:NSOrderedDescending forTable:&maxTable];
}

- (id) minValueForColumn: (NSString *)columnName{
    return [self compareData:columnName withOrder:NSOrderedAscending forTable:&minTable];
}


@end
