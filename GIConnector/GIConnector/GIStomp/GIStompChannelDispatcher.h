//
//  GIStompChannelDispatcher.h
//  GIConnector
//
//  Created by denn on 3/25/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIConfig.h"
#import "GIStomp.h"
#import "GICSPTunnel.h"

typedef enum {
    GI_STOMP_LOGIN_FAILED,
    GI_STOMP_MARKET_ABSENT,
    GI_STOMP_TICKER_ILLEGAL,
    GI_STOMP_ERROR
} GIStompDispatcherError;

//
// Handle errors
//
@protocol GIStompErrorDispatchHandler <NSObject>

- (void) didStompError: (GIError*)error;
- (void) didSystemError: (GIError*)error;

@end

@protocol GIStompLoginDispatchHandler <NSObject>

- (void) didStompLogin: (NSDictionary*)connection;
- (void) didStompLoginError: (GIError*)error withConnectionResponse: (GICommonStompFrame*)response;

@end

//
// Handle events
//
@protocol GIStompEventDispatchHandler <NSObject>

//
// Connection in progress, but not established
//
- (void) channelWillActive;

//
// Connection is opened
//
- (void) channelOpened;

//
// Connection closed
//
- (void) channelClosed;

//
// Connection start again
//
- (void) channelReconnecting;

//
// Connection restored
//
- (void) channelConnectionRestored;

@end


//
// STOMP Dispatcher manages stomp channels and handle subscribed messages
//
@interface GIStompChannelDispatcher : NSObject <GICSPTunnelHandler>

//
// Create instance
//
- (id)   initWithConfig: (GIConfig*)config;

//
// Make channel on market
//
- (void) connectOnMarket:(NSString*)market;
- (void) disconnectAllMarkets;

//
// Get instrument list from market
// 
- (GIStompTickers*) registerTickersOnMarket:(NSString*)market;

//
// Register message to dispatch. Every registered message response property will be updated when data will receive.
// If connection did not established yet create it and send subscribtion
//
- (void) registerMessage:(GIStompMessage*)message;

//
// Unregister (unsubscribe) message.
//
- (void) unregisterMessage:(GIStompMessage*)message;

//
// Stop all subscription and destroy connection
//
- (void) stop;

//
// Start new dispatcher
//
- (void) start;

//
// Error handler
//
@property id <GIStompErrorDispatchHandler> errorHandler;

//
// Events handler
//
@property id <GIStompEventDispatchHandler> eventHandler;

//
// Login handlers
//
@property id <GIStompLoginDispatchHandler> loginHandler;

@end
