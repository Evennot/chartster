//
//  GIStompChannelDispatcher.m
//  GIConnector
//
//  Created by denn on 3/25/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompChannelDispatcher.h"
#import "GIAuth.h"

static NSArray *__invalidConnectionErrors=nil;


static NSArray *invalidConnectionErrors(){
    if (!__invalidConnectionErrors)
        __invalidConnectionErrors = @[@"InvalidUser", @"InvalidBroker", @"InvalidPwd"];
    return __invalidConnectionErrors;
}

@interface GIStompChannel : NSObject

@property NSString             *channelID;
@property NSString             *sessionID;
@property NSMutableDictionary  *dispatchedRequest;
@property GIAuth               *login;
@property BOOL                 isConnected;

@end


@implementation GIStompChannel

@synthesize sessionID, login, channelID;

- (id) init{
    if (!(self = [super init])) {
        return nil;
    }
    self.dispatchedRequest  = [NSMutableDictionary dictionaryWithCapacity:1];
    self.isConnected = NO;
    return self;
}

@end

@interface GIStompChannelDispatcher ()
@property NSMutableDictionary  *channels;
@property NSMutableDictionary  *dispatchedMessages;
@end

@implementation GIStompChannelDispatcher
{
    GIConfig             *config;
    GICSPTunnel          *csp;
    
    GIStompConnect      *connectionStomp;
    NSMutableArray      *connectionQueue;
    NSMutableArray         *messageQueue;
    dispatch_queue_t    stomponReadQueue;
}

- (id) initWithConfig:(GIConfig *)aConfig{

    if(!(self=[super init]))
        return nil;
    
    config = aConfig;
    
    connectionQueue = [NSMutableArray arrayWithCapacity:1];
    messageQueue = [NSMutableArray arrayWithCapacity:1];
    
    self.dispatchedMessages = [NSMutableDictionary dictionaryWithCapacity:1];

    stomponReadQueue = dispatch_queue_create("com.goinvestconnector.stomp.queue", DISPATCH_QUEUE_CONCURRENT);

    [self start];
    
    return self;
}

- (void) start{
        
    NSLog(@"Start Channel dispatcher");
    
    self.channels =[NSMutableDictionary dictionaryWithCapacity:1];

    BOOL _started = NO;
    if (csp) {
        _started = YES;
    }
        
    csp = [[GICSPTunnel alloc] initWithGIConfig:config];
    csp.connectionHandler = self;
    
    [csp startAsync];
    
    if (_started) {
        NSMutableDictionary *_md = [self.dispatchedMessages copy];
        for (id mess in _md) {
            GIStompMessage *messObj = self.dispatchedMessages[mess];
            [self.dispatchedMessages removeObjectForKey:mess];
            [self registerMessage:messObj];
        }        
    }
}

- (void) stop{
    [csp cancel];
}

- (void) registerMessage:(GIStompMessage *)message{// onMarket:(NSString *)market{
    
    //
    // prepare connection frame
    //
    [self prepareConnectionToChannel: message.market];
    
    //
    // if message with the same id is subcribed - ignore
    //
    if (self.dispatchedMessages[message.subscriptionID]) {
        return;
    }
    
    //
    // prepare deferred send
    //
    NSArray *_deferred_mess = @[message, message.market];
    [messageQueue addObject:_deferred_mess];
    
    if ([csp isOpened]) {
        //
        // if alreadey opened
        //
        [self sendDeferredMessages];
    }
}

- (void) unregisterMessage:(GIStompMessage *)message{
    GIStompMessage *messObj = self.dispatchedMessages[message.subscriptionID];
    if (messObj){
        GIStompUnsubscriber *unsubscriber = [GIStompUnsubscriber makeForMessage:messObj];        
        [csp send: [unsubscriber.request stompFrame]];
        [self.dispatchedMessages removeObjectForKey:message.subscriptionID];
    }
}

- (void) sendDeferredMessages{
    
    while ([messageQueue count]) {
        NSArray *_deferred_mess = [messageQueue objectAtIndex:0];
        if (_deferred_mess) {
            
            //
            // get channel for the market
            //
            GIStompChannel *_channel = self.channels[_deferred_mess[1]];
            
            //
            // set session id for the channel
            //
            GIStompMessage *_mess = _deferred_mess[0];
            [_mess setSession:_channel.sessionID];
            
            //
            // if message already dispatched - ignore
            //
            if (self.dispatchedMessages[_mess.subscriptionID]) {
                [messageQueue removeObjectAtIndex:0];
                continue;
            }
            
            //
            // add to dispatch set
            //
            [self.dispatchedMessages setObject:_mess forKey:_mess.subscriptionID];
            
            //
            // send frame
            //
            //NSLog(@"Send: %@", _mess.request);
            [csp send:[_mess.request stompFrame]];
        }
        
        [messageQueue removeObjectAtIndex:0];
    }
}

//
// When phisical connection is created we try to open a channel annoced to connect ("deferred" channel connection)
//
//
- (void) sendConnectionFrames{  
    while ([connectionQueue count]) {
        
        //
        // get a deferred connection packet
        //
        NSArray  *_deferred_packet = [connectionQueue objectAtIndex:0];
        
        if(_deferred_packet){
            
            //
            // get channel market
            //
            NSString       *market  = _deferred_packet[1];
            GIStompChannel *channel = self.channels[market];            
            GIAuth           *login = config.login;            
            if (channel.login && [channel.login.user isEqualToString:login.user] && channel.sessionID) {
                continue;
            }
            else{
                //
                // send if it can
                //
                if (self.eventHandler) {
                    [self.eventHandler channelWillActive];
                }
                
                //NSLog(@"SEND CONNECTION: %@: %@ with login: %@", market, channel.channelID, channel.login);
                
                [csp send:_deferred_packet[0]];
            }            
        }
        //
        // remove from queue
        //
        [connectionQueue removeObjectAtIndex:0];
    }
}

- (GIStompTickers*) registerTickersOnMarket:(NSString *)market{
    
    GIStompChannel * _channel = self.channels[market];
    
    //
    // set new session id
    //
    _channel.sessionID = [connectionStomp sessionID];        
    
    //
    // request tickers for the channel
    //
    
    GIStompTickers *_list = [GIStompTickers select:nil onMarket:market];
    
    [_channel.dispatchedRequest setObject:_list forKey:_list.requestID];
    
    //[csp send:[_list.request stompFrame]];
    
    return _list;
}


- (void) connectOnMarket:(NSString *)market{
    [self prepareConnectionToChannel:market];
}

- (void) disconnectAllMarkets{
    [self.channels removeAllObjects];
    
    GIStompDisconnect *_disconnect = [GIStompDisconnect make];
    [csp send:[_disconnect.request stompFrame]];
    
    [connectionQueue removeAllObjects];
    [self.dispatchedMessages removeAllObjects];
    [messageQueue removeAllObjects];
    
    sleep(1);
}

- (void) disconnectChannel: (GIStompChannel*)channel{
    
    [connectionQueue removeAllObjects];

    //
    // We should disconnect with old user
    //
    
    GIStompDisconnect *_disconnect = [GIStompDisconnect make];
    
    NSLog(@"SEND DISCONNECT: %@", _disconnect);
    
    [csp send:[_disconnect.request stompFrame]];
    
    //
    // wait !
    //
    sleep(1);
    
    //
    // the first version of the protocol always disconnects from all channels
    //
    
    NSMutableArray *rkeys = [NSMutableArray arrayWithCapacity:1];
    for (NSString *mName in self.channels) {
        GIStompChannel *_ch = self.channels[mName];
        
        if ([_ch.channelID isEqualToString:@"any"])
            // nothing to do
            continue;
        //
        // set for all channels
        //
        _ch.isConnected = NO;
        
        if ([_ch.channelID isEqualToString:channel.channelID]) {
            NSLog(@"Remove channel from connected channels: %@ on market %@", _ch.channelID, mName);
            [rkeys addObject:mName];
        }
    }
    
    if ([rkeys count]>0) {
        [self.channels removeObjectsForKeys:rkeys];
    }
    
    //
    // Finally  we should reconnect others
    //
    for (NSString *mName in self.channels) {
        GIStompChannel *_ch = self.channels[mName];
        
        if ([_ch.channelID isEqualToString:@"any"])
            continue;
                
        NSString     *_user_prefix = [config getUserPrefixByChannel:_ch.channelID];
        connectionStomp = [GIStompConnect toChannel:_ch.channelID withLogin: [NSString stringWithFormat:@"%@%@", _user_prefix, config.login.user] andPasscode: config.login.password];
        //
        // Create stomp frame
        //
        
        NSLog(@"Reconnect another channel: %@ on the market: %@", _ch.channelID, mName);
        
        [connectionQueue addObject:@[[connectionStomp.request stompFrame], mName, _ch.channelID]];
    }
}

- (GIStompChannel*) getChannel: (NSString*)market{
    GIStompChannel *_channel = self.channels[market];
    NSString     *_channelID   = [config getChannelByMarket:market];
    if(!_channel){
        [self.channels setValue:[[GIStompChannel alloc] init] forKey:market];
        
        _channel = self.channels[market];

        _channel = self.channels[market];
    }
    //
    // set uniq channel ID
    //
    _channel.channelID = _channelID;
    _channel.login = [config.login copy];
    return _channel;
}

//
// Prepare connection and make a logical channel to a marketplace instance of goInvest server which associated with the channel.
//
- (void) prepareConnectionToChannel:(NSString*)market{
    //
    // get a channel by the market
    //
    NSString     *_channelID   = [config getChannelByMarket:market];

    for (NSArray *defm in connectionQueue) {
        NSString *cID = defm[2];
        if ([cID isEqualToString:_channelID]) {
            // we already preapare connection frame to send
            NSLog(@" ##### We already prepare connection frame for channel : %@ on the narket %@", _channelID, market);
            return;
        }
    }

    GIStompChannel  *channel = self.channels[market];

    if ([_channelID isEqualToString:@"any"]) {
        //
        // it does not need to send connection
        //
        NSLog(@"Connection does not need for market: %@", market);
        return;
    }
    
    if (channel && [channel.login.user isEqualToString:config.login.user] && [channel.login.password isEqualToString:config.login.password] && channel.isConnected) {
        //
        // Already created
        //
        NSLog(@"Already connected to %@ by user %@ and password %@ session: %@", market, channel.login.user, channel.login.password, channel.sessionID);
        if (self.loginHandler) {
            dispatch_async(stomponReadQueue, ^(void){
                [self.loginHandler didStompLogin:@{@"channel": channel.channelID, @"market": market, @"session": channel.sessionID}];
                //[self.loginHandler didStompLogin:@{@"channel": channel.channelID, @"market": market}];
            });
        }
        return;
    }
    else if (channel && channel.isConnected){
        //
        // reset login
        //
        [self disconnectChannel:channel];
    }
    
    //
    // Create new request to connect to the market
    //
    channel = [self getChannel:market];
    
    //
    // Add deferred request for the only one market which uses the same channel
    //
    NSString     *_user_prefix = [config getUserPrefixByChannel:channel.channelID];
    connectionStomp = [GIStompConnect toChannel:_channelID withLogin: [NSString stringWithFormat:@"%@%@", _user_prefix, channel.login.user] andPasscode: channel.login.password];
    
    //
    // Create stomp frame
    //
    [connectionQueue addObject:@[[connectionStomp.request stompFrame], market, _channelID]];
    
    if ([csp isOpened]) {
        [self onOpen:nil];
    }
}

- (void) onOpen:(NSData *)data{
//    NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
//    
//    NSLog(@"ON OPEN ..... %@", _buffer);   
    
    if (self.eventHandler) {
        [self.eventHandler channelConnectionRestored];
    }
    
    [self sendConnectionFrames];
}


- (void) onRead:(NSData *)data{
    //
    // separate stomp blocks
    //
    // find \0 - end of stomp marker
    //
    __block NSInteger _block_marker_index = 0;
    for (NSInteger i=0; i<[data length]; i++) {
        
        if ([csp isCancelled]) {
            return;
        }

        char _the_end_of_stopm;
        [data getBytes:&_the_end_of_stopm range:NSMakeRange(i, 1)];
        
        if (_the_end_of_stopm=='\0') {
            NSData *_block = [data subdataWithRange:NSMakeRange(_block_marker_index, i-_block_marker_index)];
            _block_marker_index = i+1;
            if (_block) {
                dispatch_async(stomponReadQueue, ^(void){
                    [self postRead:_block];
                });
            }
        }
    }
}

- (void) sendDeferredRequests: (GIStompChannel*)channel{
    
    for (NSNumber *_k in channel.dispatchedRequest) {
        GIRequestStompFrame *_rq = channel.dispatchedRequest[_k];
        //NSLog(@"Send request request: %@ for channel: %@ on the market: %@", _rq, channel.channelID, [config getMarketByChannel:channel.channelID]);
        [csp send:[_rq.request stompFrame]];
    }
    
}


- (void) markMarketConnected: (GIStompChannel*)channel{
    //
    // find the same channel id market and mark the market connected too
    //
    for (NSDictionary *marketPlace in config.marketPlaces) {
        NSString *cID = marketPlace[@"channel"];
        NSString *mName = marketPlace[@"id"];
        
        if ([cID isEqualToString:channel.channelID] || [cID isEqualToString:@"any"]) {
            
            GIStompChannel *_channel = self.channels[mName];
            if (!_channel) {
                //
                // if not prepared
                //
                
                //NSLog(@"Market %@ has not been prepared, make new", mName);
                
                [self.channels setValue:[[GIStompChannel alloc] init] forKey:mName];
                _channel = self.channels[mName];
                _channel.channelID = cID;
                _channel.login = [config.login copy];
            }

            //
            // mark connected
            //
            _channel.isConnected = YES;
            _channel.sessionID = channel.sessionID;
            
            //NSLog(@"Mark market %@ already connected by the reason that we have had connection to the same channel: %@", mName, cID);
            
            //
            // send deferred requests for the market
            //
            [self sendDeferredRequests:_channel];
        }
    }
}

- (void) postRead:(NSData *)data{
    
    if ([csp isCancelled]) {
        return;
    }
    
    GICommonStompFrame *rp = [[GICommonStompFrame alloc] initWithData:data];
    
    //NSLog(@" ---   READ  --- %@", rp);
    
    if ([rp.name isEqualToString:@"ERROR"]) {
        
        NSString *_message    = rp.headers[@"message"];                
        NSString *_error_code = rp.headers[@"error-code"];


        for (NSString *err_code in invalidConnectionErrors()) {
            
            if ([err_code isEqualToString:_error_code]) {

                NSString *_channelid = rp.headers[@"channel"];
                NSString *_mName = [config getMarketByChannel:_channelid];
                
                GIStompChannel *_ch = self.channels[_mName];

                
                if (_ch.isConnected) {
                    //
                    // connection established, but CONNECT command had been sent before with some invalid parameters
                    // just ignore it
                    //
                    //NSLog(@"Connection established before channel %@/%i receive previous connection event", _ch.channelID, _ch.isConnected);
                    return;
                }
                
                NSError *_error = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                                      code:GI_STOMP_LOGIN_FAILED 
                                                  userInfo:@{
                          NSLocalizedFailureReasonErrorKey: @"Connection failed",
                                 NSLocalizedDescriptionKey: [NSString  stringWithFormat: GILocalizedString(@"Connection failed: %@", @"Stomp error"), _message]
                                   }];
                
                GIError *_err = [[GIError alloc] initWithNSError:_error];
                
                
                if (self.loginHandler) {
                    dispatch_async(stomponReadQueue, ^(void){
                        [self.loginHandler didStompLoginError:_err withConnectionResponse:rp];
                    });
                    return;
                }
 
                //
                // find channel and remove
                //
                for (NSString *m in self.channels) {
                    GIStompChannel  *channel = self.channels[m];
                    if ([channel.channelID isEqualToString:_channelid]) {
                        [self.channels removeObjectForKey:m];
                        NSLog(@"Remove channel: %@", m);
                        break;
                    }
                }
            }
        }
        
        if (self.errorHandler) {            
            NSError *_error = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                                  code:GI_STOMP_ERROR 
                                              userInfo:@{
                      NSLocalizedFailureReasonErrorKey: @"Stomp error",
                             NSLocalizedDescriptionKey: [NSString  stringWithFormat: GILocalizedString(@"Stomp error has occured: %@", @"Stomp error"), _message]
                               }];
            
            GIError *_err = [[GIError alloc] initWithNSError:_error];
            [self.errorHandler didStompError: _err];
        }
        return;
    }
    else if ([rp.name isEqualToString:@"CONNECTED"]) {
        
        //
        // set connection atributes, such as session id channel id
        //
        
        [connectionStomp messageReceived: rp];
        NSString *_channelID      = [connectionStomp responsedChannelID];
        
        //
        // get for the channel id configured market
        //
        NSString *marketName      = [config getMarketByChannel:_channelID];  //[self findMarketByChannel:_channelID];
        
        //
        // get the channel
        //
        GIStompChannel * _channel = [self getChannel:marketName];
        
        //
        // set new session id
        //
        _channel.sessionID = [connectionStomp sessionID];        
        
        _channel.isConnected = YES;
        
        if (self.eventHandler) {
            [self.eventHandler channelOpened];
        }
        
        if (self.loginHandler) {
            dispatch_async(stomponReadQueue, ^(void){
                [self.loginHandler didStompLogin:@{@"channel": _channel.channelID, @"market": marketName, @"session":_channel.sessionID}];
            });
        }
        
        //
        // find the same channel id market and mark the market connected too
        //
        [self markMarketConnected:_channel];
        
        //
        // send deferred requests
        //
        
        [self sendDeferredRequests:_channel];
        
        //
        // send deferred subscribes
        //
        [self sendDeferredMessages];
    }
    else if( [rp.name isEqualToString:@"REPLY"] && [rp.headers[@"destination"] isEqualToString:[GIStompTickers destination]]){
        //
        // append to channle new tickers
        //
        
        for (NSString *m in self.channels) {
            
            GIStompChannel *_channel = self.channels[m];
            NSNumber *_requestID = [NSNumber numberWithInteger: [rp.headers[@"request-id"] integerValue]];
            
            GIRequestStompFrame *req=_channel.dispatchedRequest[_requestID];
            
            if ([req.requestID isEqualToNumber:_requestID]) {
                [req messageReceived:rp];
                
                if (req.delegate) {
                    dispatch_async(stomponReadQueue, ^(void){
                        [req.delegate didFrameReceive:req];
                    });
                }
                
                //
                // remove responsed request
                //
                [_channel.dispatchedRequest removeObjectForKey:_requestID];
            }
        }
    }
    else if([rp.name isEqualToString:@"MESSAGE"]){
        //
        // get subscription id
        //
        NSNumber        *_subscriptionID = [NSNumber numberWithInteger:[rp.headers[@"subscription"] integerValue]];
                
        //
        // get message if it is dispatched
        //
        GIStompMessage  *_mess = self.dispatchedMessages[_subscriptionID];
        
        if (_mess) {
            //
            // set new responsed data
            //

            [_mess messageReceived:rp];
            
            if (_mess.delegate) {
                
                dispatch_async(stomponReadQueue, ^(void){
                    [_mess.delegate didFrameReceive:_mess];
                });
                
                
            }            
        }
    }
}

- (void) onPing:(NSData *)data roundTrip:(NSTimeInterval)roundTrip{
    NSLog(@"Ping roundtrip: %f", roundTrip);
}


- (void) onError:(GIError *)error in:(GICSPCommand)command{
    if (command == GICSP_RECEIVE) {
        //NSLog(@"Reconnect receiver");
        if (self.eventHandler) {
            [self.eventHandler channelReconnecting];
        }
    }
    else {
        NSLog(@"%@", error);
        if ([error.domain hasSuffix:@"http.status"] && error.code==410) {
            //
            // lost connection
            //
            [self stop];
            sleep(1);
            [self start];
        }
        else
        if ([error.domain hasPrefix:GI_ERROR_DOMAIN_PREFIX]) {
            if (self.errorHandler) 
                [self.errorHandler didSystemError:error];
        }
    }
}


- (BOOL) isDataCompleted:(NSData *)chainedData{
    if(!chainedData) return YES;
    if([chainedData length]==0) return YES;
    char _the_end_of_stopm;
    [chainedData getBytes:&_the_end_of_stopm range:NSMakeRange([chainedData length]-1, 1)];
    return _the_end_of_stopm == '\0';
}


- (void) onClose:(NSData *)data{
    if (self.eventHandler) {
        [self.eventHandler channelClosed];
    }
}

- (void) onConnect:(NSData *)data{}

- (void) onSend:(NSData *)data{

    //GICommonStompFrame *rp = [[GICommonStompFrame alloc] initWithData:data];
    //NSLog(@"ON SEND: %@", rp);
    //NSLog(@"%@", [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy]);

}

@end
