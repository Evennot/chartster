//
//  GITestConnector.h
//  GICTestApp
//
//  Created by denn on 3/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIStomp.h"
#import "GICSPTunnel.h"

//
// Echange between object and dispatcher protocol
//
@protocol GICDispatchProtocol <NSObject>
- (void)  updateView:(GIStomp*)frame withDestination: (NSString*)destination;
@end

//
// Echange between object and dispatcher protocol
//
@protocol GICDispatchFrameProtocol <NSObject>
- (void)  updateFrame:(GIStomp*)frame withDestination: (NSString*)destination;
@end


@interface GIStompDispatcher : NSObject <GICSPTunnelHandler>

//
// get shared dispatcher instance
//
+ (GIStompDispatcher*) sharedInstance;

//- (void) suspend;
//- (void) resume;


//
// Register STOMP Frame object to dispatch
//


//
// Subscribe an object which support GICDispatchProtocol on message Class 
// with a ticker which replies through the channel defined by sharedInstance.
// Returns stomp subscription ID sent to channel. Afterwards, we can use it to unsubscribe replies.
//
- (NSInteger) subscribe:(id<GICDispatchProtocol>)object onMessage:(Class)messageClass withTicker:(NSString*)ticker onChannel:(NSString*)channel ;

//
// Unsubscribe message by subscriptionID
//
- (void) unsubscribe:(NSInteger)subscriptionID;

@end
