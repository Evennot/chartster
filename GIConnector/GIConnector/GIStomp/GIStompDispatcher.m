//
//  GITestConnector.m
//  GICTestApp
//
//  Created by denn on 3/23/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GIStompDispatcher.h"
#import "GIConfig.h"
#import "GICSPTunnel.h"

//#define GI_TEST_APP_URL @"https://goinvest.micex.ru"
//#define channelLogins  @{ @"1": @[@"MXSE.MU0000600010", @""]}

#define GI_TEST_APP_URL @"https://goinvest2.beta.micex.ru"
#define channelLogins  @{ @"1": @[@"vasya@superbroker", @"12345"]}

#define marketPlaces  @{@"1": @{@"name": @"MXSE", @"description": @"Фондовый рынок МБ"}};

//
// It contains session atributes wich will dispatch per channel
//
@interface GICChannel : NSObject 

//
// session id
//
@property NSString            *sessionID;

//
// The ... contains references to all object which were rehistered to dispatch,
// only registered object will receive messages that main dispatcher can send  to the object
//
@property NSMutableDictionary *dispatchedObjects;    

//
// if we request ticker list - store tickers
//
@property GIStompTickers      *tickers;

//
// request counter
//
@property (nonatomic)  NSInteger   requestCounter;

//
// logged user
//
@property GIAuth *login;

//
// marketplace name
//
@property NSString *marketplace;

@end

@implementation GICChannel

- (id) init{
    if (!(self=[super init])) {
        return nil;
    }    
    self.dispatchedObjects = [NSMutableDictionary dictionaryWithCapacity:0];   
    self.sessionID   = nil;
    
    self.requestCounter = 0;
    
    return self;
}

- (NSInteger) requestCounter{
    return _requestCounter++;
}

@end


//
// Go invest dispatcher make the follows
// 1. create one connection to server
// 2. multiplex channels to the connect
// 3. receive messages
// 4. dispatch messages by received attributes
// 5. resend messages to view instances registered to receive (subscribe) those msaages
//
@interface GIStompDispatcher()

//
// Channels are opened by subscribers. 
// Each channel keeps current session and dictionary of objects receive messages which they subscibed
//
@property(strong) NSMutableDictionary *channels;    

@end


@implementation GIStompDispatcher
{    
    GIConfig            *config;
    GICSPTunnel         *csp;
    
    NSMutableArray      *connectionQueue;
    NSMutableArray      *messagesQueue;
}

//
// Make singletone instance for the current process
//
static GIStompDispatcher *sharedConnector = nil;

+ (GIStompDispatcher*) sharedInstance{
    if(!sharedConnector){
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            sharedConnector = [[GIStompDispatcher alloc] init];
        });
    }            
        
    return sharedConnector;
}

- (id) init{
    
    if (sharedConnector) {
        return sharedConnector;
    }
    
    if(!(self=[super init]))
        return nil;
    
    config = [[GIConfig alloc] initWithUrl: GI_TEST_APP_URL];
    
    config.validatesSecureCertificate = NO;

    //
    // SHOULD BE REPLACED
    //
    config.logins = [[GILogins alloc] initWithDictionary:channelLogins];
    
    self.channels = [NSMutableDictionary dictionaryWithCapacity:0];
    
    connectionQueue = [NSMutableArray arrayWithCapacity:1];
    messagesQueue   = [NSMutableArray arrayWithCapacity:1];
    
    csp = [[GICSPTunnel alloc] initWithGIConfig:config];
    
    csp.connectionHandler=self;
    
    [csp startAsync];
    
    return self;
}

//
// When phisical connection is created we try to open a channel annoced to connect ("deferred" channel connection)
// 
//
- (void) sendConnectionFrames{

    NSLog(@"Send connection frame  qu=%i",[connectionQueue count]);

    while ([connectionQueue count]) {
        
        NSArray  *_deferred_packet = [connectionQueue objectAtIndex:0];
        
        if(_deferred_packet){
            
            NSString   *channelID = _deferred_packet[1];
            GICChannel *channel = self.channels[channelID];
            GIAuth     *login   = [config.logins channelLogin:channelID];
            
            if (channel.login && [channel.login.user isEqualToString:login.user]) {
                NSLog(@"Worning: user:%@ has already connected to %@ channel", login.user, channelID);
                continue;
            }
            else{
                [csp send:_deferred_packet[0]];
            }
            
        }
        
        [connectionQueue removeObjectAtIndex:0];        
    }
}

//
// Prepare connection and make a logical channel to a marketplace instance of goInvest server which associated with the channel.
//
- (void) prepareConnectionToChannel: (NSString*)channelID{
    
    GIAuth     *login   = [config.logins channelLogin:channelID];       
    
    NSData  *frame            = [[GIStompConnect connectChanenlId:1 withLogin:login.user andPasscode:login.password] stompFrame];
    NSArray *_deferred_packet = @[frame,channelID];
    
    [connectionQueue addObject:_deferred_packet];
    
    if ([csp isOpened]) {
        [self onOpen:nil];        
    }
}

//
// tunnel protocol handler, we received  message that we just estableshed connection to tunnel and waiting for messages
//
- (void) onOpen:(NSData *)data{        
    //
    // When connection established
    //
    [self sendConnectionFrames];   
    
    [self sendDeferredSubscription];
}

- (void) unsubscribe:(NSInteger)subscriptionID{    
}

//
// Prepare subscription and send if we can
//
- (NSInteger) subscribe:(id<GICDispatchProtocol>)object onMessage:(Class)messageClass withTicker:(NSString *)ticker onChannel:(NSString *)channel{
    
    static NSInteger subscriptionCounter = 0;
    
    //
    // if connection has not been created
    //
    if (!sharedConnector.channels[channel]){
        [sharedConnector.channels setValue: [GICChannel new] forKey:channel];
        [sharedConnector prepareConnectionToChannel:channel];
    }
 
    NSString *_destination = [messageClass destinationName];
    
    NSLog(@" SET DEST : %@", _destination);
    
    GICChannel *_session = self.channels[channel];
    
    [_session.dispatchedObjects setValue:object forKey:[messageClass destinationName]];
            
    NSInteger sID = subscriptionCounter++;
    
    if (_session.sessionID) {
        GIStompMessage *message = [[messageClass class] subscibe:ticker withSession:_session.sessionID andSubscriptionID:sID];
        //
        // sync localcoun
        //
        [csp send: [message stompFrame]];
    }
    else{
        NSLog(@"Error: connection has not been esteblished!");
        NSNumber * sc = [NSNumber numberWithInteger:sID];
        NSArray *_defered = @[messageClass, ticker, channel,  sc ];
        [messagesQueue addObject:_defered];
    }
    
    return sID;
}

//
// Send deferred subscription
//
- (void) sendDeferredSubscription{
    NSLog(@" ONOPEN  command qu=%i",[messagesQueue count]);
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        NSTimeInterval _start = [NSDate timeIntervalSinceReferenceDate];
        
        while ([messagesQueue count] && config.connectionTimeout >= ( [NSDate timeIntervalSinceReferenceDate] - _start) ) {
            
            NSArray  *_defered = [messagesQueue objectAtIndex:0];        
            
            Class     _class_message = _defered[0];
            NSString *_ticker = _defered[1];
            NSString *_channel = _defered[2];
            NSInteger _sibscID = [_defered[3] integerValue];
            GICChannel *_session = self.channels[_channel];
            
            //
            // вставить проверку на повторный коннект от юзера на канал
            //
            
            if (_session.sessionID) {
                
                NSLog(@" SEND DEFERED: %@", _defered);
                
                [csp send: [[[_class_message class] subscibe:_ticker withSession:_session.sessionID andSubscriptionID: _sibscID] stompFrame]];
                
                [messagesQueue removeObjectAtIndex:0];            
            }
            else
                sleep(1);
            
        }
    });
}


//
// Read tunnel replied messages
//
- (void) onRead:(NSData *)data{
    
    for (int i =0; ![csp isOpened] && i<10; i++) {
        NSLog(@"Session is not opened yet");
        sleep(1);
    }
    
    
    
    GIStomp  *_rp = [[GIStomp alloc] initWithData:data];
    
    if ([_rp.name isEqualToString:@"CONNECTED"]) {
        
        NSString      *_stompSession = _rp.headers[@"session"];
        NSString      *_channelID = _rp.headers[@"channel"];
        
        
        GICChannel *_session = self.channels[_channelID];
        
        NSLog(@"rp=%@\n stopSess=%@ channel=%@ realsess=%@", _rp, _stompSession, _channelID, _session.sessionID);
        
        if (_session.sessionID) {
            return;
        }        
        
        _session.sessionID = _rp.headers[@"session"];
        
        //
        // set login we used when send connection frame
        //
        _session.login = [config.logins channelLogin:_channelID];
        
        //
        // send request to get instruments listed for the channel
        //
        [csp send: [[GIStompTickers select: nil 
                                  onMarket: @"MXSE" 
                               withSession: _session.sessionID 
                                 onChannel: _channelID 
                             withRequestID: _session.requestCounter] 
                    stompFrame]];
        
        return;
    }
    else if ([_rp.name isEqualToString:@"REPLY"] && [_rp.headers[@"destination"] isEqualToString:@"list"] ){
        
        GICChannel *_session = self.channels[[_rp.headers[@"request-id"] componentsSeparatedByString:@":"][0]];
        _session.tickers = [[GIStompTickers alloc] initWithData:data];
        
        return;
    }
    else if ([_rp.name isEqualToString:@"MESSAGE"]){
        NSLog(@" RPPP %@", _rp);
    }
    
    //NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSUTF8StringEncoding];
    
    
    //    if(!stompSession){        
    //        NSLog(@"New STOMP connection received");
    //        GIStompFrame  *rp = [[GIStompFrame alloc] initWithData:data];
    //        stompSession = rp.headers[@"session"];
    //        [csp send: [[GIStompTickers select:nil onMarket:@"MXSE" withSession:stompSession] stompFrame]];
    //        [csp send: [[GIStompSyslog subscibe] stompFrame]];
    //    }
    //    else{
    //        GIStompFrame  *rp = [[GIStompFrame alloc] initWithData:data];
    //
    //        
    //        if ([rp.name isEqualToString:@"MESSAGE"] && rp.body.data) {
    //            
    //            NSString *destination = rp.headers[@"destination"];
    //            id <GICDispatchProtocol> disp = dispatchers[destination];
    //
    //            NSLog(@"> received rows %i<", [rp.body countOfRows]);
    //            NSLog(@"> received dest %@<", destination);
    //            NSLog(@"> received disp %@<", disp);
    //
    //            if (disp) {
    //                dispatch_async(dispatch_get_main_queue(), ^(void){
    //                    [disp updateView:rp withDestination:destination];
    //                });
    //            }
    //            
    //        }
    //    }    
}

- (void) onClose:(NSData *)data{
}


- (void) onError:(GIError *)error in:(GICSPCommand)command{
    if (command == GICSP_RECEIVE) {
        NSLog(@"Reconnect receiver");
    }
    else{
        //NSLog(@" -------- Error: in %i ---------- \n\n %@ ", command, error);
        //stompSession = nil;
    }
}

- (void) dealloc{
    NSLog(@" dealloc ");
    [csp cancel];
}



#define orderbooks  @"\
MESSAGE\n\
destination: orderbooks\n\
message-id:  123\n\
subscription: 2345\n\
session-id: %@\n\
\n\
{\
\"properties\":{\"type\": \"snapshot\", \"seqnum\": 1},\n\
\"columns\": [\"BYSELL\",\"BYSELL_TEXT\",\"PRICE\", \"QUANTITY\", \"Q\"],\
\"data\": [\
[\"B\",\"B\",[101.07,2], 245, \"Q\"],\
[\"B\",\"B\",[101.06,2], 245, \"Q\"],\
[\"B\",\"B\",[101.05,2], 245, \"Q\"],\
[\"B\",\"B\",[101.04,2], 245, \"Q\"],\
[\"B\",\"B\",[101.03,2], 245, \"Q\"],\
[\"B\",\"B\",[101.02,2], 245, \"Q\"],\
[\"B\",\"B\",[101.01,2], 245, \"Q\"],\
[\"B\",\"B\",[100.9,2], 245, \"Q\"],\
[\"B\",\"B\",[100.8,2], 245, \"Q\"],\
[\"B\",\"B\",[100.7,2], 245, \"Q\"],\
[\"B\",\"B\",[100.6,2], 245, \"Q\"],\
[\"B\",\"B\",[100.5,2], 245, \"Q\"],\
[\"B\",\"B\",[100.4,2], 245, \"Q\"],\
[\"B\",\"B\",[100.3,2], 245, \"Q\"],\
[\"B\",\"B\",[100.2,2], 245, \"Q\"],\
[\"B\",\"B\",[100.1,2], 245, \"Q\"],\
[\"B\",\"B\",[%f,2], 145, \"Q\"],\
[\"S\",\"B\",[%f,2], 445, \"Q\"],\
[\"S\",\"B\",[99.9,2], 45, \"Q\"],\
[\"S\",\"B\",[99.1,2], 345, \"Q\"],\
[\"S\",\"B\",[100.0,2], 445, \"Q\"],\
[\"S\",\"B\",[99.9,2], 45, \"Q\"],\
[\"S\",\"B\",[99.1,2], 345, \"Q\"],\
[\"S\",\"B\",[100.0,2], 445, \"Q\"],\
[\"S\",\"B\",[99.9,2], 45, \"Q\"],\
[\"S\",\"B\",[99.1,2], 345, \"Q\"],\
[\"S\",\"B\",[100.0,2], 445, \"Q\"],\
[\"S\",\"B\",[99.9,2], 45, \"Q\"],\
[\"S\",\"B\",[99.1,2], 345, \"Q\"],\
[\"S\",\"B\",[100.0,2], 445, \"Q\"],\
[\"S\",\"B\",[99.9,2], 45, \"Q\"],\
[\"S\",\"B\",[99.1,2], 345, \"Q\"],\
[\"S\",\"B\",[100.0,2], 445, \"Q\"],\
[\"S\",\"B\",[99.9,2], 45, \"Q\"],\
[\"S\",\"B\",[99.1,2], 345, \"Q\"]\
]\
}\n\
\n\n\0"

#define lasttrades  @"\
MESSAGE\n\
destination: lasttrades\n\
message-id:  123\n\
subscription: 2345\n\
session-id: %@\n\
\n\
{\
\"properties\":{\"type\": \"snapshot\", \"seqnum\": 1},\n\
\"columns\": [\"SECID\",\"PRICE\",\"CHANGE\", \"QUANTITY\", \"Q\"],\
\"data\": [\
[\"MXSE.EQBR.SBER\",[%f,2],[0.2,2], 245, \"Q\"],\
[\"MXSE.EQNE.GAZP\",[100.1],[0.2,2], 245, \"Q\"]\
]\
}\n\
\n\n\0"

- (void) startEmulator: (NSString *)sessionID
{
    static float i = 100.0;
    
    while (1) {
        
        NSString *ob = [NSString stringWithFormat:orderbooks, sessionID,i, i-0.01]; i+=0.01;
        NSString *sb = [NSString stringWithFormat:lasttrades, sessionID, i];
        
        [self onRead: [ob dataUsingEncoding:NSUTF8StringEncoding]];
        [self onRead: [sb dataUsingEncoding:NSUTF8StringEncoding]];
        usleep(500000);
        
    }
}

- (void) onPing:(NSData *)data roundTrip:(NSTimeInterval)roundTrip{
    NSLog(@" -------- PING round trip: %f \n\n", roundTrip);
    static BOOL emuStarted = NO;
    
    for (NSString *chID in self.channels) {
        GICChannel *_channel = self.channels[chID];
        if (!_channel.sessionID) {
            _channel.sessionID = @"123-456-7890";
        }
        if (!emuStarted) {
            emuStarted=YES;
            [self startEmulator: _channel.sessionID];
        }
    }
}

- (void) onConnect:(NSData *)data {
}

- (void) onSend:(NSData *)data{
    
}

- (BOOL) isDataCompleted:(NSData *)chainedData{
    if(!chainedData) return NO;
    if([chainedData length]==0) return YES;
    
    char _the_end_of_stopm;
    
    [chainedData getBytes:&_the_end_of_stopm range:NSMakeRange([chainedData length]-1, 1)];
    return _the_end_of_stopm == '\0';
}


@end
