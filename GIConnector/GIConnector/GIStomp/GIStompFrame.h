//
//  GIStompFrame.h
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIStompBody.h"

//
// Declaration protocol should be used by concrete STOMP class declaration
// such as connect frame, disconnect, messages and etc...
//
@protocol GIStompDeclarationProtocol <NSObject>
//
// Name of destination
//
@optional
+ (NSString*) destination;
@end


//
// COMMON STOMP FRAME
//
@interface GICommonStompFrame : NSObject <NSCopying>

@property(readonly) NSString     *name;
@property(readonly) NSDictionary *headers;

//
// Reference to body data
//
@property GIStompBody  *body;

//
// Init STOMP Frame from received data
//
- (id) initWithData: (NSData*)aData;

//
// Update stomp frame instance from another source
//
- (void) updateFromData: (NSData*)aData;

//
// Get STOMP Frame header, it usually uses when we receive new message
//
- (id)   getHeader:(NSString*)key;
//
// Set STOMP Frame header, it usually uses to send Frame
//
- (void) setHeader:(NSString*)key withValue:(id)value;

//
// Create instance with name of stomp frame (CONNECT, MESSAGE, REPLAY, etc)
//
- (id) initWithName:(NSString *)aName;

//
//
//
- (NSData*) stompFrame;
@end



//
// STOMP receiver protocol
//

@protocol GIStompReceiverProtocol <NSObject>

- (void) messageReceived: (GICommonStompFrame*)frame;

@end

//
// Responses Queue
//
@interface GIStompFrameQueue : NSObject
@property(readonly) NSUInteger size;
- (GICommonStompFrame*) pop;
- (void) push: (GICommonStompFrame*)frame;
- (void) setMaxSize: (NSUInteger) size;
- (NSUInteger) maxSize;
@end

//
// Root of STOMP
//
@interface GIStomp : NSObject <GIStompDeclarationProtocol, GIStompReceiverProtocol>

//
// Frame contains request frame
//
@property GICommonStompFrame    *request;

//
// Last response
//
@property(readonly) GICommonStompFrame    *lastResponse;

//
// We can have many responses for one request
//
@property(readonly) GIStompFrameQueue     *responseQueue;

//
// Set max queue depth if we want to keep last size responses only
//
- (void) setResponseQueueMaxSize: (NSUInteger)size;

//
// Create stomp request and response containers
//
- (id) initWithRequestName: (NSString*)rqname andResponseName:(NSString*)rpname;


@end
