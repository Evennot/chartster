//
//  GIStompFrame.m
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompFrame.h"

//
// COMMON
//
@interface GICommonStompFrame ()
@property NSString *name;
@end

@implementation GICommonStompFrame
{
    NSMutableDictionary  *_headers;
    NSMutableArray       *_header_keys; // to order
    NSMutableData        *_data;    
}

@synthesize name;

- (id) copyWithZone:(NSZone *)zone{
    GICommonStompFrame *_new_obj = [[[self class] allocWithZone:zone] init];
    
    if (_new_obj) {
        _new_obj->name = [self.name copy];
        _new_obj->_headers = [self.headers mutableCopy];
        _new_obj->_data = [self->_data mutableCopy];
        _new_obj->_body = [self->_body copy];
        _new_obj->_header_keys = [self->_header_keys copy];
    }
    
    return _new_obj;
}

- (NSDictionary*) headers{
    return _headers;
}

- (id)   getHeader:(NSString*)key{
    return [_headers objectForKey:key];
}

- (void) setHeader:(NSString *)key withValue:(id)value{
    _data = nil;
    if (![_headers valueForKey:key]){
        [_header_keys addObject:key];
    }
    [_headers setObject:value forKey:key];
}

- (void) updateFromData:(NSData *)aData{
    
    _headers     = [[NSMutableDictionary alloc] init];
    _header_keys = [[NSMutableArray alloc] init];
    _data = nil;
    
    if (aData) {
        
        self.name = nil;
        
        int  _header_delimeter_index    = 0;        
        BOOL _header_delimeter_is_found = NO;
        
        NSMutableString *_line = [NSMutableString stringWithCapacity:[aData length]]  ;
        
        for (int i=0; i<[aData length]; i++) {
            
            //
            // find STOMP marker
            //
            char _stomp_marker = ((char*)[aData bytes])[i];
            
            //
            // keep : for header
            //
            if(!_header_delimeter_is_found) _header_delimeter_index++;
            if(_stomp_marker == ':')        _header_delimeter_is_found = YES;

            if (_stomp_marker == '\n'){
                
                //
                // name
                //
                if (!self.name) {
                    self.name = [[NSString stringWithString: _line] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    [_line setString:@""];
                    _header_delimeter_index = 0;
                }
                else{
                    if (_header_delimeter_index>0 && _header_delimeter_is_found) {
                        
                        //
                        // headers
                        //
                        NSString *_h = [[_line substringToIndex:_header_delimeter_index-1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];                        
                        NSString *_v = [[_line substringFromIndex:_header_delimeter_index] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        [_headers setValue:_v forKey:_h];
                        
                        [_line setString:@""];
                        _header_delimeter_index = 0;
                        _header_delimeter_is_found = NO;
                    }
                }
            }
            else if(_stomp_marker == '{'){
                //
                // Breake to parse data block "on demand" if we'll need
                //
                
                int _last_index = [aData length]-1;
                
                // trim the end of data
                for(int l = _last_index; l>i; l--){
                    char _stomp_marker = ((char*)[aData bytes])[l];
                    if(_stomp_marker=='}'){
                        _last_index = l-i+1;
                        break;
                    }
                }
                
                if (i<_last_index) {
                    //
                    // create new body
                    //
                    self.body = [[GIStompBody alloc] initFromData: [aData subdataWithRange:NSMakeRange(i, _last_index)] ];
                }
                else
                    self.body = nil;
                break;
            }
            else if (_stomp_marker=='\0'){
                break;
            }
            else
                //
                // keep line
                //
                [_line appendFormat:@"%c" ,_stomp_marker];
            }
    }
}

- (id) initWithName:(NSString *)aName{
    
    if (!(self=[super init]))
        return nil;
    
    self.name = [aName uppercaseString];
    
    _headers     = [[NSMutableDictionary alloc] init];
    _header_keys = [[NSMutableArray alloc] init];
    _data = nil;
    
    return self;
}

- (id) initWithData:(NSData *)aData{
    
    if (!(self=[super init]))
        return nil;
    
    [self updateFromData:aData];
    
    return self;
}

- (NSData*) stompFrame{
    
    if (!_data){
        _data = [[NSMutableData alloc] init];
        
        [_data appendBytes:[self.name cStringUsingEncoding:NSUTF8StringEncoding] length:[self.name length]];
        [_data appendBytes:"\n" length:1];
        
        for (id key in [_header_keys objectEnumerator]) {
            NSString *_header_line = [NSString stringWithFormat:@"%@: %@\n", key, _headers[key]];
            [_data appendBytes:[_header_line cStringUsingEncoding:NSUTF8StringEncoding] length:[_header_line length]];
        }
        
        [_data appendData: [self.body dataWithJSONObject]];
        
        [_data appendBytes:"\n\n\0" length:3];
    }
    return _data;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@\n%@\n%@", self.name, self.headers, self.body];
}

@end



//
// Responses Queue
//
@protocol GIStompQueueDelegat <NSObject>

- (void) resetLastResponse: (GICommonStompFrame*)frame;

@end

@interface GIStompFrameQueue()
@property id <GIStompQueueDelegat> delegat;
@end

@implementation GIStompFrameQueue
{
    NSMutableArray *queue;
    NSUInteger      depth;
    NSLock         *lock;
}

- (id) init{
    self = [super init];
    if(self){
        queue = [NSMutableArray arrayWithCapacity:0];
        depth = 0;
        lock  = [[NSLock alloc]init];
    }
    return self;
}

- (NSUInteger) maxSize{
    return depth;
}

- (void) setMaxSize:(NSUInteger)size{
    [lock lock];
    depth=size;
    if ([queue count]>depth) {
        [queue removeObjectsInRange:NSMakeRange(depth, [queue count]-depth)];
    }
    [lock unlock];
}

- (NSUInteger) size{
    return [queue count];
}

- (GICommonStompFrame*) pop{
    [lock lock];
    if ([queue count]==0){
        [lock unlock];
        return nil;
    }
    @try {
        id r_ = [queue objectAtIndex:0];

        if (self.delegat)
            [self.delegat resetLastResponse:r_];
        
        [queue removeObjectAtIndex:0];
        [lock unlock];
        return r_;
    }
    @catch (NSException *exception) {
        return nil;
    }
    return nil;
}

- (void) push:(GICommonStompFrame *)frame{
    if (!frame)
        return;
   
    [lock lock];
    if (depth>0 && [queue count]>=depth) {
        [queue removeObjectAtIndex:(depth-1)];
    }
    
    [self.delegat resetLastResponse:frame];
    [queue addObject:frame];
    
    [lock unlock];
}

@end


//
// ROOT OF STOMP
//

@interface GIStomp() <GIStompQueueDelegat>
@property GICommonStompFrame *lastResponse;
@property GIStompFrameQueue  *responseQueue;
@end

@implementation GIStomp

@synthesize  responseQueue=_responseQueue, lastResponse=_lastResponse;


- (id) initWithRequestName:(NSString *)rqname andResponseName:(NSString *)rpname{
    if (!(self=[super init])) {
        return nil;
    }
    
    self.request  = [[GICommonStompFrame alloc] initWithName:rqname];
    _lastResponse = nil;
    
    _responseQueue = [[GIStompFrameQueue alloc] init];
    _responseQueue.delegat = self;
    
    return self;
}

- (void) messageReceived:(GICommonStompFrame *)frame{
    [_responseQueue push:frame];
}

- (void) resetLastResponse:(GICommonStompFrame *)lastResponse{
    _lastResponse = lastResponse;
}


- (GICommonStompFrame*) lastResponse{
    return _lastResponse;
}

- (void) setLastResponse:(GICommonStompFrame *)lastResponse{
    [_responseQueue push:lastResponse];
}

- (void) setResponseQueueMaxSize:(NSUInteger)size{
    [_responseQueue setMaxSize:size];
}

- (NSString*) description{
    return [NSString stringWithFormat:@"Request: %@\nResponse: %@\n", self.request,self.lastResponse];
}

@end
