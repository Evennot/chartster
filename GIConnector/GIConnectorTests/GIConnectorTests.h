//
//  GIConnectorTests.h
//  GIConnectorTests
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "GICSPTunnel.h"

@interface GIConnectorTests : SenTestCase <GICSPTunnelHandler>

@end
