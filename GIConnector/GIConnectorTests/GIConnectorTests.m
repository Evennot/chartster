//
//  GIConnectorTests.m
//  GIConnectorTests
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GIAuth.h"
#import "GIConnectorTests.h"
#import "GIAbstractConnector.h"


@interface GITestConnectorHandler : NSObject <GIDataHandler>
@end


@implementation GITestConnectorHandler
- (id) process:(GIExchangeContainer*)exchange{
    
    NSLog(@"Start process again: %@: %@", exchange.gic, exchange.userData);
    return nil;
};

- (void) error:(GIExchangeContainer*)exchange{
    
    NSLog(@"Error has ben occured: %@: %@", exchange.gic, exchange.error);
    
};
@end


@implementation GIConnectorTests

static GIConfig *config;

- (void) test0Auth
{
    config = [[GIConfig alloc] initWithUrl:@"http://localhost:1080"];                
    GIAuth *login = config.login;
    
    [login restoreFromKeyChain: [NSString stringWithFormat:@"%@", config.serverUrl]];

    NSLog(@"1. login %@:%@", config.login.user, config.login.password);
    
    login.password = @"12345";
    login.user     = @"demo";
    
    NSLog(@"2. login %@:%@", config.login.user, config.login.password);
    
    [login saveInKeyChain: [NSString stringWithFormat:@"%@", config.serverUrl]];

    [login restoreFromKeyChain: [NSString stringWithFormat:@"%@", config.serverUrl]];

    NSLog(@"3. login %@:%@", config.login.user, config.login.password);

}


- (void)test1GIConfig
{
    config = [[GIConfig alloc] initWithUrl:@"http://user:password@localhost:1080"];            
    NSLog(@"\n----------\n");
    NSLog(@"%@", config);
    NSLog(@"\n----------\n");
    NSLog(@"GIConnector test pass 2\n");
    
}

- (GICSPTunnel*) tunnelRun: (NSString*)name sendCount: (int) sends checkState: (BOOL) check
{
    NSLog(
          NSLocalizedStringFromTable(@"\n\nSTART ::: ****************************** \n%@ %@\n\n", @"GIConnectorLocalizable", @" Start logging ") , 
          name, config.serverUrl);
    
    GICSPTunnel *csp = [[GICSPTunnel alloc] initWithGIConfig:config];
    
    csp.connectionHandler=self;
    
    [csp startAsync];   
        
    for (int i =0; ![csp isOpened] && check && i<10; i++) {
        NSLog(@"Session is not opened yet");
        sleep(1);
    }
    
    for (int i=0; i<sends; i++) {
        
        if ([csp isCancelled]) {
            NSLog(@"CSP cancelled");
            break;
        }

        if ([csp isOpened] || !check) {
            
            NSString *send_str = [NSString stringWithFormat: @"# 500 10 \n CONNECT %i", i];
            
            [csp sendString: send_str];
            
            if (i==sends-1 && i>0) {
                NSLog(@"CANCEL OPERATION .... ");
                [csp cancel];
            }            
        }
        else
            NSLog(@"Session is not opened");
        
        //usleep(50000);
        sleep(1);
    }
        
    
    NSLog(@"\n\n%@\nPASSED :::: ***************************** \n\n\n", name);
    
    return csp;
}

#define _RUN_ALL_TESTS_1

#ifdef _RUN_ALL_TESTS  
- (void) test2GICSPTunnel
{
    config.serverUrl = @"http://user:password@wrongserver";
    [self tunnelRun: @"GICSPtunnel test connection to WRING (not exists) host" sendCount: 1 checkState:NO];
    sleep(1);
}

- (void) test3GICSPTunnel
{
    config.serverUrl = @"https://user:password@goinvest.micex.ru";
    config.connectionTimeout = 1;
    [self tunnelRun: @"GICSPtunnel test connection to FIREWALLED (timeouted) host" sendCount: 5 checkState:YES];
    sleep(1);
}

- (void) test4GICSPTunnel
{
    config.serverUrl = @"https://user:password@localhost:1080";
    config.validatesSecureCertificate = YES;
    [self tunnelRun: @"GICSPtunnel check SSL WRONG CERT" sendCount: 1 checkState:YES];
    sleep(1);
}

- (void) test5GICSPTunnel
{
    config.serverUrl = @"https://user:password@localhost:1080";
    config.validatesSecureCertificate = NO;
    config.connectionTimeout = 10;
    GICSPTunnel *csp=[self tunnelRun: @"GICSPtunnel check RECEIVER" sendCount: 5 checkState:YES];
    sleep(10);
    [csp cancel];
}

- (void) test6GICSPTunnel
{
    config.serverUrl = @"https://user:password@localhost:1080";
    config.validatesSecureCertificate = NO;
    config.connectionTimeout = 10;
    config.reconnectCount    = 100;
    config.reconnectTimeout  = 1;
    config.doPing = YES;
    
    GICSPTunnel *csp=[self tunnelRun: @"GICSPtunnel check RECEIVER" sendCount: 1 checkState:YES];
    
    for (int i=0; i<20; i++) {
        if (i==10){
            
            NSLog(@"CLOSE RECEIVER");
            [csp closeReceiver];
        }
        if (i==11){
            NSLog(@"RESTORE RECEIVER");
            [csp restoreReceiver];
        }
        
        if (i>=12){
            [csp sendString: [NSString stringWithFormat:@"# 10 100 RESTORE RECEIVER %i", i]];
        }
        
        sleep(1);
    }    
    
    [csp cancel];
    
    sleep(1);
}

#endif


/*

- (void)test4GICallbacks
{
    NSLog(@"GIConnector test start 4\n %@", config);
        
    GITestConnectorHandler  *handler= [[GITestConnectorHandler alloc] init];
    GIAbstractConnector     *gic    = [[GIAbstractConnector alloc] initWithConfig: config andDataHandler: handler];
    
    
    [gic startAsync];
    
    BOOL   inprogress = YES;
    while (inprogress) {
        break;
        sleep(1);
        inprogress = NO;
    }
    
    NSLog(@"GIConnector test pass 4\n");
}
*/


- (void) onError:(GIError *)error in:(GICSPCommand)command{
    NSLog(@"  \n-------- Error: in %i ---------- \n\n %@ ", command, error);
}

- (void) onClose:(NSData *)data{
    
    NSLog(@"  \n\n-------- Close: ---------- \n\n");
    
    NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
    NSLog(@"Close data: %@", _buffer);
    
}

- (void) onOpen:(NSData *)data{
    
    NSLog(@"  \n\n-------- Open : ---------- \n\n");
    
    NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
    NSLog(@"Open data: %@", _buffer);
}

- (void) onPing:(NSData *)data roundTrip:(NSTimeInterval)roundTrip{
    
    NSLog(@"  \n\n-------- PING round trip %f: ---------- \n\n", roundTrip);
    
    NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
    NSLog(@"PING data: %@", _buffer);
}

- (void) onRead:(NSData *)data{
    
    NSLog(@"  \n\n-------- READ  ---------- \n\n");
    NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
    
    NSLog(@"%@",_buffer);
    
    NSLog(@" \n\n-------- /READ  ---------- \n\n");
}

- (void) onSend:(NSData *)data{
    
    NSLog(@"  \n\n-------- SEND  ---------- \n\n");
    if (data) {
        //
        NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
        NSLog(@"%@",_buffer);
    }
    
    NSLog(@"  \n\n-------- /SEND  ---------- \n\n");
}


/*
 *  Common untiests init...
 */

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

@end
