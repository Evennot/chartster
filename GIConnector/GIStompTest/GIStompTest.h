//
//  GIStompTest.h
//  GIStompTest
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "GIStompChannelDispatcher.h"


@interface GIStompTest : SenTestCase <GIStompDispatcherProtocol, GIStompErrorDispatchHandler, GIStompEventDispatchHandler, GIStompLoginDispatchHandler>

@end
