//
//  GIStompTest.m
//  GIStompTest
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompTest.h"
#import "GIStomp.h"
#import "GICSPTunnel.h"
#import "GIAuth.h"
#import "GIStompChannelDispatcher.h"


@implementation GIStompTest
{
    GIConfig    *config;
    GICSPTunnel *csp;
    NSString    *stompSession;
    NSInteger   channel;
}


#define __CONNECTION_STOMP__1
#ifdef __CONNECTION_STOMP__

NSString *connected = @"\
CONNECTED\n\
content-length:14\n\
session:8db987ec-8fad-11e2-8247-3cd92bf560d8\n\
channel:1\n\n\
\n\
{\"account\":[]}\n\
\n\n\0";


- (void)test01Stomp_Init{
    GIStomp *sf = [[GIStomp alloc] initWithRequestName:@"CONNECT" andResponseName:@"CONNECTED"];
    [sf.response updateFromData:[connected dataUsingEncoding:NSUTF8StringEncoding]];
    STAssertFalse(!sf,  @"STOMP Frame couldnot be create");
}

- (void)test02Stomp_Connect{
    GIStompConnect *connect = [GIStompConnect toChannel:@"1" withLogin:@"nobody" andPasscode:@"somepass"];
    STAssertFalse(!connect,  @"STOMP Frame CONNECT error");
}

- (void)test03Stomp_DisConnect{
    GIStompDisconnect *dconnect = [GIStompDisconnect make];
    STAssertFalse(!dconnect,  @"STOMP Frame DISCONNECT error");
}

- (void)test04Stomp_Tickers{
    GIStompTickers *st =[GIStompTickers select:@"sber" onMarket:@"MXSE"];
    NSLog(@"\n\n-----\n\n%@ \n\n",  st);
    STAssertFalse(!st,  @"STOMP Frame tikers error");
}

- (void)test05Stomp_TickersUTF{
    GIStompTickers *st =[GIStompTickers select:@"сбер" onMarket:@"MXSE"];
    NSLog(@"\n\n-----\n\n%@ \n\n",  st);
    STAssertFalse(!st,  @"STOMP Frame utf8 tikers error");
}



- (void)test06Stomp_GICLogins{

    NSDictionary *dlogins  = @{ 
                               @"1": @[@"user1", @"passwd1"],
                               @"2": @[@"user2", @"passwd2"],
                               @"3": @[@"user3", @"passwd3"]
                               };
    
    GILogins *logins = [[GILogins alloc] initWithDictionary:dlogins];
    NSLog(@"Logins: %@", logins);
    STAssertFalse(!logins, @"Logins has error");
    
}

- (void)test07Stomp_ParseData
{
    NSString *somedata = @"\
    MESSAGE\n\
    destination: orders\n\
    message-id:  123\n\
    subscription: 2345\n\
    session-id: 8db987ec-8fad-11e2-8247-3cd92bf560d8\n\
    \n\
    {\
    \"properties\":{\"type\": \"snapshot\", \"seqnum\": 1},\n\
    \"columns\": [\"ORDERNO\",\"TICKER\",\"BUY\", \"SELL\", \"QTY\"],\
    \"data\": [[\"ORDERNO\",\"TICKER\",\"BUY\", \"SELL\", \"QTY\"],\
    [1,\"MXSE.EQBR.GAZP\",3,4,5],[10,\"MXSE.EQBR.SBER\",30,40,50],[1,\"MXSE.EQBR.LKOH\",3,4,5]]\
    }\n\
    \n\n\0";
    
    GIStompOrderbook    *rp = [GIStompOrderbook select:@""];
    [rp.response updateFromData:[somedata dataUsingEncoding:NSUTF8StringEncoding]];

    NSLog(@"Replay stomp: %@\n", [rp.response stompFrame] );
    STAssertFalse(!rp,  @"STOMP Frame name is not parsed");
}


NSString *somedata = @"\
MESSAGE\n\
destination: orderbooks\n\
message-id:  123\n\
subscription: 2345\n\
session-id: 8db987ec-8fad-11e2-8247-3cd92bf560d8\n\
\n\
{\
\"properties\":{\"type\": \"snapshot\", \"seqnum\": 1},\n\
\"columns\": [\"BYSELL\",\"BYSELL_TEXT\",\"PRICE\", \"QUANTITY\", \"Q\"],\
\"data\": [\
[\"B\",\"B\",[100.1,2], 245, \"Q\"],\
[\"B\",\"B\",[100.01,2], 145, \"Q\"],\
[\"S\",\"B\",[100.0,2], 445, \"Q\"],\
[\"S\",\"B\",[99.9,2], 45, \"Q\"],\
[\"S\",\"B\",[99.1,2], 345, \"Q\"]\
]\
}\n\
\n\n\0";

- (void)test08Stomp_ParseName{
    GIStompOrderbook    *rp = [GIStompOrderbook select:@""];
    [rp.response updateFromData:[somedata dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"Replay stomp: name=>%@<\n", rp.response.name);
    NSLog(@"Replay stomp: headers=>%@<\n", rp.response.headers);
    STAssertFalse(![rp.response.name isEqualToString:@"MESSAGE"],  @"STOMP Frame name is not parsed");
}

- (void)test09Stomp_ParseDestination{
    GIStompOrderbook *rp =[GIStompOrderbook select:@""];
    [rp.response updateFromData:[somedata dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSLog(@"test 09 :\n%@\n", rp);
    
    NSLog(@"Replay stomp: request destination=>%@<\n", rp.request.headers[@"destination"]);
    NSLog(@"Replay stomp: response destination=>%@< should be %@\n", rp.response.headers[@"destination"], [[GIStompOrderbook class] destination]);
    STAssertFalse(![rp.response.headers[@"destination"] isEqualToString:[GIStompOrderbook destination]],   @"STOMP frame destination is not parsed");
}
#endif


- (void)test09Stom_Dispatcher{
    
    
    config = [[GIConfig alloc] initWithUrl:@"https://goinvest2.beta.micex.ru"];
    config.login = [[GIAuth alloc] initWithUser:@"vasya@superbroker" andPassword:@"12345"];

//    config = [[GIConfig alloc] initWithUrl:@"https://goinvest.micex.ru/"];
//    //config = [[GIConfig alloc] initWithUrl:@"https://localhost:8080"];
//    config.login = [[GIAuth alloc] initWithUser:@"MXSE.MU0000600010" andPassword:@""];

    config.validatesSecureCertificate = NO;

    GIStompChannelDispatcher *_dispatcher = [[GIStompChannelDispatcher alloc] initWithConfig:config];
    
    _dispatcher.errorHandler = self;
    _dispatcher.eventHandler = self;
    
    
    _dispatcher.loginHandler = self;
    
    [_dispatcher connectOnMarket:@"MXSE"];
    [_dispatcher connectOnMarket:@"MXCX"];
    [_dispatcher connectOnMarket:@"INDICES"];
    [_dispatcher connectOnMarket:@"RATES"];

    
    GIStompCandles *candles = [GIStompCandles select:@"MXSE.EQBR.SBER" andInterval:@"M10"];
    
    [_dispatcher registerMessage:candles];
    candles.delegate = self;

    
    sleep(10);
    
#if __GI_TEST_RECONNECTION
    
    NSLog(@">>>>>>>>>>\n");
    config.login = [[GIAuth alloc] initWithUser:@"vasya1@superbroker" andPassword:@"12345"];

    [_dispatcher connectOnMarket:@"MXCX"];

    
    sleep(10);

    NSLog(@">>>>>>>>>>\n");
    config.login = [[GIAuth alloc] initWithUser:@"vasya@superbroker" andPassword:@"12345"];

    [_dispatcher connectOnMarket:@"MXCX"];
    [_dispatcher connectOnMarket:@"MXSE"];
    [_dispatcher connectOnMarket:@"INDICES"];

    config.login = [[GIAuth alloc] initWithUser:@"vasya1@superbroker" andPassword:@"12345"];
    
    [_dispatcher connectOnMarket:@"MXSE"];

    config.login = [[GIAuth alloc] initWithUser:@"vasya@superbroker" andPassword:@"12345"];
    [_dispatcher connectOnMarket:@"MXSE"];

    //GIStompTickers *tickers = [_dispatcher registerTickersOnMarket:@"MXSE"];

    //tickers.delegate = self;
    
#endif
    
    
    [GIStompOrderbook setDepth:5];
    
    GIStompOrderbook  *ob = [GIStompOrderbook select:@"MXSE.EQNE.GAZP"];
    GIStompSecurity *ot = [GIStompSecurity select:@"MXSE.EQNE.GAZP"];


    [_dispatcher registerMessage:ob];
//    [_dispatcher registerMessage:ot];

    ob.delegate = self;
    ot.delegate = self;

    
    sleep(5);
    
    NSLog(@"----- UNSUBSCRIBE ------");
    
    [_dispatcher unregisterMessage:ob];
    //[_dispatcher unregisterMessage:ot];
    [_dispatcher unregisterMessage:candles];
    
    sleep(10);

    NSLog(@"----- SUBSCRIBE AGAIN ------");

    [_dispatcher registerMessage:ob];
    [_dispatcher registerMessage:candles];
    
    
    sleep(10);
    
#if 0

    GIStompOrderbook  *obs = [GIStompOrderbook select:@"MXSE.EQBR.SBER"];
    GIStompSecurity *ots = [GIStompSecurity select:@"MXSE.EQBR.SBER"];
//    GIStompLastTrades *otst= [GIStompLastTrades select:@"MXSE.EQBR.SBER"];

    [_dispatcher registerMessage:obs];
    [_dispatcher registerMessage:ots];
//    [_dispatcher registerMessage:otst];

    obs.delegate = self;
    ots.delegate = self;
//    otst.delegate = self;

    GIStompOrderbook  *obl = [GIStompOrderbook select:@"MXSE.EQBR.LKOH"];
    GIStompSecurity *otl = [GIStompSecurity select:@"MXSE.EQBR.LKOH"];
//    GIStompLastTrades *otlt= [GIStompLastTrades select:@"MXSE.EQBR.LKOH"];

    [_dispatcher registerMessage:obl];
    [_dispatcher registerMessage:otl];
//    [_dispatcher registerMessage:otlt];
    
    obl.delegate = self;
    otl.delegate = self;
//    otlt.delegate = self;

    
    NSArray *a = @[@"MXSE.EQBR.VTBR",
                   @"MXSE.EQBR.IRAO",
                   @"MXSE.EQNL.TGKI",
                   @"MXSE.EQNL.FEES",
                   @"MXSE.EQBR.MSNG",
                   @"MXSE.EQNL.ROSN"];
    
    
    for (NSString *t in a) {
        GIStompOrderbook  *obu = [GIStompOrderbook select:t];
        [_dispatcher registerMessage:obu];
        obu.delegate = self;
    }
    
#endif
    
    sleep(20);
    
    NSLog(@"Stop!");
    [_dispatcher stop];

    NSLog(@"Stoped!");
    sleep(5);
    
    [_dispatcher start];
    NSLog(@"Start new!");
    
    sleep(100);
    
}

- (void) didStompLoginError:(GIError *)error withConnectionResponse:(GICommonStompFrame *)response{
    NSLog(@"Login error: %@: %@", error, response);
}

- (void) didStompLogin:(NSDictionary*)connection {
    NSLog(@"Login connection: %@", connection);
}

- (void) didFrameReceive:(id)frame{
    NSLog(@"\nYAHOOOO!\n%@\n", [frame class]);
    
    
    if ([frame isKindOfClass:[GIStompTickers class]]){
        NSLog(@" TICKERS: %@", frame);
        return;
    }
    
    
    GIStompMessage *m = frame;
    
    for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c=[m.responseQueue pop]) {
        //NSString *s = [NSString stringWithFormat:@"%@[%i]: %@ proerties=%@", m.request.headers[@"destination"], [c.body countOfRows], c, c.body.properties];
        //NSLog(@"%@", s);
        NSLog(@" Queue length = %i %@ %@ : %@", [m.responseQueue size], [m.lastResponse.body stringForIndex:0 andKey:@"TICKER"], m.lastResponse.body.properties, [c description]);
    }
    
}

- (void) channelConnectionRestored{
    NSLog(@"Channel restored");
}

- (void) channelReconnecting{
    NSLog(@"Channel reconnection");
}


- (void) didSystemError:(GIError *)error{
    NSLog(@"%@", error);
}

- (void) didStompError:(GIError *)error{
    NSLog(@"%@", error);
}

- (void) channelWillActive{
    NSLog(@"Start connecting...");
}

- (void) channelOpened{
    NSLog(@"Opened");
}

- (void) channelClosed{
    NSLog(@"Closed");
}

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

@end
