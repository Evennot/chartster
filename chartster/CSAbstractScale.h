//
//  CSAbstractScale.h
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSEssentials.h"
#import "CSAbstractScaleRepresentation.h"

@class CSLabelComposition;

typedef enum MarkingModeTag {
    mmNone = 0,
    mmMarkZone = 1,
    mmHint = 2
} MarkingMode;

@interface CSAbstractScale : NSObject <SmoothScalerProtocol> {
    CFTimeZoneRef defZone;
    BOOL hasScaleTension;
    CFTimeInterval compensationTime;

    double startIndex;
    
    // cached recalc values
    CGRect rFrame;
    CGRect vFrame;
    
    CGFloat itemSize;
    CGFloat lastIntFrameWidth;
    int lastMaxVisibleItemCount;
    
    // time period stripes
    CGRect vRects[100];
    NSUInteger vRectCount;
}

@property (nonatomic) BOOL badScale;

@property (nonatomic, setter = setType:) ScaleRepresentation type;
@property (nonatomic, readonly) CandleInterval candleItemInterval;
@property (nonatomic, readonly, strong) CSAbstractScaleRepresentation *representation;
@property (nonatomic) CGFloat verticalGridWidth;
@property (nonatomic, strong) UIColor *stripeColor;
@property (nonatomic, strong) UIColor *gridColor;
@property (nonatomic, readonly, getter = getBigPeriodMask) int bigPeriodMask;
@property (nonatomic, readonly, strong) NSString *decimalledFormat; // CSView has this format string too, but here it takes into account only price decimals and CSView format string is in percents or in currency depending on mode

@property (nonatomic, setter = setDesiredItemSize:, getter = getDesiredItemSize) CGFloat desiredItemSize; // desired item size for the next realign routine.

@property (nonatomic) MarkingMode mark;
@property (nonatomic) CGPoint markZoneBalloonPointL;
@property (nonatomic) CGPoint markZoneBalloonPointR;

@property (nonatomic, strong) NSDateFormatter *axisFormatterTime; // hh:mm
@property (nonatomic, strong) NSDateFormatter *axisFormatterDay;  // dd
@property (nonatomic, strong) NSDateFormatter *axisFormatterDateBig; // yyyy mm dd
@property (nonatomic, strong) NSDateFormatter *axisFormatterDate; // mm dd, month dd, month,
@property (nonatomic, strong) NSDateFormatter *axisFormatterYear; // yyyy

- (id) initFor:(CandleInterval)theCandleItemInterval;

- (void) recalcViewInFrame:(CGRect) intFrame ClippingInset:(CGFloat) cInset;
- (void) rescale:(float) scale Around:(CGPoint) p;
- (void)pan:(float) x;
- (float)tryPan:(float) x; // returns panning difficulty: ~1: OK, ~0: difficult, < 0 impossible
- (BOOL) checkNeededCompensation:(BOOL) panOnly ForMasterView:(id <CSChartLayoutManager>)masterView;
- (BOOL) hasVerticalScaleTension;
- (void)realign:(CGRect) intFrame ClippingInset:(CGFloat) cInset;
- (double) priceAtY:(float) y;
- (float) yForPrice:(double) p;
- (BOOL) hasData;
- (BOOL) drawPresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen;
- (BOOL) drawParameter:(NSString *) param PresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen;
- (void) drawInContext:(CGContextRef) context NeedsOverlayDrawing:(BOOL *)needsOverlayDrawing;
- (void) drawOverlaysInContext:(CGContextRef) context;
- (void) drawStripesInContext:(CGContextRef)context;
- (double) markZoneBetween:(CGFloat) x0 And:(CGFloat) x1 HLOC0:(out HLOCV *) hloc0 HLOC1:(out HLOCV *) hloc1 Dates:(out NSArray **) dates; // returns delta
- (void) hintOn:(CGFloat) x HLOC:(out NSArray **) data Date:(out NSString **) date;
- (FeedingResult) feedingDone;
- (void) enumerateVerticalGridUsingBlock:(GridEnum) block ForMinimalWidth:(float) minW IntFrame:(CGRect) intFrame;
- (CSLabelComposition *) arrangePriceLabelsForMinimalWidth:(float) minW IntFrame:(CGRect) intFrame Previous:(CSLabelComposition *)previousComposition;
#ifdef mywarns
#warning ^^ not used yet
#endif
- (NSUInteger) mainCount; // main chart candle count

- (void) dropData;
//- (double) startIndexMod:(int) modFactor;
- (double) lastClose;

//- (void) setType:(ScaleRepresentation) v;
- (void) setDecimals:(NSNumber *)decimals;

@end


@interface CSLabelComposition : NSObject {
    CandleInterval candleItemInterval;
    float minW;
#ifdef mywarns
#warning ^^ not used yet
#endif


    int mask1; // default tag mask
#ifdef mywarns
#warning ^^ not used yet
#endif

}

- (id) initFor:(CandleInterval)ci MinLabelWidth:(float) minWidth;
#ifdef mywarns
#warning ^^ not used yet
#endif

//- (void)updateWith:(NSUInteger)lastUsedItemNo CountLimit:(NSUInteger) countLimit;
//(GridEnum) block ForMinimalWidth:(float) minW IntFrame:(CGRect) intFrame;

@end
