//
//  CSAbstractScale.m
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSAbstractScale.h"
#import "CSCandleRepresentationCandles.h"
#import "CSScaleRepresentationBars.h"
#import "CSScaleRepresentationLine.h"

@interface CSAbstractScale () {
    // vertical scale tension vars
    BOOL hadPreviousScale;
    CFAbsoluteTime scaleTensionStart;
    float previousScaleH, previousScaleL;
    float desiredScaleH, desiredScaleL;
}

@property (nonatomic, strong) CSAbstractScaleRepresentation *representation;
//@property (nonatomic, strong) NSString *decimalledFormat;

@end

@implementation CSAbstractScale

#define DEFCOMPENSATIONTIME 0.4

double intervalLengths[10] =
{60, 10*60, 30*60,
    60*60, 4*60*60, 24*60*60, 7*24*60*60, 27*24*60*60, 360*24*60*60, 10*365*24*60*60};

@synthesize candleItemInterval;
#ifdef mywarns
#warning ^^ not used
#endif



- (id) initFor:(CandleInterval)theCandleItemInterval {
    self = [super init];
    if (self) {
        compensationTime = DEFCOMPENSATIONTIME;
        defZone = CFTimeZoneCopyDefault();
        candleItemInterval = theCandleItemInterval;
        self.axisFormatterTime = [[NSDateFormatter alloc] init];
        [self.axisFormatterTime setDateFormat:@"HH:mm"];
        self.axisFormatterDay = [[NSDateFormatter alloc] init];
        [self.axisFormatterDay setDateFormat:@"dd"];
        self.axisFormatterDateBig = [[NSDateFormatter alloc] init];
        [self.axisFormatterDateBig setDateFormat:@"yyyy-MM-dd"];
        self.axisFormatterDate = [[NSDateFormatter alloc] init];
        [self.axisFormatterDate setDateFormat:@"MM-dd"];
        self.axisFormatterYear = [[NSDateFormatter alloc] init];
        [self.axisFormatterYear setDateFormat:@"yyyy"];
    }
    return self;
}

- (int) getBigPeriodMask {
    switch (candleItemInterval) {
        case ciMonth:
            return hYearChange;
        case ciHour:
            return hDayChange;
        case ci10Minutes:
            if (itemSize > 30)
                return hHOURChange;
            return h4HourChange;
        case ciMinute:
            if (itemSize > 30)
                return h30MinChange;
            return h4HourChange;
        default:
            return hMonthChange;
    }
}

- (void) setDecimals:(NSNumber *)decimals {
    _decimalledFormat = [NSString stringWithFormat:@"%%.%df", [decimals integerValue]];
}

- (void) recalcViewInFrame:(CGRect) intFrame ClippingInset:(CGFloat) cInset {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"recalcViewInFrame" userInfo:nil];
}

- (void)rescale:(float) scale Around:(CGPoint) p {
    double centralItemIndex = (p.x / itemSize) + startIndex;
    itemSize *= scale;
    if (itemSize < 1.5) itemSize = 1.5;
    startIndex = centralItemIndex - (p.x / itemSize);
}

- (void)pan:(float) x {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"pan" userInfo:nil];
}

- (float)tryPan:(float) x {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"tryPan" userInfo:nil];
}

- (NSUInteger) mainCount { // main chart candle count
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"mainCount" userInfo:nil];
}

- (BOOL) hasHorizontalScaleTensionTo:(CGFloat *)targetScale {
    NSUInteger count = [self mainCount];
    
    CGFloat minItemSize = 5;
    if (count > 0) {
        minItemSize = MAX(minItemSize, lastIntFrameWidth / count);
    }
    if (itemSize > 40) {
        *targetScale = 40/itemSize;
        // frictionScale*t*t/2 + tensionVelocity*t - targetScale = 0
        // frictionScale*te = -tensionVelocity
        // te = -tensionVelocity/frictionScale
        //*tensionVelocity = -sqrt(2**targetScale*frictionScale);
        return YES;
    }
    if (itemSize < minItemSize) {
        *targetScale = minItemSize/itemSize;
        //*tensionVelocity = sqrt(2**targetScale*frictionScale);
        return (ABS(*targetScale - 1.0) > 0.001);
    }
    return NO;
}

// checking if auto scale/movement compensation is needed
- (BOOL) checkNeededCompensation:(BOOL) panOnly ForMasterView:(id <CSChartLayoutManager>)masterView {
    CGFloat v = 0;
    CGFloat p = 0;
    if (!panOnly) {
        if ([self hasHorizontalScaleTensionTo:&p]) {
            [masterView needsScaleTensionCompensationTo:p];
            return YES;
        }
    }
    v = [self positionTensionAround:&p];
    if (ABS(v) > 0.5) {
        [masterView needsPanTensionCompensationToPosition:p];
        return YES;
    }
    return [self hasVerticalScaleTension];
}

- (BOOL) hasVerticalScaleTension {
    return hasScaleTension;
}

- (CGFloat) positionTensionAround:(CGFloat *)pos {
    CGFloat tensionVelocity = 0;
    if (startIndex < 0) {
        // startIndex*is = tensionVelocity*t - frictionX*t*t/2
        // frictionX*te = tensionVelocity
        // te = tensionVelocity/frictionX
        tensionVelocity = -2*sqrt(-1*startIndex*itemSize*frictionX);
        *pos = -startIndex*itemSize;
        return tensionVelocity;
    }
    NSUInteger count = [self mainCount];
    if (startIndex + lastMaxVisibleItemCount + 1 > count) {
        tensionVelocity = 2*sqrt((startIndex - count + lastMaxVisibleItemCount)*itemSize*frictionX);
        *pos = -(startIndex - count + lastMaxVisibleItemCount)*itemSize;
        return tensionVelocity;
    }
    return 0;
}

- (float) yForPrice:(double) p {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"yForPrice" userInfo:nil];
}

- (double) priceAtY:(float) y {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"priceAtY" userInfo:nil];
}

- (double) lastClose {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"lastClose" userInfo:nil];
}

- (void)realign:(CGRect) intFrame ClippingInset:(CGFloat) cInset {
    NSUInteger count = [self mainCount];
    if (count) {
//        if (itemSize < 5)
//            itemSize = 5;
//        else {
//            if (itemSize > 40) {
//                CGFloat def = intFrame.size.width / count;
//                if (def < itemSize)
//                    itemSize = def;
//                else
//                    itemSize = 40;
//            }
//        }
#ifdef mywarns
#warning unify MIN/MAX itemSize!
#endif
        itemSize = intFrame.size.width / count;

        for (int i = 0; i < 100; i++)
            vRects[i] = CGRectMake(-1, -3, -1, intFrame.size.height + 6);
    } else
        itemSize = intFrame.size.width;
    vRectCount = 0;
    [self recalcViewInFrame:intFrame ClippingInset:cInset];
}

- (BOOL) hasData {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"hasData" userInfo:nil];
}

- (void) setType:(ScaleRepresentation) v {
    if (self.representation) {
        if (v == _type) return;
    }
    _type = v;
    if (v == srCandles)
        _representation = [[CSCandleRepresentationCandles alloc] initWithScaler:self];
    else {
        if (v == srBars)
            _representation = [[CSScaleRepresentationBars alloc] initWithScaler:self];
        else
            _representation = [[CSScaleRepresentationLine alloc] initWithScaler:self];
    }
}

- (void) drawStripesInContext:(CGContextRef)context {
    if (vRectCount > 0) {
        CGFloat r, g, b, a;
        [_stripeColor getRed:&r green:&g blue:&b alpha:&a];
        CGContextSetRGBFillColor(context, r, g, b, a);
        CGContextAddRects(context, vRects, vRectCount);
        CGContextFillPath(context);
        [_gridColor getRed:&r green:&g blue:&b alpha:&a];
        CGContextSetRGBStrokeColor(context, r, g, b, a);
        CGContextSetLineWidth(context, _verticalGridWidth);
        CGContextAddRects(context, vRects, vRectCount);
        CGContextStrokePath(context);
    }
}

// SmoothScalerProtocol function:
- (void) correctMax:(inout CGFloat *)maxP Min:(inout CGFloat *)minP {
    if (hadPreviousScale) {
        if (hasScaleTension) {
            CFAbsoluteTime t = CFAbsoluteTimeGetCurrent();
            float k = (t - scaleTensionStart);
            if (k > compensationTime) { // tension compensated
                previousScaleH = desiredScaleH;
                previousScaleL = desiredScaleL;
                hasScaleTension = NO;
            } else {
                previousScaleH = (desiredScaleH * k + previousScaleH * (compensationTime - k)) / compensationTime;
                previousScaleL = (desiredScaleL * k + previousScaleL * (compensationTime - k)) / compensationTime;
            }
        }
        if ((ABS(desiredScaleH - *maxP) > EPSILON) || (ABS(desiredScaleL - *minP) > EPSILON)) { // new scale tension direction!
            desiredScaleH = *maxP;
            desiredScaleL = *minP;
            scaleTensionStart = CFAbsoluteTimeGetCurrent();
            hasScaleTension = YES;
        }
        *maxP = previousScaleH;
        *minP = previousScaleL;
    } else {
        hadPreviousScale = YES;
        desiredScaleH = previousScaleH = *maxP;
        desiredScaleL = previousScaleL = *minP;
    }
}

- (BOOL) drawPresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"drawPresentationInContext" userInfo:nil];
}

- (BOOL) drawParameter:(NSString *) param PresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen {
    return NO;
}

- (void) drawInContext:(CGContextRef) context NeedsOverlayDrawing:(BOOL *)needsOverlayDrawing {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"drawInContext" userInfo:nil];
}

- (void) drawOverlaysInContext:(CGContextRef) context {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"drawOverlaysInContext" userInfo:nil];
}

- (double) markZoneBetween:(CGFloat) x0 And:(CGFloat) x1 HLOC0:(out HLOCV *) hloc0 HLOC1:(out HLOCV *) hloc1 Dates:(out NSArray **) dates {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"markZoneBetween" userInfo:nil];
}

- (void) hintOn:(CGFloat) x HLOC:(out NSArray **) data Date:(out NSString **) date {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"hintOn" userInfo:nil];
}

- (void) enumerateVerticalGridUsingBlock:(GridEnum)block ForMinimalWidth:(float)minW IntFrame:(CGRect)intFrame {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"enumerateVerticalGridUsingBlock" userInfo:nil];
}

- (CSLabelComposition *) arrangePriceLabelsForMinimalWidth:(float) minW IntFrame:(CGRect) intFrame Previous:(CSLabelComposition *)previousComposition {
    #ifdef mywarns
    #warning minW, intFrame, previousComposition not used?
    #endif
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"arrangePriceLabelsForMinimalWidth" userInfo:nil];  
}

- (void) dropData {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"dropData" userInfo:nil];
}

- (FeedingResult) feedingDone {
    @throw [NSException exceptionWithName:@"Abstract method called!" reason:@"feedingDone" userInfo:nil];
}

@end

@implementation CSLabelComposition

//@synthesize candleItemInterval;

-(id) initFor:(CandleInterval)ci MinLabelWidth:(float) minWidth {
    self = [super init];
    if (self) {
        candleItemInterval = ci;
        minW  = minWidth;
        switch (candleItemInterval) {
            case ciMonth:
                mask1 = hMonthChange;
                break;
            case ciWeek:
                mask1 = hWeekChange;
                break;
            case ciDay:
                mask1 = hDayChange;
                break;
            case ciHour:
                mask1 = hHOURChange;
                break;
            case ci10Minutes:
                mask1 = h10MinChange;
                break;
            case ciMinute:
                mask1 = hNormal;
                break;
            default:
                mask1 = hYearChange;
                break;
        }
    }
    return self;
}

//- (void)updateWith:(NSUInteger)lastUsedItemNo CountLimit:(NSUInteger)countLimit {
//    NSString *time;
//    NSString *date;
//    int maxTimesCount = 10;//(intFrame.size.width / minW) + 1;
//    int lastGoodMask = 0;
//    NSUInteger lastGoodCount = 0;
//    for (; mask1 < hYearChange; mask1 <<= 1) {
//        NSUInteger icnt = [self hlocsOfMask:mask1 Since:lastUsedItemNo Till:countLimit];
//        if (icnt) {
//            if (icnt > lastGoodCount) {
//                lastGoodCount = icnt;
//                lastGoodMask = mask1;
//            }
//            if (icnt > maxTimesCount)
//                continue;
//        } else
//            if (lastGoodCount > 0)
//                mask1 = lastGoodMask;
//        break;
//    }
//    NSDateFormatter *m1f = self.axisFormatterTime;
//    NSDateFormatter *m2f = self.axisFormatterDateBig;
//    int mask2 = hYearChange;
//    switch (mask1) {
//        case hYearChange:
//            mask1 = 0;
//            m1f = nil;
//            m2f = self.axisFormatterYear;
//            break;
//        case hMonthChange:
//        case hWeekChange:
//            mask2 = hYearChange;
//            m1f = self.axisFormatterDate;
//            if (mask1 == hMonthChange)
//                [m1f setDateFormat:@"MMM"];
//            else
//                [m1f setDateFormat:@"MM dd"];
//            m2f = self.axisFormatterYear;
//            break;
//        case hDayChange:
//            mask2 = hMonthChange;
//            m1f = self.axisFormatterDay;
//            m2f = self.axisFormatterDate;
//            [m2f setDateFormat:@"MMM yyyy"];
//            break;
//        default:
//            mask2 = hDayChange;
//            break;
//    }
//    if (m1f) {
//        int idx = 0;
//        int hadDate = NO;
//        for (int item = lastUsedItemNo; item < count; item++) {
//            if (hlocs[item].tag & (mask1 | mask2)) {
//                float labelX = ((item - startIndex)*itemSize);
//                if (labelX < 0) continue;
//                if (labelX > intFrame.size.width) break;
//                NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[item]];//startIndex + (x / itemSize)];
//                if (!hadDate || ((hlocs[item].tag & mask2) != 0)) {
//                    date = [m2f stringFromDate:d];
//                    hadDate = YES;
//                } else
//                    date = nil;
//                if (mask1 && ((hlocs[item].tag & mask1) != 0)) {
//                    time = [m1f stringFromDate:d];
//                } else
//                    time = nil;
//                block(idx++, time, date, intFrame.origin.x + labelX);
//            }
//        }
//        if (!hadDate) {
//            NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[lastUsedItemNo]];
//            block(idx, nil, [m2f stringFromDate:d], intFrame.origin.x);
//        }
//    } else { // 'year' labels only
//        int idx = 0;
//        for (int item = lastUsedItemNo; item < count; item++) {
//            if ((hlocs[item].tag & mask2) || (item == lastUsedItemNo)) {
//                float labelX = ((item - startIndex)*itemSize);
//                if (labelX > intFrame.size.width) break;
//                NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[item]];//startIndex + (x / itemSize)];
//                date = [m2f stringFromDate:d];
//                block(idx++, nil, date, intFrame.origin.x + labelX);
//            }
//        }
//    }
//}

@end
