//
//  CSAbstractScaleRepresentation.h
//  chartster
//
//  Created by Danila Parhomenko on 4/17/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSEssentials.h"

#define TAGWIDTH (68/2)
#define TAGMARGIN (14/2)
#define TAGHEIGHT (40/2)

@class CSAbstractScaleRepresentation;

@protocol SmoothScalerProtocol

- (void) correctMax:(inout CGFloat *) maxP Min:(inout CGFloat *) minP;

@end

@interface CSAbstractScaleRepresentation : NSObject {
    NSUInteger lineCapacity;
    NSUInteger lineLength;

    CGFloat maxP, minP, maxValue;
    float rescale; //  - (intFrame.size.height - 2)/(maxP - minP);
    float offset; // - maxP*rescale;
    float volumeRescale; // - (volumeFrame.size.height - 2)/maxValue
    float volumeOffset; // - maxV*volumeRescale;
    
    CGRect priceFrame, volumeFrame;
    // volume stuff
    CGRect *volumeBodies;
    NSUInteger volumeCapacity;
    NSUInteger volumeCount;
    CGRect *animVolumeBodies;
    CGFloat mainR, mainG, mainB, mainAlpha;
}

@property (nonatomic, strong) NSNumber *lineWidth;
@property (nonatomic, strong) NSNumber *stickWidth;
@property (nonatomic) BOOL monochrome;
#ifdef mywarns
#warning non-monochrome candle support pending
#endif
@property (nonatomic, setter = setMainColor:, strong) UIColor *mainColor;
@property (nonatomic, weak) id<SmoothScalerProtocol> scaler;

- (id) initWithScaler:(id<SmoothScalerProtocol>) sc;
- (void) setLineLength:(int) theLineLength HLOCVs:(HLOCV *)hlocs ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect) rVolume;
- (void) drawPresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add;
- (void) drawInContext:(CGContextRef)context;
- (double) priceAtY:(float) y;
- (float) yForPrice:(double) p;
- (float) lastX;
- (float) firstX;
- (void) useMainColorOn:(CGContextRef) context;

@end
