//
//  CSAppDelegate.h
//  chartster
//
//  Created by Itheme on 4/2/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
