//
//  CSCandleRepresentationCandles.h
//  chartster
//
//  Created by Danila Parhomenko on 4/17/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSAbstractScaleRepresentation.h"

@interface CSCandleRepresentationCandles : CSAbstractScaleRepresentation {
    CGPoint *placeholder;
    CGRect *candleBodiesRise;
    CGRect *candleBodiesFall;
    CGPoint *candleBodiesSame;
    NSUInteger riseCount;
    NSUInteger fallCount;
    NSUInteger sameCount;
    NSUInteger highsLowsCount;
    CGPoint *highsLows;
    float *tmp;
    
    CGRect *animCandleBodiesRise;
    CGRect *animCandleBodiesFall;
    CGPoint *animCandleBodiesSame;
    CGPoint *animHighsLows;
#ifdef mywarns
#warning all anim* ptrs ^^ not used
#endif

}

@end
