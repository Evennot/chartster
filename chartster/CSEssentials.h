//
//  CSEssentials.h
//  chartster
//
//  Created by Itheme on 4/5/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#ifndef chartster_CSEssentials_h
#define chartster_CSEssentials_h

//static NSString *kMinute1 = @"M";

typedef enum CandleIntervalEnum {
    ciMinute = 1,
    ci10Minutes = 2,
    ciHour = 3,
    ciDay = 4,
    ciWeek = 5,
    ciMonth = 6
} CandleInterval;

typedef enum ScaleRepresentationTag {
    srLine = 0,
    srCandles = 1,
    srBars = 2,
    srMultiChartLine = 3
} ScaleRepresentation;

typedef struct HLOCStruct {
    float high, low, open, close, value;
    int32_t tag;
} HLOCV;

enum HLOCTagEnum {
    hPseudo = 1,
    hNormal = 1 << 1, // minutes or so
//    h5MinChange = 1 << 2,
    h10MinChange = 1 << 3,
    h15MinChange = 1 << 4,
    h30MinChange = 1 << 5,
    hHOURChange = 1 << 6,
    h4HourChange = 1 << 7,
    hDayChange = 1 << 8,
    hWeekChange = 1 << 9,
    hMonthChange = 1 << 10,
    hYearChange = 1 << 11
};

typedef enum FeedingResultEnum {
    frBad = 0,
    frLastCandleUpdateOnly = 1,
    frNewCandlesAdded = 2,
    frCompletelyNewData = 3
} FeedingResult;

#define HLOCVStride 6

#define EPSILON 0.0000005

#define PRICEFRAMEK (4.0/5.0)
#define VOLUMEFRAMEK (1.0/5.0)
#define AXISBODYMARGIN 1.5


typedef void(^GridEnum)(NSUInteger idx, NSString *title, NSString *subTitle, float desiredCoordinate);

@protocol CSChartLayoutManager

- (void) presentationStarted; // some action (or part of an action) starts
- (void) presentationEnded; // some action (or part of an action) ended. Actions like presentation, moving, scaling, decelerating... method should be renamed probably
- (void) decelerate;
- (void) newScaleLoaded:(CandleInterval) interval;
- (void) compensateTension;
- (void) needsScaleTensionCompensationTo:(CGFloat) targetScale;
- (void) needsPanTensionCompensationToPosition:(CGFloat)pos;

- (void) zoneMarkMovedToP1:(CGPoint) p1 P2:(CGPoint) p2;
- (void) singleMarkMovedToP1:(CGPoint) p;

@end

// CSView property categories
extern NSString *kPropertySectionGeneral;
extern NSString *kPropertyKeySectionSubData;
extern NSString *kPropertySectionVGrid;
extern NSString *kPropertySectionHGrid;
extern NSString *kPropertySectionAxes;
// CSView visual properties
extern NSString *kPropertyKeyWidth;
extern NSString *kPropertyKeyElementWidth; // line width for candles, bars, etc
extern NSString *kPropertyKeyColor; // in simple cases
extern NSString *kPropertyKeyStrokeColor; // in cases of different colors for stroke and fill
extern NSString *kPropertyKeyFillColor; // in cases of different colors for stroke and fill
extern NSString *kPropertyKeyVerticalBars; // @BOOL drawing vertical stripes or not
extern NSString *kPropertyKeyRiseColor; // candles/bars only
extern NSString *kPropertyKeyFallColor; // candles/bars only
extern NSString *kPropertyKeyMonochrome; // @BOOL monochrome candles style

extern float frictionX;
extern float frictionScale;

extern double intervalLengths[10];

#endif
