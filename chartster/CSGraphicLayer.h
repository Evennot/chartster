//
//  CSGraphicLayer.h
//  chartster
//
//  Created by Itheme on 4/3/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//
//  CSGraphicLayer contains current CSScale and hidden CSScale for new interval if it was not downloaded yet
//
#import <QuartzCore/QuartzCore.h>
#import "CSEssentials.h"
#import "CSSubContainer.h"
#import "CSAbstractScale.h"

typedef enum DecelerationStateTag {
    dsStill = 0, // no deceleration. Looks like everything is compensated
    dsNotYet = 1, // no deceleration. User has fingers on the chart
    dsPanAfterTouch = 2,
    dsScaleAfterTouch = 4,
    dsAfterTouches = 6,
    dsPanTensionCompensation = 8,
    dsScaleTensionCompensation = 0x10,
    dsTensionCompensations = 0x18
} DecelerationState;

@interface CSGraphicLayer : CALayer

@property (nonatomic, strong) id timer;
@property (nonatomic, weak) id<CSChartLayoutManager> masterView;
@property (nonatomic, strong, readonly) CSAbstractScale *currentScale;
@property (nonatomic, setter = setCandleItemInterval:) CandleInterval candleItemInterval;
@property (nonatomic, setter = setDecelerating:) DecelerationState decelerating;
@property (nonatomic, setter = setPercentMode:, getter = getPercentMode) BOOL percentMode;
@property (nonatomic, setter = setChartType:, getter = getChartType) ScaleRepresentation type;
@property (nonatomic) BOOL animated;
@property (nonatomic, readonly, strong) NSMutableDictionary *visualProps;
@property (nonatomic, getter = getDesiredItemSize, setter = setDesiredItemSize:) CGFloat desiredItemSize;
- (id) initWithHorizontalLineCount:(NSUInteger) hCount Owner:(id<CSChartLayoutManager>) master;

- (void)realignAnimated:(BOOL) animated XRescale:(double) xrescale;
- (void)recalcViewInFrame:(CGRect) intFrame;

- (void)rescale:(float) scale Around:(CGPoint) p;
- (void)pan:(float) x;
- (float)tryPan:(float) x; // in case of deceleration. Without display invalidation. Returns panning difficulty: ~1: OK, ~0: difficult, < 0 impossible
- (void) dropMainData;
- (void) dropSubData:(NSNumber *)subScaleId;
- (void) dropAllData;
#ifdef mywarns
#warning ^^ not used yet
#endif

- (void) feedH:(id)h L:(id)l O:(id)o C:(id)c V:(id)v AtTime:(NSString *)time;

// prepare to feeding main (or single) chart:
- (void) beginUpdateWithEstimatedCandleCount:(NSUInteger)count ScaleId:(NSNumber *)scaleId Decimals:(int)decimals;

// feeding is over:
- (FeedingResult) endUpdate;

// prepare to feeding sub chart:
- (CSSubContainer *) beginUpdateSubScale:(NSNumber *)subScaleId Decimals:(int)decimals;

//- (BOOL) feedNews:(int)newsIdx At:(NSDate *) time;
//- (void) placeNews:(NSMutableArray *)news;
- (double) priceAtHLine:(NSUInteger) idx;
- (double) percentPriceAtHLine:(NSUInteger) idx;
- (CGFloat) hLineY:(NSUInteger) idx;

- (void) enumerateVerticalGridUsingBlock:(GridEnum) block ForMinimalWidth:(float) minW;

- (void) presentWithDecimals:(NSNumber *)decimals;

- (NSUInteger) alignHLines:(int)decimals;

- (void) markZoneBetween:(CGFloat) x0 And:(CGFloat) x1 Block:(void (^)(double delta, NSString *date0, NSString *date1)) cbBlock;
- (void) unmark;
- (void) hintOn:(CGFloat) x Block:(void (^)(NSArray *data, NSString *date)) cbBlock;

- (void) setGraphProperties:(NSDictionary *)props;

@end
