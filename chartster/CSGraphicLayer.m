//
//  CSGraphicLayer.m
//  chartster
//
//  Created by Itheme on 4/3/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSGraphicLayer.h"
#import "CSAbstractScale.h"
#import "CSScale.h"
#import "CSMultiScale.h"

const int mainPresentationProgress = 1;
const int subPresentationProgress = 2;

@interface CSGraphicLayer () {
    int presentationInProgress; // 1: main presentation 2: subchart presentation
    BOOL needsMainPresentation;
    BOOL _percentMode;
    BOOL feedingNewSubChart;
    
    CFAbsoluteTime animationStart;

    NSUInteger activeLines;
    
    CGFloat *hLinesYs;
    
    CGFloat horGridR, horGridG, horGridB, horGridA, horGridWidth, axisLineWidth;
    
}

@property (nonatomic, strong) UIImage *leftShadowImage;
@property (nonatomic, strong) UIImage *rightShadowImage;
@property (nonatomic, strong) UIImage *bottomShadowImage;
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic) NSUInteger hLineCount;

@property (nonatomic, strong) CSAbstractScale *currentScale;
@property (nonatomic, strong) CSAbstractScale *upcomingScale; // scale of a new interval. If it was not loaded yet
@property (nonatomic, weak) CSAbstractScale *feededScale;
@property (nonatomic, weak) CSSubContainer *feededSubChart;
@property (nonatomic, strong) NSString *mainScaleId;
//@property (nonatomic, strong) NSMutableDictionary *loadedScales;
@property (nonatomic, strong) NSMutableDictionary *visualProps;
@property (nonatomic, strong) NSArray *presentatingSubCharts;

@end

@implementation CSGraphicLayer

static NSArray *allTheSections = nil;
static NSArray *allTheProperties = nil;

- (id) initWithHorizontalLineCount:(NSUInteger) hCount Owner:(id<CSChartLayoutManager>) master {
    self = [super init];
    if (self) {
        _animated = YES;
        _candleItemInterval = 0;
        if (allTheProperties == nil) {
            allTheProperties = @[kPropertyKeyWidth, kPropertyKeyElementWidth,kPropertyKeyVerticalBars, kPropertyKeyColor, kPropertyKeyFillColor, kPropertyKeyStrokeColor, kPropertyKeyRiseColor, kPropertyKeyFallColor, kPropertyKeyMonochrome, kPropertyKeySectionSubData];
            allTheSections = @[kPropertySectionGeneral, kPropertySectionHGrid, kPropertySectionVGrid, kPropertySectionAxes];
        }
        NSArray *subChartsProps = @[@{kPropertyKeyWidth: @2.0, kPropertyKeyColor: [UIColor colorWithRed:255.0/255.0 green:206.0/255.0 blue:0.0 alpha:1.0]},
                                    @{kPropertyKeyWidth: @1.5, kPropertyKeyColor: [UIColor colorWithRed:152.0/255.0 green:173.0/255.0 blue:64.0/255.0 alpha:1.0]},
                                    @{kPropertyKeyWidth: @1.5, kPropertyKeyColor: [UIColor colorWithRed:41.0/255.0 green:101.0/255.0 blue:185.0/255.0 alpha:1.0]},
                                    @{kPropertyKeyWidth: @1.0, kPropertyKeyColor: [UIColor colorWithRed:255.0/255.0 green:109.0/255.0 blue:182.0/255.0 alpha:1.0]},
                                    @{kPropertyKeyWidth: @1.0, kPropertyKeyColor: [UIColor colorWithRed:255.0/255.0 green:152.0/255.0 blue:0.0 alpha:1.0]},
                                    @{kPropertyKeyWidth: @0.5, kPropertyKeyColor: [UIColor colorWithRed:22.0/255.0 green:160.0/255.0 blue:133.0/255.0 alpha:1.0]}];
        self.visualProps = [@{
                            kPropertySectionGeneral:
                              [@{kPropertyKeyWidth: @3.0, kPropertyKeyElementWidth: @1.5,
                                 kPropertyKeyColor: [UIColor colorWithRed:38/255.0 green:167/255.0 blue:214/255.0 alpha:1.0],
                                 //kPropertyKeyRiseColor, kPropertyKeyFallColor
                                 kPropertyKeyMonochrome: @TRUE,
                                 kPropertyKeySectionSubData: subChartsProps
                                 } mutableCopy],
                            kPropertySectionHGrid:
                                [@{kPropertyKeyWidth: @0.5,
                                 kPropertyKeyColor: [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0]} mutableCopy],
                            kPropertySectionVGrid:
                                [@{kPropertyKeyWidth: @0.5,
                                 kPropertyKeyVerticalBars: @TRUE,
                                 kPropertyKeyStrokeColor: [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0],
                                 kPropertyKeyFillColor: [UIColor colorWithRed:0.65 green:0.65 blue:0.6 alpha:0.2]} mutableCopy],
                            kPropertySectionAxes :
                                [@{kPropertyKeyWidth: @1.0} mutableCopy]
                            } mutableCopy];

        self.contentsScale = [[UIScreen mainScreen] scale];
        //
        //self.shadowImage = [[UIImage imageNamed:@"shadow.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(40, 40, 40, 40)];
        self.leftShadowImage = [UIImage imageNamed:@"graph_inner_shadow_left@2x.png"];
        self.rightShadowImage = [UIImage imageNamed:@"graph_inner_shadow_right@2x.png"];
        self.bottomShadowImage = [UIImage imageNamed:@"graph_inner_shadow_top@2x.png"];
        self.candleItemInterval = ci10Minutes;
        _hLineCount = hCount;
        self.masterView = master;
        self.formatter = [[NSDateFormatter alloc] init];
        [self.formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
        [self applyPropertiesToScale];
    }
    return self;
}

- (void) dealloc {
    if (hLinesYs)
        free(hLinesYs);
}

- (void) recalcViewInFrame:(CGRect) intFrame {
    [_currentScale recalcViewInFrame:CGRectOffset(CGRectInset(intFrame, 1.5, 0), -1.5, 0) ClippingInset:axisLineWidth*2 + AXISBODYMARGIN];
}

- (void) updateMarks {
    switch (self.currentScale.mark) {
        case mmMarkZone:
            [self.masterView zoneMarkMovedToP1:self.currentScale.markZoneBalloonPointL P2:self.currentScale.markZoneBalloonPointR];
            break;
        case mmHint:
            [self.masterView singleMarkMovedToP1:self.currentScale.markZoneBalloonPointL];
            break;
        default:
            break;
    }
}

- (void)rescale:(float) scale Around:(CGPoint) p {
    if (presentationInProgress) return;
    [_currentScale rescale:scale Around:p];
    [self updateMarks];

    [self setNeedsDisplay];
    //[self.timer setPaused:NO];
}

- (void)pan:(float) x {
    if (presentationInProgress) return;
    //[self.timer setPaused:NO];
    [_currentScale pan:x];
    [self updateMarks];

    [self setNeedsDisplay];
}

- (float)tryPan:(float) x { // User:(NSString *)user {
//    NSLog(@"tp %f %@", x, user);
    float res = [_currentScale tryPan:x];
    [self updateMarks];
    return res;
}

- (void) setDecelerating:(DecelerationState)v {
    if (presentationInProgress) return;
    if (_decelerating == v) return;
    if (_decelerating == dsScaleTensionCompensation) {
        if (v == dsPanTensionCompensation) return;
    } else {
        if (_decelerating == dsPanTensionCompensation) {
            if (v == dsScaleTensionCompensation) return;
        }
    }
    //NSLog(@"%d", v);
    DecelerationState old = _decelerating;
    _decelerating = v;
    if (v == dsStill) {
        //if (old != dsPanTensionCompensation)
            if ([_currentScale checkNeededCompensation:old == dsScaleTensionCompensation ForMasterView:self.masterView])
                [self.timer setPaused:NO];
    } else
        [self.timer setPaused:NO];
}

- (int) findFittedLineCountFor:(double) pDelta MaxStride:(double) x1Lim MinStride:(double) x1Min ResultStrive:(double *)fittedX2{
    int fittedLineCount = -1;
    double fittedX2Error = pDelta;
    for (int ac = self.hLineCount; ac >= 5; ac--) {
        int i = 0;
        for (double x1 = x1Min; x1 < x1Lim; i++) {
            double x2;
            switch (i % 4) {
                case 0:
                    x2 = x1;
                    break;
                case 1:
                    x2 = x1 * 1.5;
                    break;
                case 2:
                    x2 = x1 * 2.5;
                    break;
                default:
                    x2 = x1 * 5;
                    x1 *= 10;
                    break;
            }
            double cDelta = x2 * ac;
            if (fittedX2Error > ABS(pDelta - cDelta)) {
                *fittedX2 = x2;
                fittedX2Error = ABS(pDelta - cDelta);
                fittedLineCount = ac;
            } else
                if (pDelta < cDelta + x2)
                    break;
        }
    }
    return fittedLineCount;
}

- (void) alignFirstLineOf:(double) maxVisiblePrice AndStrive:(double) hStrive In:(CGRect) intFrame {
    CGFloat lastY = [_currentScale yForPrice:maxVisiblePrice - (activeLines - 1)*hStrive];
    if (lastY > intFrame.size.height/* + intFrame.origin.y*/)
        activeLines--;
    else {
        lastY = [_currentScale yForPrice:maxVisiblePrice - activeLines*hStrive];
        if (lastY < intFrame.size.height/* + intFrame.origin.y*/)
            maxVisiblePrice -= hStrive;
    }
    for (int i = activeLines; i--; ) hLinesYs[i] = [_currentScale yForPrice:maxVisiblePrice - hStrive*i];
}

- (NSUInteger) alignHLines:(int) decimals {
    CGRect intFrame = self.frame;
    intFrame.size.height *= PRICEFRAMEK;
    intFrame.size.height -= 5;
    
    double maxVisiblePrice = [_currentScale priceAtY:0/*intFrame.origin.y*/];
    double minVisiblePrice = [_currentScale priceAtY:intFrame.size.height/* + intFrame.origin.y*/];
    double pDelta = maxVisiblePrice - minVisiblePrice;
    int fittedLineCount = -1;
    if (pDelta < EPSILON) {
        activeLines = 0;
        return 0;
    }
    if (self.percentMode) {
        double last = [_currentScale lastClose];
        double fittedX2 = -1;
        double maxPercent = (maxVisiblePrice - last)/last;
        double minPercent = (minVisiblePrice - last)/last;
        fittedLineCount = [self findFittedLineCountFor:maxPercent - minPercent MaxStride:100 MinStride:0.0001 /* before 100% multiplication */ ResultStrive:&fittedX2];
        if (fittedLineCount > 0) {
            activeLines = fittedLineCount;
            double maxRest = fmod(maxPercent, fittedX2);
            maxPercent -= maxRest;
            maxVisiblePrice = (maxPercent + 1.0)*last;
            [self alignFirstLineOf:maxVisiblePrice AndStrive:fittedX2*last In:intFrame];
            return activeLines;
        }
    } else {
        int minPDeltaOrder = log10(pDelta / 5 / self.hLineCount) - 1;
        int maxPDeltaOrder = log10(pDelta * 5);
        double fittedX2 = -1;
        if (decimals < -minPDeltaOrder)
            minPDeltaOrder = -decimals;
        double x1Lim = pow(10, maxPDeltaOrder); // 1000
        double x1Min = pow(10, minPDeltaOrder); // 0.0001
        fittedLineCount = [self findFittedLineCountFor:pDelta MaxStride:x1Lim MinStride:x1Min ResultStrive:&fittedX2];
        if (fittedLineCount > 0) {
            activeLines = fittedLineCount;
            double maxRest = fmod(maxVisiblePrice, fittedX2);
            maxVisiblePrice -= maxRest;
            [self alignFirstLineOf:maxVisiblePrice AndStrive:fittedX2 In:intFrame];
            return activeLines;
        }
    }
    activeLines = self.hLineCount;
    double pixelsPerLine = intFrame.size.height/(activeLines + 1.0);
    for (int i = activeLines; i--; ) hLinesYs[i] = /*intFrame.origin.y +*/ ((i + 1)*pixelsPerLine);
    return activeLines;
}

- (void) realignAnimated:(BOOL) animated XRescale:(double) xrescale {
    CGRect intFrame = self.frame;
    if (animated && (ABS(xrescale - 1.0) > 0.01)) {
        [_currentScale rescale:xrescale Around:CGPointMake(intFrame.size.width*0.5, intFrame.size.height*0.5)];
        [_currentScale recalcViewInFrame:intFrame ClippingInset:axisLineWidth*2 + AXISBODYMARGIN];
        [_currentScale checkNeededCompensation:NO ForMasterView:self.masterView];
        [self setNeedsDisplay];
    } else
        [_currentScale realign:intFrame ClippingInset:axisLineWidth*2 + AXISBODYMARGIN];
    
    hLinesYs = malloc(sizeof(CGFloat) * self.hLineCount);
    intFrame.size.height *= PRICEFRAMEK;
    intFrame.size.height -= 1.5;

    double pixelsPerLine = intFrame.size.height/(self.hLineCount + 1.0);
    for (int i = self.hLineCount; i--; ) hLinesYs[i] = /*intFrame.origin.y + */((i + 1)*pixelsPerLine);

    //[self unmark];

    [self presentAgain];
}

- (void) drawInContext:(CGContextRef)context {
    if (_currentScale.badScale) return;
    CGRect intFrame = self.frame;
    CGFloat wx = intFrame.size.width - (axisLineWidth * 2) - AXISBODYMARGIN;
    if ([_currentScale hasData]) {
#ifdef mywarns
#warning CGImage exc bad access in argb32_sample_rectilinear_argb32 right after transform sometimes
#endif
        CGContextDrawImage(context, CGRectMake(0, 0, 11, intFrame.size.height * PRICEFRAMEK), self.leftShadowImage.CGImage);
        CGContextDrawImage(context, CGRectMake(wx - 11, 0, 11, intFrame.size.height * PRICEFRAMEK), self.rightShadowImage.CGImage);
        CGContextDrawImage(context, CGRectMake(0, intFrame.size.height * PRICEFRAMEK - 11, wx, 11), self.bottomShadowImage.CGImage);
    }
    if (presentationInProgress) {
        float drawen = ((CFAbsoluteTimeGetCurrent() - animationStart)) * 1.8;
        if (presentationInProgress & mainPresentationProgress) {
            if ([_currentScale drawPresentationInContext:context Frame:intFrame Done:drawen])
                return;
            if (self.masterView)
                [self.masterView presentationEnded];
            presentationInProgress = 0;
        } else {
            if (self.presentatingSubCharts) {
                if ([_currentScale drawParameter:self.presentatingSubCharts[0] PresentationInContext:context Frame:intFrame Done:drawen])
                    return;
                if (self.presentatingSubCharts.count > 1) {
                    self.presentatingSubCharts = [self.presentatingSubCharts subarrayWithRange:NSMakeRange(1, self.presentatingSubCharts.count - 1)];
                    animationStart = CFAbsoluteTimeGetCurrent();
                    [self.timer setPaused:NO];
                    return;
                }
                self.presentatingSubCharts = nil;
                if (self.masterView)
                    [self.masterView presentationEnded];
            } else
                if (self.masterView)
                    [self.masterView presentationEnded];
            presentationInProgress = 0;
        }
        //if (!_deceleratingX && !_deceleratingScale)
        //    [self.timer setPaused:YES];
    }
    //[self.shadowImage drawInRect:intFrame];
    BOOL needsOverlays = NO;
    [_currentScale drawInContext:context NeedsOverlayDrawing:&needsOverlays];

    //CGContextClipToRect(context, CGRectMake(0, 0, intFrame.size.width, intFrame.size.height));

    CGContextSetLineWidth(context, horGridWidth);
    CGContextSetRGBStrokeColor(context, horGridR, horGridG, horGridB, horGridA);
    // dashing
    CGFloat l[2] = {1, 3};
    CGFloat phase = 0;//[_currentScale startIndexMod:8];
    for (int i = activeLines; i--; ) {
        CGContextMoveToPoint(context, 0, hLinesYs[i]);
        CGContextSetLineDash(context, phase, l, 2);
        CGContextAddLineToPoint(context, intFrame.size.width - (axisLineWidth*2), hLinesYs[i]);
    }
    CGContextStrokePath(context);

    CGContextSetLineWidth(context, axisLineWidth);
    CGContextSetLineDash(context, 0, nil, 0);
    CGContextSetRGBStrokeColor(context, 0.6, 0.6, 0.6, 1.0);
    CGFloat p;// = (intFrame.size.height * PRICEFRAMEK);
    p = intFrame.size.width - 1.5 - axisLineWidth;
// non gradient axis line:
//    CGContextMoveToPoint(context, p, hLinesYs[0] - 1.5);
//    CGContextAddLineToPoint(context, p, hLinesYs[activeLines - 1] + 1.5);
    for (int i = activeLines; i--; ) {
        CGContextMoveToPoint(context, intFrame.size.width - (axisLineWidth*2), hLinesYs[i]);
        CGContextAddLineToPoint(context, intFrame.size.width, hLinesYs[i]);
    }
    CGContextStrokePath(context);
// pseudeo gradient axis line:
    CGFloat ly = (intFrame.size.height * PRICEFRAMEK - hLinesYs[activeLines - 1]);
    CGFloat fx = [_currentScale.representation firstX];
    CGFloat lx = [_currentScale.representation lastX] + 10;
    if (lx < 20)
        lx = 20;
    for (int i = 1; i < 7; i++) {
        CGContextSetRGBStrokeColor(context, 0.6, 0.6, 0.6, (1)/6.5);
        CGContextMoveToPoint(context, p, (hLinesYs[0])*i/5.0);
        CGContextAddLineToPoint(context, p, ly*i/6.0 + hLinesYs[activeLines - 1]);

        CGContextMoveToPoint(context, fx + 20 * i/7, intFrame.size.height * PRICEFRAMEK + AXISBODYMARGIN);
        CGContextAddLineToPoint(context, lx - (20*i/7), intFrame.size.height * PRICEFRAMEK + AXISBODYMARGIN);

        CGContextMoveToPoint(context, fx + 20 * i/7, intFrame.size.height - axisLineWidth/2);
        CGContextAddLineToPoint(context, lx - (20*i/7), intFrame.size.height - axisLineWidth/2);
        
        CGContextStrokePath(context);
    }

    //CGContextRestoreGState(context);
    
    if (needsOverlays)
        [_currentScale drawOverlaysInContext:context];
    
    DecelerationState ds = self.decelerating;
    if (ds & dsAfterTouches)
        [_masterView decelerate];
    else {
        if (ds & dsTensionCompensations)
            [_masterView compensateTension];
        else {
            if (![_currentScale hasVerticalScaleTension])
                [self.timer setPaused:YES];
            [_masterView presentationEnded];
        }
    }
}

- (void) presentWithDecimals:(NSNumber *)decimals {
    if (_currentScale)
        [_currentScale setDecimals:decimals];
    if (_upcomingScale)
        [_upcomingScale setDecimals:decimals];
    [self presentAgain];
}

- (void) presentAgain {
    if (_animated) {
        if (presentationInProgress)
            return;
        if (!needsMainPresentation && (self.presentatingSubCharts == nil)) {
            if (self.masterView) {
                [self.masterView presentationStarted];
                [self.masterView presentationEnded];
            }
            return;
        }
        if (needsMainPresentation) {
            presentationInProgress = mainPresentationProgress;
            needsMainPresentation = NO;
            if (self.masterView)
                [self.masterView presentationStarted];
        } else
            if (self.presentatingSubCharts)
                presentationInProgress = subPresentationProgress;
        animationStart = CFAbsoluteTimeGetCurrent();
        if (self.timer == nil) {
            if ( NSClassFromString(@"CADisplayLink") ) {
                self.timer = [CADisplayLink displayLinkWithTarget:self selector:@selector(setNeedsDisplay)];
                ((CADisplayLink*)self.timer).frameInterval = 2;
                [self.timer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            } else {
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0 target:self selector:@selector(setNeedsDisplay) userInfo:nil repeats:YES];
            }
        } else
            [self.timer setPaused:NO];
    } else {
        if (self.masterView) {
            [self.masterView presentationStarted];
            [self.masterView presentationEnded];
        }
        [self setNeedsDisplay];
    }
}

- (double) priceAtHLine:(NSUInteger) idx {
    if (idx >= self.hLineCount) return 0;
    return [_currentScale priceAtY:hLinesYs[idx]/* - self.frame.origin.y*/];
}

- (double) percentPriceAtHLine:(NSUInteger) idx {
    return [self priceAtHLine:idx]*100.0/[_currentScale lastClose];
}

- (CGFloat) hLineY:(NSUInteger) idx {
    if (idx >= activeLines) return 0;
    return hLinesYs[idx] + self.frame.origin.y;
}

- (void) markZoneBetween:(CGFloat) x0 And:(CGFloat) x1 Block:(void (^)(double delta, NSString *date0, NSString *date1)) cbBlock {
    if (x0 > x1)
        [self markZoneBetween:x1 And:x0 Block:cbBlock];
    else {
        HLOCV h0, h1;
        NSArray *dates;
        double delta = [self.currentScale markZoneBetween:x0 And:x1 HLOC0:&h0 HLOC1:&h1 Dates:&dates];
        if (ABS(delta) > EPSILON) {
            cbBlock(delta, dates[0], dates[1]);
            [self.masterView zoneMarkMovedToP1:CGPointMake(x0, [_currentScale yForPrice:h0.close]) P2:CGPointMake(x1, [_currentScale yForPrice:h1.close])];
        }
        [self setNeedsDisplay];
    }
}

- (void) unmark {
    self.currentScale.mark = mmNone;
    [self.masterView zoneMarkMovedToP1:CGPointZero P2:CGPointZero];
    [self.masterView singleMarkMovedToP1:CGPointZero];
}

- (void) hintOn:(CGFloat) x Block:(void (^)(NSArray *, NSString *))cbBlock {
    NSArray *hloc;
    NSString *date;
    [self.currentScale hintOn:x HLOC:&hloc Date:&date];
    cbBlock(hloc, date); 
    [self.masterView singleMarkMovedToP1:CGPointMake(x, 0)];
    [self setNeedsDisplay];
}

- (void) enumerateVerticalGridUsingBlock:(GridEnum) block ForMinimalWidth:(float) minW {
    [_currentScale enumerateVerticalGridUsingBlock:block ForMinimalWidth:minW IntFrame:self.frame];
}

- (void) dropMainData {
    [_currentScale dropData];
    presentationInProgress = 0;
    needsMainPresentation = YES;
    self.presentatingSubCharts = nil;
    _decelerating = dsStill;
    [self.timer setPaused:YES];
    [self recalcViewInFrame:self.frame];
}

- (void) dropSubData:(NSNumber *)subScaleId {
#ifdef mywarns
#warning ELSE
#endif

//    if (self.subScales) {
//        CSScale *ss = [self.subScales valueForKey:[subScaleId stringValue]];
//        if (ss) {
//            [ss dropData];
//            [ss recalcViewInFrame:CGRectOffset(CGRectInset(self.frame, 1.5, 0), -1.5, 0) ClippingInset:axisLineWidth*2];
//        }
//    }
}

- (void) dropAllData {
    [self dropMainData];
}

- (void) setCandleItemInterval:(CandleInterval)v {
    if (_candleItemInterval == v) return;
    _candleItemInterval = v;
    ScaleRepresentation l = srLine;
    if (self.currentScale)
        l = self.currentScale.type;
    if (self.upcomingScale) {
        if (self.upcomingScale.candleItemInterval == v)
            return;
    }
    self.upcomingScale = [[CSScale alloc] initFor:v As:l];
    //needPresentation = YES;
    //[self applyPropertiesToScale];
}

- (CGFloat) getDesiredItemSize {
    if (_currentScale)
        return _currentScale.desiredItemSize;
    return 0;
}

- (void) setDesiredItemSize:(CGFloat)dis {
    if (_currentScale)
        _currentScale.desiredItemSize = dis;
}

- (void) feedH:(id)h L:(id)l O:(id)o C:(id)c V:(id)v AtTime:(NSString *)time {
    NSTimeInterval t = [[_formatter dateFromString:time] timeIntervalSinceReferenceDate];
    if (_feededSubChart)
        [_feededSubChart feedH:[h floatValue] L:[l floatValue] O:[o floatValue] C:[c floatValue] V:[v floatValue] AtTime:t];
    else
        [((CSScale *)_feededScale) feedH:[h floatValue] L:[l floatValue] O:[o floatValue] C:[c floatValue] V:[v floatValue] AtTime:t];
}

- (void) beginUpdateWithEstimatedCandleCount:(NSUInteger)count ScaleId:(NSNumber *)scaleId Decimals:(int)decimals {
    self.feededScale = nil;
    self.feededSubChart = nil;
    self.mainScaleId = [scaleId stringValue];
    if (_upcomingScale) {
        self.feededScale = _upcomingScale;
    } else {
        self.feededScale = _currentScale;
    }
    if (self.feededScale) {
        if ([self.feededScale isKindOfClass:[CSScale class]])
            [((CSScale *)_feededScale) beginFeeding:count];
        else
            self.feededSubChart = [((CSMultiScale *)self.feededScale) beginFeedingSubScaleId:[scaleId stringValue] Main:YES DecFormat:[NSString stringWithFormat:@"%%.%df", decimals]];
    } else
        self.mainScaleId = nil;
}

- (FeedingResult) endUpdate {
    FeedingResult r = [_feededScale feedingDone];
    if (r != frBad) {
        if ([_feededScale isEqual:self.upcomingScale]) {
            needsMainPresentation = YES;
            self.feededScale = nil;
            self.feededSubChart = nil;
            //self.mainScaleId = nil;
            self.currentScale = self.upcomingScale;
            self.upcomingScale = nil;
            feedingNewSubChart = NO;
            [self applyPropertiesToScale];
            [self.masterView newScaleLoaded:self.currentScale.candleItemInterval];
        } else {
            if (feedingNewSubChart) {
                if (self.feededSubChart) {
                    if (self.presentatingSubCharts)
                        self.presentatingSubCharts = [self.presentatingSubCharts arrayByAddingObject:self.feededSubChart.lastScaleId];
                    else
                        self.presentatingSubCharts = @[self.feededSubChart.lastScaleId];
                    self.feededScale = nil;
                    self.feededSubChart = nil;
                }
                feedingNewSubChart = NO;
                [self applyPropertiesToScale];
                r = frCompletelyNewData;
            }
        }
    } else {
        feedingNewSubChart = NO;
    }
    return r;
}

- (CSSubContainer *) beginUpdateSubScale:(NSNumber *)subScaleId Decimals:(int)decimals {
    self.feededScale = nil;
    self.feededSubChart = nil;
    if (_upcomingScale)
        self.feededScale = _upcomingScale;
    else
        self.feededScale = _currentScale;
    if (_feededScale) {
        if ([_feededScale isKindOfClass:[CSScale class]]) {
            CSMultiScale *neue = [[CSMultiScale alloc] initAsExtensionOf:(CSScale *)_feededScale
                                                              NewScaleId:_mainScaleId DecFormat:_feededScale.decimalledFormat];
            if ([_feededScale isEqual:_upcomingScale])
                self.feededScale = self.upcomingScale = neue;
            else
                self.feededScale = self.currentScale = neue;
            [self applyPropertiesToScale];
#warning maybe get rid of ^^
        }
        self.feededSubChart = [((CSMultiScale *)self.feededScale) beginFeedingSubScaleId:[subScaleId stringValue] Main:NO DecFormat:[NSString stringWithFormat:@"%%.%df", decimals]];
        if ([self.feededSubChart getData:nil Times:nil] == 0)
            feedingNewSubChart = YES;
        return self.feededSubChart;
    }
    return nil;
}

- (void) setPercentMode:(BOOL)pm {
    _percentMode = pm;
    [_masterView presentationEnded];
}

- (BOOL) getPercentMode {
    if ([_currentScale isKindOfClass:[CSScale class]])
        return _percentMode;
    // else it is multiscale and it could be only percented!
    return TRUE;
}

- (void) setChartType:(ScaleRepresentation)t {
    if (self.currentScale) {
        if (self.currentScale.type == t) return;
        self.currentScale.type = t;
        needsMainPresentation = YES;
        [self applyPropertiesToScale];
        [self realignAnimated:NO XRescale:1.0];
    }
}

- (ScaleRepresentation) getChartType {
    if (self.currentScale)
        return self.currentScale.type;
    return srLine;
}

- (void) applyPropertiesToScale {
    NSDictionary *currentProps = [self.visualProps valueForKey:kPropertySectionGeneral];
    CSAbstractScaleRepresentation *r = self.currentScale.representation;
    r.lineWidth = [currentProps valueForKey:kPropertyKeyWidth];
    r.stickWidth = [currentProps valueForKey:kPropertyKeyElementWidth];
    r.mainColor = [currentProps valueForKey:kPropertyKeyColor];
    r.monochrome = [[currentProps valueForKey:kPropertyKeyMonochrome] boolValue];
    if ([self.currentScale isKindOfClass:[CSMultiScale class]])
        [((CSMultiScale *)self.currentScale) applyAttributes:[currentProps valueForKey:kPropertyKeySectionSubData]];
    currentProps = [self.visualProps valueForKey:kPropertySectionVGrid];
    self.currentScale.verticalGridWidth = [[currentProps valueForKey:kPropertyKeyWidth] floatValue];
    self.currentScale.stripeColor = [currentProps valueForKey:kPropertyKeyFillColor];
    self.currentScale.gridColor = [currentProps valueForKey:kPropertyKeyStrokeColor];
    currentProps = [self.visualProps valueForKey:kPropertySectionHGrid];
    UIColor *c = [currentProps valueForKey:kPropertyKeyColor];
    [c getRed:&horGridR green:&horGridG blue:&horGridB alpha:&horGridA];
    horGridWidth = [[currentProps valueForKey:kPropertyKeyWidth] floatValue];
    currentProps = [self.visualProps valueForKey:kPropertySectionAxes];
    axisLineWidth = [[currentProps valueForKey:kPropertyKeyWidth] floatValue];
}

- (void) setGraphProperties:(NSDictionary *)props {
    for (NSString *s in allTheSections) {
        NSDictionary *newProps = [props valueForKey:s];
        NSMutableDictionary *currentProps = [self.visualProps valueForKey:s];
        if (newProps == nil) continue;
        for (NSString *k in allTheProperties)
            if ([currentProps valueForKey:k]) { // relevant property
                id nv = [newProps valueForKey:k];
                if (nv)
                    [currentProps setValue:nv forKey:k];
            }
    }
    [self applyPropertiesToScale];
    [self setNeedsDisplay];
}

/*- (BOOL) feedNews:(int)newsIdx At:(NSDate *) time {
 
 CFAbsoluteTime t = [time timeIntervalSinceReferenceDate];
 
 }
 - (void) placeNews:(NSMutableArray *)news {
 //  §
 }*/

@end

