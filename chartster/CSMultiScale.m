//
//  CSMultiScale.m
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSMultiScale.h"
#import "CSScaleRepresentationMulti.h"


@interface CSMultiScale () {
    BOOL hasDesiredItemSize;

    NSUInteger zoneIndex0, zoneIndex1;
    BOOL priceTagsCalculated; // tags shown on long touch
}

@property (nonatomic, strong) NSMutableDictionary *unmergedPackets;
@property (nonatomic, strong) NSMutableDictionary *mergedPackets;
@property (nonatomic, strong) NSString *feededScaleId;
@property (nonatomic, strong) CSSubContainer *feededPacket;
@property (nonatomic, strong) NSString *mainScaleId;
@property (nonatomic, strong) CSSubContainer *mainC;

@end

@implementation CSMultiScale

#define MULTICHARTCOMPENSATIONTIME 1.5

@synthesize candleItemInterval, type = _type, badScale = _badScale;
@synthesize representation = _representation;
@synthesize gridColor = _gridColor, verticalGridWidth = _verticalGridWidth;

- (void) setType:(ScaleRepresentation) v {
    v = srMultiChartLine;
    if (self.representation) {
        if (v == _type) return;
    }
    _type = v;
    _representation = [[CSScaleRepresentationMulti alloc] initWithScaler:self];
}

- (id) initFor:(CandleInterval)theCandleItemInterval {
    self = [super initFor:theCandleItemInterval];
    if (self) {
        candleItemInterval = theCandleItemInterval;
        compensationTime = MULTICHARTCOMPENSATIONTIME;

        self.type = srMultiChartLine;
    }
    return self;
}

- (id) initAsExtensionOf:(CSScale *) singleScale NewScaleId:(NSString *) scaleId DecFormat:(NSString *)xdecimalledFormat {
    self = [super initFor:singleScale.candleItemInterval];
    if (self) {
        candleItemInterval = singleScale.candleItemInterval;
        compensationTime = MULTICHARTCOMPENSATIONTIME;

        CSSubContainer *c = [self beginFeedingSubScaleId:scaleId Main:YES DecFormat:xdecimalledFormat];
        [c appendSkeletonData:singleScale];
        [self feedingDone];
        self.type = srMultiChartLine;
    }
    return self;

}
//- (void) dealloc {
//    if (capacity) {
//        free(hlocs);
//        free(times);
//    }
//    if (packetCapacity) {
//        free(packetHLOCs);
//        free(packetTimes);
//    }
//    //    if (defZone)
//    //        CFRelease(defZone);
//}
//
- (void) setDesiredItemSize:(CGFloat)dis {
    hasDesiredItemSize = YES;
    desiredItemSize = dis;
}

- (CGFloat) getDesiredItemSize {
    return itemSize;
}

- (void)pan:(float) x {
    startIndex += x / itemSize;
}

- (BOOL) drawPresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen {
    if ((drawen > 0.0) && (drawen < 1.0)) {
        [self.representation drawPresentationInContext:context Drawen:drawen Addendum:(1.0 - drawen) * intFrame.size.height];
        int c = vRectCount * drawen;
        if (c > 0) {
            CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.1);
            CGContextAddRects(context, vRects, c);
            CGContextFillPath(context);
        }
        if (c < vRectCount) {
            float rest = (vRectCount * drawen) - c;
            if (rest > 0.0001) {
                CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.1*rest);
                CGContextAddRect(context, vRects[c]);
                CGContextFillPath(context);
            }
        }
        return YES;
    }
    return NO;
}

- (BOOL) drawParameter:(NSString *) param PresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen {
    if ((drawen > 0.0) && (drawen < 1.0)) {
        [((CSScaleRepresentationMulti *)self.representation) drawParameter:param PresentationInContext:context Drawen:drawen Addendum:(1.0 - drawen) * intFrame.size.height];
        int c = vRectCount * drawen;
        if (c > 0) {
            CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.1);
            CGContextAddRects(context, vRects, c);
            CGContextFillPath(context);
        }
        if (c < vRectCount) {
            float rest = (vRectCount * drawen) - c;
            if (rest > 0.0001) {
                CGContextSetRGBFillColor(context, 0.5, 0.5, 0.5, MAX(0.2, 0.2*rest));
                CGContextAddRect(context, vRects[c]);
                CGContextFillPath(context);
            }
        }
        return YES;
    } else {
        BOOL tmp;
        [self drawInContext:context NeedsOverlayDrawing:&tmp];
    }
    return NO;
}

- (CSSubContainer *) beginFeedingSubScaleId:(NSString *)scaleId Main:(BOOL)isMain DecFormat:(NSString *)xdecimalledFormat {
    CSSubContainer *prevc = nil;
    if (isMain) {
        prevc = self.mainC;
        if (prevc) { // feeding main chart! moving everything into the unmerged dict.
            [self.mergedPackets enumerateKeysAndObjectsUsingBlock:^(NSString *mkey, CSSubContainer *mc, BOOL *stop) {
                CSSubContainer *prevUM = [self.unmergedPackets valueForKey:mkey];
                if (prevUM)
                    [mc appendSkeletonData:prevUM];
                [self.unmergedPackets setValue:mc forKey:mkey];
            }];
            [self.mergedPackets removeAllObjects];
        }
        self.mainScaleId = scaleId;
    } else {
        if (self.unmergedPackets) {
            prevc = [self.unmergedPackets valueForKey:scaleId];
        } else
            self.unmergedPackets = [[NSMutableDictionary alloc] init];
        if (self.mergedPackets) {
            if (prevc == nil)
                prevc = [self.mergedPackets valueForKey:scaleId];
        } else
            self.mergedPackets = [[NSMutableDictionary alloc] init];
    }
    self.feededPacket = [[CSSubContainer alloc] initWithPacket:prevc Zone:defZone CI:candleItemInterval DecFormat:xdecimalledFormat];
    self.feededScaleId = scaleId;
    return self.feededPacket;
}

- (void) dropData {
    _badScale = YES;
    self.unmergedPackets = nil;
    self.mergedPackets = nil;
    self.feededPacket = nil;
    self.feededScaleId = nil;
    self.mainScaleId = nil;
    self.mainC = nil;
}

- (void) processUnmergedPackets {
    if (self.mainC == nil) return;
    NSMutableArray *containersMergedThisTime = [[NSMutableArray alloc] init];
    BOOL hasInsertions = NO;
    for (NSString *scaleId in self.unmergedPackets.allKeys) {
        CSSubContainer *subContainer = [self.unmergedPackets valueForKey:scaleId];
        if ([subContainer isEqual:self.mainC]) continue;
        NSUInteger ic = 0;
        MergeResult r = [self.mainC tryMerge:subContainer InsertionCount:&ic];
        if (r >= mrIdeal) {
            subContainer.lastScaleId = scaleId;
            if (ic > 0) {
                hasInsertions = YES;
            }
            [containersMergedThisTime addObject:subContainer];
        }
    }
    if (containersMergedThisTime.count > 0) {
        if (hasInsertions && (containersMergedThisTime.count > 1)) { // in case of one merged container insertions are already processed
            while (hasInsertions) {
                hasInsertions = NO;
                for (CSSubContainer *c in containersMergedThisTime) {
                    NSUInteger ic = 0;
                    [self.mainC tryMerge:c InsertionCount:&ic];
                    if (ic > 0)
                        hasInsertions = YES;
                }
            }
        }
        for (CSSubContainer *c in containersMergedThisTime) {
            [self.mergedPackets setValue:c forKey:c.lastScaleId];
            [self.unmergedPackets setValue:nil forKey:c.lastScaleId];
        }
    }
}

- (FeedingResult) feedingDone {
    FeedingResult fr = frBad;
    if ([self.feededScaleId isEqualToString:self.mainScaleId]) { // new data for main chart
        fr = frCompletelyNewData;
        if (self.mainC) {
            fr = [self.mainC appendSkeletonData:self.feededPacket];
        } else {
            self.mainC = self.feededPacket;
            [self.mergedPackets setValue:self.feededPacket forKey:self.mainScaleId];
        }
    } else { // sub chart data
        fr = frLastCandleUpdateOnly; // last candle was not updated but rescale may be needed
        CSSubContainer *prevPacket = [self.unmergedPackets valueForKey:self.feededScaleId];
        if (prevPacket)
            fr = [prevPacket appendSkeletonData:self.feededPacket];
        else {
            prevPacket = [self.mergedPackets valueForKey:self.feededScaleId];
            if (prevPacket) {
                fr = [prevPacket appendSkeletonData:self.feededPacket];
                if (fr == frLastCandleUpdateOnly) { // don't care about mergind
                    self.feededPacket = nil;
                    return fr;
                }
                [self.unmergedPackets setValue:prevPacket forKey:self.feededScaleId]; // moving packet from merged to unmerged
                [self.mergedPackets setValue:nil forKey:self.feededScaleId];
            } else
                [self.unmergedPackets setValue:self.feededPacket forKey:self.feededScaleId];
        }
    }
    [self processUnmergedPackets];
    self.feededPacket = nil;
    return fr;
}

- (float) yForPrice:(double) p {
    if (_badScale) return 0;
    return [self.representation yForPrice:p];
}

- (double) startIndexMod:(int) modFactor {
    return fmod(startIndex, modFactor);
}

- (double) lastClose {
    if (!_badScale) {
        HLOCV *x;
        CFAbsoluteTime *t;
        NSUInteger count = [self.mainC getData:&x Times:&t];
        if (count > 0)
            return x[count - 1].close;
    }
    return 0;
}

- (void) proceedToRepresentationLine {
    CSScaleRepresentationMulti *mr = (CSScaleRepresentationMulti *)self.representation;
    [mr setLineLengthForContainers:self.mergedPackets MainScale:self.mainScaleId ItemSize:itemSize RepresentationFrame:rFrame VolumeFrame:vFrame];
}

- (BOOL) hasData {
    return (self.mainC) && ([self.mainC getData:nil Times:nil]);
}

- (void) recalcViewInFrame:(CGRect) intFrame ClippingInset:(CGFloat) cInset {
    lastIntFrameWidth = intFrame.size.width - cInset / 2;
    CGFloat clippedWidth = lastIntFrameWidth;
    if (startIndex < -clippedWidth/itemSize + 1)
        startIndex = -clippedWidth/itemSize + 1;
    if (self.mainC == nil) {
        _badScale = YES;
        return;
    }
    HLOCV *hlocs;
    CFAbsoluteTime *time;
    NSUInteger count = [self.mainC getData:&hlocs Times:&time];
    if (startIndex > count - 2.0)
        startIndex = MAX(0, count - 2.0);
    if (count == 0) {
        _badScale = YES;
        return;
    }
    if (hasDesiredItemSize) {
        itemSize = desiredItemSize;
        hasDesiredItemSize = NO;
    }
    if (itemSize < 1.5)
        itemSize = 1.5;
    _badScale = NO;
    lastMaxVisibleItemCount = clippedWidth/itemSize;
    
    int si = startIndex;
    
    if (itemSize < 1.5)
        itemSize = 1.5;
    // visible count = intFrame.size.width/itemSize;
    //startIndex = count - intFrame.size.width/itemSize;
    double dlinelength = clippedWidth/itemSize;
    int lineLength = dlinelength;
    CGFloat rFrameX = /*intFrame.origin.x*/ - (startIndex - si)*itemSize;
    while (dlinelength - 1.0*lineLength - (rFrameX/itemSize) > 0.4)
        lineLength++;
    if (lineLength + si > count) lineLength = count - si;
    if ((lineLength <= 0)/* || (lineLength > count)*/) {
        
        //Now we use chart tension and let almost all scrolling
        
        _badScale = YES;
        return;
    }
    rFrame = CGRectMake(rFrameX, 0, intFrame.size.width - cInset - rFrameX, intFrame.size.height * PRICEFRAMEK - 1.5);
    vFrame = CGRectMake(rFrameX, intFrame.size.height * PRICEFRAMEK + (cInset/2) + AXISBODYMARGIN, intFrame.size.width - cInset, intFrame.size.height * VOLUMEFRAMEK - (cInset/2) - (AXISBODYMARGIN * 2));
    self.mainC.lastEffectiveStartIndex = si;
    if (si < 0) {
        float negMargin = - si * itemSize;
        rFrame.origin.x += negMargin;
        if (negMargin > rFrame.size.width) {
            rFrame.size.width = 10; // rFrame.origin.x is outside of drawing frame now anyway
        } else {
            rFrame.size.width -= negMargin;
        }
        if (lineLength > si)
            lineLength += si;
        else
            lineLength = 1;
        self.mainC.lastEffectiveStartIndex = 0;
    }
    self.mainC.lastEffectiveLineLength = lineLength;
    [self.mergedPackets setValue:self.mainC forKey:self.mainScaleId];
    [self proceedToRepresentationLine];
    _badScale = NO;
    int r = 0;
    vRectCount = 0;
    int m = self.bigPeriodMask;
    for (int i = 0; i < count; i++)
        if (hlocs[i].tag & m) {
            if (r & 1) {
                vRects[vRectCount].size.width = MIN(clippedWidth, (i - startIndex) * itemSize) - vRects[vRectCount].origin.x;
                if (i >= si) vRectCount++;
                if (vRectCount >= 100) break;
            } else {
                vRects[vRectCount].origin.x = (i - startIndex) * itemSize;
                if (vRects[vRectCount].origin.x > intFrame.size.width - 6) break;
            }
            r++;
        }
    if ((r & 1) && (vRectCount < 100)) {
        vRects[vRectCount].size.width = MIN(clippedWidth, (count - startIndex) * itemSize) - vRects[vRectCount].origin.x;
        vRectCount++;
    }
}

- (NSUInteger) mainCount {
    if (self.mainC == nil) {
        _badScale = YES;
        return 0;
    }
    HLOCV *hlocs;
    CFAbsoluteTime *time;
    return [self.mainC getData:&hlocs Times:&time];
}

- (float)tryPan:(float) x {
    if (x < 0) {
        if (startIndex < 0) {
            if (lastMaxVisibleItemCount <= 0)
                return -1; // broken visualization
            if (startIndex < - lastMaxVisibleItemCount + 1)
                return -1;
            float proportion = - 0.5*startIndex / (lastMaxVisibleItemCount - 1);
            // (0.5 - proportion) : ~ 0 near lastMaxVisibleItemCount - 1, ~ 0.5 near 0
            startIndex += (0.5 - proportion)*x / itemSize;
            return proportion;
        }
        startIndex += x / itemSize;
    } else {
        NSUInteger count = [self mainCount];
        if (startIndex > count - lastMaxVisibleItemCount + 1) {
            if (startIndex > count - 3)
                return -1;
            float proportion = 0.5 * (count - 3 - startIndex) / (lastMaxVisibleItemCount - 1);
            // proportion is ~0 around count - 3, ~ 0.5 around lastMaxVisibleItemCount - 1
            startIndex += proportion*x / itemSize;
            return proportion;
        }
        startIndex += x / itemSize;
    }
    return 1.0;
}

- (BOOL) drawMarkZoneOn:(CGContextRef)context {
    if (startIndex > zoneIndex1) return NO;
/*    if (startIndex + lastMaxVisibleItemCount < zoneIndex0) return NO;
    CGFloat x0 = 0;
    BOOL x0Vis = NO;
    BOOL x1Vis = NO;
    CGFloat x1;
    if (startIndex < zoneIndex0) {
        x0 = itemSize * (zoneIndex0 - startIndex);
        x0Vis = YES;
    }
    if (lastMaxVisibleItemCount + startIndex > zoneIndex1) {
        x1 = itemSize * (zoneIndex1 - startIndex);
        x1Vis = YES;
    } else
        x1 = itemSize * lastMaxVisibleItemCount;
    if (x0Vis) {
        CGContextMoveToPoint(context, x0 - 0.5, 0);
        CGContextAddLineToPoint(context, x0 - 1, [self.representation yForPrice:hlocs[zoneIndex0].close]);
    }
    CGFloat y1 = [self.representation yForPrice:hlocs[zoneIndex1].close];
    self.markZoneBalloonPoint = CGPointMake(x1, y1);
    if (x1Vis) {
        CGContextMoveToPoint(context, x1 + 0.5, 0);
        CGContextAddLineToPoint(context, x1 + 1, y1);
    }
    if (x1Vis || x0Vis) {
        CGContextSetRGBStrokeColor(context, 1.0, 0, 0, 1.0);
        CGContextStrokePath(context);
    }
    
    CGContextAddRect(context, CGRectMake(x0, 0, x1 - x0, vRects[0].size.height));*/
    return YES;
}

- (BOOL) drawHintOn:(CGContextRef)context Stage:(int)stage {
    if (stage == 0) return YES;
    if (startIndex > zoneIndex0) return NO;
    if (startIndex + lastMaxVisibleItemCount < zoneIndex0) return NO;
    CGFloat x = 0;
    x = itemSize * (zoneIndex0 - startIndex);
    HLOCV *hlocs = nil;
    [self.mainC getData:&hlocs Times:nil];
    CGFloat y = [self.representation yForPrice:hlocs[zoneIndex0].close];

    self.markZoneBalloonPointL = self.markZoneBalloonPointR = CGPointMake(x, y);
    if (stage) {
        CGContextSelectFont(context, "Arial", 8, kCGEncodingMacRoman);
        CGContextSetTextMatrix(context, CGAffineTransformMake(1.0,0.0, 0.0, -1.0, 0.0, 0.0));
        CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);

        BOOL firstDraw = !priceTagsCalculated;
        UIFont *f = nil;

        if (firstDraw) {
            f = [UIFont systemFontOfSize:8];
            priceTagsCalculated = YES;
        }
        CSScaleRepresentationMulti *mr = (CSScaleRepresentationMulti *)self.representation;
        [mr drawHint:[NSString stringWithFormat:self.mainC.decimalledFormat, hlocs[zoneIndex0].close] On:context At:self.markZoneBalloonPointL Font:f ScaleKey:self.mainScaleId];

        [self.mergedPackets enumerateKeysAndObjectsUsingBlock:^(NSString *mkey, CSSubContainer *mc, BOOL *stop) {
            HLOCV *sh = nil;
            CFAbsoluteTime *st = nil;
            NSUInteger scount = [mc getData:&sh Times:&st];
            CSScaleRepresentationSubLine *rep = [((CSScaleRepresentationMulti *)self.representation).subs valueForKey:mkey];
            if ((zoneIndex0 < scount) && (rep)) {
                [mr drawHint:[NSString stringWithFormat:mc.decimalledFormat, sh[zoneIndex0].close] On:context At:CGPointMake(x, [rep yForPrice:sh[zoneIndex0].close]) Font:f ScaleKey:mkey];
            }
        }];
    } else {
        /*CGContextMoveToPoint(context, x, 0);
        CGContextAddLineToPoint(context, x, y);*/
    }
    return YES;
    
}

- (void) drawOverlaysInContext:(CGContextRef)context {
    if (self.mark == mmHint)
        [self drawHintOn:context Stage:1];
}

- (void) drawInContext:(CGContextRef)context NeedsOverlayDrawing:(BOOL *)needsOverlayDrawing {
    if (hasScaleTension)
        [self proceedToRepresentationLine];
/*    if (self.hasMarkZone && [self drawMarkZoneOn:context]) {
        CGContextSetRGBFillColor(context, 0.0, 0.0, 0.1, 0.1);
        CGContextFillPath(context);
    }*/
    if (self.mark == mmMarkZone)
        *needsOverlayDrawing = NO;//[self drawMarkZoneOn:context Stage:0];
    else
        *needsOverlayDrawing = (self.mark == mmHint) && [self drawHintOn:context Stage:0];
    [self.representation drawInContext:context];
    [self drawStripesInContext:context];
}

- (NSString *)getTimeForX:(int) x ItemUsed:(NSUInteger *)itemIndex MainTime:(CFAbsoluteTime *)times {
    NSUInteger count = [self mainCount];
    if (_badScale) {
        *itemIndex = 0;
        return @"";
    }
    *itemIndex = MAX(0, startIndex + (x / itemSize));
    if (*itemIndex >= count)
        *itemIndex = count - 1;
    NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[*itemIndex]];//startIndex + (x / itemSize)];
    return [self.axisFormatterTime stringFromDate:d];
}

- (double) markZoneBetween:(CGFloat) x0 And:(CGFloat) x1  HLOC0:(out HLOCV *) hloc0 HLOC1:(out HLOCV *) hloc1 Dates:(out NSArray **) dates {
    return -1;
#ifdef mywarns
   #warning unimplemented
#endif
}

- (void) hintOn:(CGFloat)x HLOC:(out NSArray **)data Date:(out NSString **)date {
    self.mark = mmHint;
    HLOCV *hlocs = nil;
    CFAbsoluteTime *times = nil;
    NSUInteger count = [self.mainC getData:&hlocs Times:&times];
    if (count == 0) return;
    zoneIndex0 = startIndex + (x / itemSize);
    if (zoneIndex0 >= count)
        zoneIndex0 = count - 1;

    *data = @[@"..."];
    priceTagsCalculated = NO;
    
    NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[zoneIndex0]];
    switch (candleItemInterval) {
        case ciMinute:
        case ci10Minutes:
        case ciHour:
            *date = [NSString stringWithFormat:@"%@ %@", [self.axisFormatterDateBig stringFromDate:d], [self.axisFormatterTime stringFromDate:d]];
            return;
        default:
            *date = [self.axisFormatterDateBig stringFromDate:d];
            return;
    }
}

- (int) hlocs:(HLOCV *)hlocs OfMask:(int) mask Since:(NSUInteger) item0 Till:(NSUInteger) countLimit {
    int timesCount = 0;
    for (int item = item0; item < countLimit; item++)
        if (hlocs[item].tag & mask)
            timesCount++;
    return timesCount;
}

- (void) enumerateVerticalGridUsingBlock:(GridEnum)block ForMinimalWidth:(float)minW IntFrame:(CGRect)intFrame {
    NSUInteger lastUsedItemNo;
    //    if (intFrame.origin.x < -itemSize)
    //        intFrame.origin.x = fmod(intFrame.origin.x, itemSize);
    NSString *time;
    if (self.mainC == nil) {
        _badScale = YES;
        return;
    }
    HLOCV *xhlocs = nil;
    CFAbsoluteTime *xt = nil;
    NSUInteger count = [self.mainC getData:&xhlocs Times:&xt];
    [self getTimeForX:intFrame.origin.x ItemUsed:&lastUsedItemNo MainTime:xt];
    if (_badScale) return;
    NSString *date;// = [self getDateForItemUsed:lastUsedItemNo];
    int mask1;
    switch (candleItemInterval) {
        case ciMonth:
            mask1 = hMonthChange;
            break;
        case ciWeek:
            mask1 = hWeekChange;
            break;
        case ciDay:
            mask1 = hDayChange;
            break;
        case ciHour:
            mask1 = hHOURChange;
            break;
        case ci10Minutes:
            mask1 = h10MinChange;
            break;
        case ciMinute:
            mask1 = hNormal;
            break;
        default:
            mask1 = hYearChange;
            break;
    }
    int maxTimesCount = (intFrame.size.width / minW) + 1;
    int countLimit = MIN(lastUsedItemNo + (intFrame.size.width / itemSize), count);
    int lastGoodMask = 0;
    NSUInteger lastGoodCount = 0;
    for (; mask1 < hYearChange; mask1 <<= 1) {
        NSUInteger icnt = [self hlocs:xhlocs OfMask:mask1 Since:lastUsedItemNo Till:countLimit];
        if (icnt) {
            if (icnt > lastGoodCount) {
                lastGoodCount = icnt;
                lastGoodMask = mask1;
            }
            if (icnt > maxTimesCount)
                continue;
        } else
            if (lastGoodCount > 0)
                mask1 = lastGoodMask;
        break;
    }
    NSDateFormatter *m1f = self.axisFormatterTime;
    NSDateFormatter *m2f = self.axisFormatterDateBig;
    int mask2 = hYearChange;
    switch (mask1) {
        case hYearChange:
            mask1 = 0;
            m1f = nil;
            m2f = self.axisFormatterYear;
            break;
        case hMonthChange:
        case hWeekChange:
            mask2 = hYearChange;
            m1f = self.axisFormatterDate;
            if (mask1 == hMonthChange)
                [m1f setDateFormat:@"MMM"];
            else
                [m1f setDateFormat:@"MM dd"];
            m2f = self.axisFormatterYear;
            break;
        case hDayChange:
            mask2 = hMonthChange;
            m1f = self.axisFormatterDay;
            m2f = self.axisFormatterDate;
            [m2f setDateFormat:@"MMM yyyy"];
            break;
        default:
            mask2 = hDayChange;
            break;
    }
    if (m1f) {
        int idx = 0;
        int hadDate = NO;
        for (int item = lastUsedItemNo; item < count; item++) {
            if (xhlocs[item].tag & (mask1 | mask2)) {
                float labelX = ((item - startIndex)*itemSize);
                if (labelX < 0) continue;
                if (labelX > intFrame.size.width) break;
                NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:xt[item]];//startIndex + (x / itemSize)];
                if (!hadDate || ((xhlocs[item].tag & mask2) != 0)) {
                    date = [m2f stringFromDate:d];
                    hadDate = YES;
                } else
                    date = nil;
                if (mask1 && ((xhlocs[item].tag & mask1) != 0)) {
                    time = [m1f stringFromDate:d];
                } else
                    time = nil;
                block(idx++, time, date, intFrame.origin.x + labelX);
            }
        }
        if (!hadDate) {
            NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:xt[lastUsedItemNo]];
            block(idx, nil, [m2f stringFromDate:d], intFrame.origin.x);
        }
    } else { // 'year' labels only
        int idx = 0;
        for (int item = lastUsedItemNo; item < count; item++) {
            if ((xhlocs[item].tag & mask2) || (item == lastUsedItemNo)) {
                float labelX = ((item - startIndex)*itemSize);
                if (labelX > intFrame.size.width) break;
                NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:xt[item]];//startIndex + (x / itemSize)];
                date = [m2f stringFromDate:d];
                block(idx++, nil, date, intFrame.origin.x + labelX);
            }
        }
    }
}

#ifdef mywarns
#warning ^^^ almost copypasted from CSScale
#endif

- (double) priceAtY:(float) y {
    if (_badScale) return 0;
    return [self.representation priceAtY:y];
}

- (void) applyAttributes:(NSArray *) attrs {
    ((CSScaleRepresentationMulti *)self.representation).subAttributes = attrs;
}



@end
