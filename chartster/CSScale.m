//
//  CSScale.m
//  chartster
//
//  Created by Danila Parhomenko on 4/12/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSScale.h"
#import <Accelerate/Accelerate.h>
#import "CSScaleRepresentationLine.h"
#import "CSCandleRepresentationCandles.h"
#import "CSScaleRepresentationBars.h"

#ifdef mywarns
#warning Well, maybe i should call presentationEnded not after each recalc. And run smooth UIView animation for time labels
#endif

@interface CSScale () {
    NSUInteger packetCapacity;
    NSUInteger packetCount;
    HLOCV *packetHLOCs;
    CFAbsoluteTime *packetTimes;
    
    BOOL hasDesiredItemSize;
    
    CFAbsoluteTime zoneT0, zoneT1;
    NSUInteger zoneIndex0, zoneIndex1;

    //last time labels' enumeration properties
    //NSUInteger lastUsedItemNo;

    int mask0; // corresponding time period tag mask
    
    // cached recalc values
    int effectiveSI, lastLineLength;
    
    // tag properties
    CGFloat leftTagWidth, rightTagWidth;
    CGImageRef tagLeft0, tagLeftx, tagLeft1;
    CGImageRef tagRight0, tagRightx, tagRight1;
}

@property (nonatomic, strong) UIImage *hatchImage;
//@property (nonatomic, strong) UIImage *point;
@property (nonatomic, strong) NSString *leftPrice;
@property (nonatomic, strong) NSString *rightPrice;

@end

@implementation CSScale

@synthesize candleItemInterval, type = _type, badScale = _badScale;
@synthesize representation = _representation;

- (void) setType:(ScaleRepresentation) v {
    if (self.representation) {
        if (v == _type) return;
    }
    _type = v;
    if (v == srCandles)
        _representation = [[CSCandleRepresentationCandles alloc] initWithScaler:self];
    else {
        if (v == srBars)
            _representation = [[CSScaleRepresentationBars alloc] initWithScaler:self];
        else
            _representation = [[CSScaleRepresentationLine alloc] initWithScaler:self];
    }
}

- (id) initFor:(CandleInterval)theCandleItemInterval As:(ScaleRepresentation) t {
    self = [super initFor:theCandleItemInterval];
    if (self) {
        candleItemInterval = theCandleItemInterval;
        switch (theCandleItemInterval) {
            case ciMinute:
                currentCandleInterval = intervalLengths[0];
                mask0 = hNormal;
                break;
            case ciHour:
                currentCandleInterval = intervalLengths[3];
                mask0 = hHOURChange;
                break;
            case ciDay:
                currentCandleInterval = intervalLengths[5];
                mask0 = hDayChange;
                break;
            case ciWeek:
                currentCandleInterval = intervalLengths[6];
                mask0 = hWeekChange;
                break;
            case ciMonth:
                currentCandleInterval = intervalLengths[7];
                mask0 = hMonthChange;
                break;
            case ci10Minutes:
                currentCandleInterval = intervalLengths[1];
                mask0 = h10MinChange;
                break;
            default:
                currentCandleInterval = intervalLengths[8];
                mask0 = hYearChange;
                break;
        }
        maxCurrentCandleInterval = currentCandleInterval + 0.1;
        
        self.type = t;
        self.hatchImage = [UIImage imageNamed:@"hatch8.png"];
        //self.point = [UIImage imageNamed:@"onPoint.png"];
        UIImage *i = [UIImage imageNamed:@"cursor_blue_left2@x.png"];

        // tag image parts:
        //   /|      |\
        //  ( | 10.2 | >--o
        //   \|      |/
        //
        // tag1 tagx  tag0
        tagLeft1 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(0, 0, TAGMARGIN*2, (TAGHEIGHT)*2));
        tagLeftx = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(TAGMARGIN*2, 0, 1, TAGHEIGHT*2));
        tagLeft0 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(TAGMARGIN*2 + 1, 0, (TAGWIDTH - TAGMARGIN)*2 - 1, TAGHEIGHT*2));
        i = [UIImage imageNamed:@"cursor_blue_right2@x.png"];

        // tag image parts:
        //     /|      |\
        // o--< | 88.9 | )
        //     \|      |/
        //
        // tag0  tagx  tag1
        tagRight0 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(0, 0, (TAGWIDTH - TAGMARGIN)*2, (TAGHEIGHT)*2));
        tagRightx = CGImageCreateWithImageInRect(i.CGImage, CGRectMake((TAGWIDTH - TAGMARGIN)*2 - 1, 0, 1, TAGHEIGHT*2));
        tagRight1 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake((TAGWIDTH - TAGMARGIN)*2, 0, TAGMARGIN*2, TAGHEIGHT*2));
    }
    return self;
}

- (void) dealloc {
    if (capacity) {
        free(hlocs);
        free(times);
    }
    if (packetCapacity) {
        free(packetHLOCs);
        free(packetTimes);
    }
    if (tagLeft0) CFRelease(tagLeft0);
    if (tagLeftx) CFRelease(tagLeftx);
    if (tagLeft1) CFRelease(tagLeft1);
    if (tagRight0) CFRelease(tagRight0);
    if (tagRightx) CFRelease(tagRightx);
    if (tagRight1) CFRelease(tagRight1);
//    if (defZone)
//        CFRelease(defZone);
}

- (NSUInteger) mainCount {
    return count;
}

- (void) setDesiredItemSize:(CGFloat)dis {
    hasDesiredItemSize = YES;
    desiredItemSize = dis;
}

- (CGFloat) getDesiredItemSize {
    return itemSize;
}

- (void) proceedToRepresentationLine:(int) lineLength HLOCS:(HLOCV *)xhlocs {
    [self.representation setLineLength:lineLength HLOCVs:xhlocs ItemSize:itemSize RepresentationFrame:rFrame VolumeFrame:vFrame];
}

- (BOOL) hasData {
    return count > 0;
}

- (void) recalcViewInFrame:(CGRect) intFrame ClippingInset:(CGFloat) cInset {
    lastIntFrameWidth = intFrame.size.width - cInset / 2;
    CGFloat clippedWidth = lastIntFrameWidth;
    if (startIndex < -clippedWidth/itemSize + 1)
        startIndex = -clippedWidth/itemSize + 1;
    if (startIndex > count - 2.0)
        startIndex = MAX(0, count - 2.0);
    if (count == 0) {
        _badScale = YES;
        return;
    }
    if (hasDesiredItemSize) {
        itemSize = desiredItemSize;
        hasDesiredItemSize = NO;
    }
    if (itemSize < 1.5)
        itemSize = 1.5;
    _badScale = NO;
    lastMaxVisibleItemCount = clippedWidth/itemSize;

    int si = startIndex;
    
    if (itemSize < 1.5)
        itemSize = 1.5;
        // visible count = intFrame.size.width/itemSize;
        //startIndex = count - intFrame.size.width/itemSize;
    double dlinelength = clippedWidth/itemSize;
    int lineLength = dlinelength + 3;
    CGFloat rFrameX = /*intFrame.origin.x*/ - (startIndex - si)*itemSize;
    /*while (dlinelength - 1.0*lineLength - (rFrameX/itemSize) > 0.4)
        lineLength++;*/
    if (lineLength + si > count) lineLength = count - si;
    if ((lineLength <= 0)/* || (lineLength > count)*/) {
        
        //Now we use chart tension and let almost all scrolling

        _badScale = YES;
        return;
    }
    rFrame = CGRectMake(rFrameX, 0, intFrame.size.width - cInset - rFrameX, intFrame.size.height * PRICEFRAMEK - 1.5);
    vFrame = CGRectMake(rFrameX, intFrame.size.height * PRICEFRAMEK + (cInset/2) + AXISBODYMARGIN, intFrame.size.width - cInset, intFrame.size.height * VOLUMEFRAMEK - (cInset/2) - (AXISBODYMARGIN * 2));
    effectiveSI = si;
    if (si < 0) {
        float negMargin = - si * itemSize;
        rFrame.origin.x += negMargin;
        if (negMargin > rFrame.size.width) {
            rFrame.size.width = 10; // rFrame.origin.x is outside of drawing frame now anyway
        } else {
            rFrame.size.width -= negMargin;
        }
        if (lineLength > si)
            lineLength += si;
        else
            lineLength = 1;
        effectiveSI = 0;
    }
    //__block int caseVar = hadPreviousScale?(hasScaleTension?2:1):0;
    lastLineLength = lineLength;
    [self proceedToRepresentationLine:lineLength HLOCS:&(hlocs[effectiveSI])];
    _badScale = NO;
    int r = 0;
    vRectCount = 0;
    int m = self.bigPeriodMask;
    for (int i = 0; i < count; i++)
        if (hlocs[i].tag & m) {
            if (r & 1) {
                vRects[vRectCount].size.width = MIN(clippedWidth, (i - startIndex) * itemSize) - vRects[vRectCount].origin.x;
                if (i >= si) vRectCount++;
                if (vRectCount >= 100) break;
            } else {
                vRects[vRectCount].origin.x = (i - startIndex) * itemSize;
                if (vRects[vRectCount].origin.x > intFrame.size.width - 6) break;
            }
            r++;
        }
    if ((r & 1) && (vRectCount < 100)) {
        vRects[vRectCount].size.width = MIN(clippedWidth, (count - startIndex) * itemSize) - vRects[vRectCount].origin.x;
        vRectCount++;
    }
}

- (void)pan:(float) x {
    startIndex += x / itemSize;
}

- (float)tryPan:(float) x {
    if (x < 0) {
        if (startIndex < 0) {
            if (lastMaxVisibleItemCount <= 0)
                return -1; // broken visualization
            if (startIndex < - lastMaxVisibleItemCount + 1)
                return -1;
            float proportion = - 0.5*startIndex / (lastMaxVisibleItemCount - 1);
            // (0.5 - proportion) : ~ 0 near lastMaxVisibleItemCount - 1, ~ 0.5 near 0
            startIndex += (0.5 - proportion)*x / itemSize;
            return proportion;
        }
        startIndex += x / itemSize;
    } else {
        if (startIndex > count - lastMaxVisibleItemCount + 1) {
            if (startIndex > count - 3)
                return -1;
            float proportion = 0.5 * (count - 3 - startIndex) / (lastMaxVisibleItemCount - 1);
            // proportion is ~0 around count - 3, ~ 0.5 around lastMaxVisibleItemCount - 1
            startIndex += proportion*x / itemSize;
            return proportion;
        }
        startIndex += x / itemSize;
    }
    return 1.0;
}

- (BOOL) drawPresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen {
    if ((drawen > 0.0) && (drawen < 1.0)) {
        [self.representation drawPresentationInContext:context Drawen:drawen Addendum:(1.0 - drawen) * intFrame.size.height];
        int c = vRectCount * drawen;
        if (c > 0) {
            CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.1);
            CGContextAddRects(context, vRects, c);
            CGContextFillPath(context);
        }
        if (c < vRectCount) {
            float rest = (vRectCount * drawen) - c;
            if (rest > 0.0001) {
                CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.1*rest);
                CGContextAddRect(context, vRects[c]);
                CGContextFillPath(context);
            }
        }
        return YES;
    }
    return NO;
}

- (void) drawLeftTagOn:(CGContextRef) context P:(CGPoint)p Text:(NSString *)price Extent:(CGFloat)ext {
    p.x += 5 - ext; // to match center of dot
    p.y -= 0.5 + (TAGHEIGHT/2);
    CGContextDrawImage(context, CGRectMake(p.x + 1, p.y, TAGMARGIN, TAGHEIGHT), tagLeft1);
    CGContextDrawImage(context, CGRectMake(p.x + TAGMARGIN + 0.5, p.y, ext - TAGWIDTH, TAGHEIGHT), tagLeftx);
    CGContextDrawImage(context, CGRectMake(p.x + ext + TAGMARGIN - TAGWIDTH, p.y, TAGWIDTH - TAGMARGIN, TAGHEIGHT), tagLeft0);
    CGContextShowTextAtPoint(context, p.x + TAGMARGIN, p.y + 3 + 0.5 + (TAGHEIGHT/2), [price cStringUsingEncoding:NSMacOSRomanStringEncoding], price.length);
}

- (void) drawRightTagOn:(CGContextRef) context P:(CGPoint)p Text:(NSString *)price Extent:(CGFloat)ext {
    p.x -= 6; // to match center of dot
    p.y -= 0.5 + (TAGHEIGHT/2);
    CGContextDrawImage(context, CGRectMake(p.x, p.y, TAGWIDTH - TAGMARGIN, TAGHEIGHT), tagRight0);
    CGContextDrawImage(context, CGRectMake(p.x + TAGWIDTH - TAGMARGIN - 0.5, p.y, ext - TAGWIDTH, TAGHEIGHT), tagRightx);
    CGContextDrawImage(context, CGRectMake(p.x + ext - TAGMARGIN - 1.0, p.y, TAGMARGIN, TAGHEIGHT), tagRight1);
    CGContextShowTextAtPoint(context, p.x + TAGWIDTH - TAGMARGIN, p.y + 3 + 0.5 + (TAGHEIGHT/2), [price cStringUsingEncoding:NSMacOSRomanStringEncoding], price.length);
}

- (BOOL) drawMarkZoneOn:(CGContextRef)context Stage:(int) stage {
    if (startIndex > zoneIndex1) return NO;
    if (startIndex + lastMaxVisibleItemCount < zoneIndex0) return NO;
    CGFloat x0 = itemSize * (zoneIndex0 - startIndex);
    BOOL x0Vis = NO;
    BOOL x1Vis = NO;
    CGFloat x1;
    if (startIndex < zoneIndex0) {
        x0Vis = YES;
    }
    if (lastMaxVisibleItemCount + startIndex > zoneIndex1) {
        x1 = itemSize * (zoneIndex1 - startIndex);
        x1Vis = YES;
    } else
        x1 = itemSize * lastMaxVisibleItemCount;
    CGFloat y0 = 0;
    if (x0Vis)
        y0 = [self.representation yForPrice:hlocs[zoneIndex0].close];
    CGFloat y1 = [self.representation yForPrice:hlocs[zoneIndex1].close];
    if (stage) {
        CGContextSelectFont(context, "Arial", 8, kCGEncodingMacRoman);
        CGContextSetTextMatrix(context, CGAffineTransformMake(1.0,0.0, 0.0, -1.0, 0.0, 0.0));
        CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
        if (x0Vis) {
            if (self.leftPrice == nil) {
                self.leftPrice = [NSString stringWithFormat:self.decimalledFormat, hlocs[zoneIndex0].close];
                UIFont *f = [UIFont systemFontOfSize:8];
                leftTagWidth = [self.leftPrice sizeWithFont:f].width + TAGWIDTH;
            }
            NSString *price = self.leftPrice;
            if (x0 > leftTagWidth - 2)
                [self drawLeftTagOn:context P:CGPointMake(x0, y0) Text:price Extent:leftTagWidth];
            else
                [self drawRightTagOn:context P:CGPointMake(x0, y0) Text:price Extent:leftTagWidth];
        }
        if (x1Vis) {
            if (self.rightPrice == nil) {
                self.rightPrice = [NSString stringWithFormat:self.decimalledFormat, hlocs[zoneIndex1].close];
                UIFont *f = [UIFont systemFontOfSize:8];
                rightTagWidth = [self.rightPrice sizeWithFont:f].width + TAGWIDTH;
            }
            NSString *price = self.rightPrice;
            if (x1 > lastMaxVisibleItemCount*itemSize - rightTagWidth + 2)
                [self drawLeftTagOn:context P:CGPointMake(x1, y1) Text:price Extent:rightTagWidth];
            else
                [self drawRightTagOn:context P:CGPointMake(x1, y1) Text:price Extent:rightTagWidth];
        }
        CGContextFillPath(context);
    } else {
        self.markZoneBalloonPointL = CGPointMake(x0, y0);
        self.markZoneBalloonPointR = CGPointMake(x1, y1);
        if (x0Vis) {
            CGContextMoveToPoint(context, x0 - 0.5, 0);
            CGContextAddLineToPoint(context, x0 - 0.5, y0);
        }
        if (x1Vis) {
            CGContextMoveToPoint(context, x1 + 0.5, 0);
            CGContextAddLineToPoint(context, x1 + 0.5, y1);
        }
        if (x1Vis || x0Vis) {
            CGContextSetLineWidth(context, 0.5);
            CGContextSetRGBStrokeColor(context, 0.0, 0, 0, 1.0);
            CGContextStrokePath(context);
        }
        if ((self.type == srLine) || (self.type == srMultiChartLine)) {
            CGContextSaveGState(context);
            CGContextClipToRect(context, CGRectMake(x0, 0, x1 - x0, vRects[0].size.height));
            [((CSScaleRepresentationLine *)self.representation) addChartSkyLineTo:context];
            CGContextClosePath(context);
            CGContextClip(context);
            CGContextDrawTiledImage(context, CGRectMake(0, 0, 8, 8), self.hatchImage.CGImage);
            CGContextRestoreGState(context);
        } else {
            CGContextAddRect(context, CGRectMake(x0, 0, x1 - x0, vRects[0].size.height));
            CGContextSetRGBFillColor(context, 0.0, 0.0, 0.1, 0.1);
            CGContextFillPath(context);
        }
    }
    return YES;
}

- (BOOL) drawHintOn:(CGContextRef)context Stage:(int)stage {
    if (startIndex > zoneIndex0) return NO;
    if (startIndex + lastMaxVisibleItemCount < zoneIndex0) return NO;
    CGFloat x = 0;
    x = itemSize * (zoneIndex0 - startIndex);
    CGFloat y = [self.representation yForPrice:hlocs[zoneIndex0].close];
    self.markZoneBalloonPointL = self.markZoneBalloonPointR = CGPointMake(x, y);
    if (stage) {
        CGContextSelectFont(context, "Arial", 8, kCGEncodingMacRoman);
        CGContextSetTextMatrix(context, CGAffineTransformMake(1.0,0.0, 0.0, -1.0, 0.0, 0.0));
        CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
        if (self.leftPrice == nil) {
            self.leftPrice = [NSString stringWithFormat:self.decimalledFormat, hlocs[zoneIndex0].close];
            UIFont *f = [UIFont systemFontOfSize:8];
            leftTagWidth = [self.leftPrice sizeWithFont:f].width + TAGWIDTH;
        }
        NSString *price = self.leftPrice;
        if (x > leftTagWidth - 2)
            [self drawLeftTagOn:context P:CGPointMake(x, y) Text:price Extent:leftTagWidth];
        else
            [self drawRightTagOn:context P:CGPointMake(x, y) Text:price Extent:leftTagWidth];
        CGContextFillPath(context);
    } else {
        CGContextMoveToPoint(context, x, 0);
        CGContextAddLineToPoint(context, x, y);
    }
    return YES;

}

- (void) drawInContext:(CGContextRef)context NeedsOverlayDrawing:(BOOL *)needsOverlayDrawing {
    if (hasScaleTension)
        [self proceedToRepresentationLine:lastLineLength HLOCS:&(hlocs[effectiveSI])];
    if (self.mark == mmMarkZone)
        *needsOverlayDrawing = [self drawMarkZoneOn:context Stage:0];
    else
        *needsOverlayDrawing = (self.mark == mmHint) && [self drawHintOn:context Stage:0];
    [self.representation drawInContext:context];
    [self drawStripesInContext:context];
}

- (void) drawOverlaysInContext:(CGContextRef)context {
    if (self.mark == mmMarkZone)
        [self drawMarkZoneOn:context Stage:1];
    else
        [self drawHintOn:context Stage:1];
}

- (NSString *)getTimeForX:(int) x ItemUsed:(NSUInteger *)itemIndex {
    if (count == 0)
        _badScale = YES;
    if (_badScale) {
        *itemIndex = 0;
        return @"";
    }
    *itemIndex = MAX(0, startIndex + (x / itemSize));
    if (*itemIndex >= count)
        *itemIndex = count - 1;
    NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[*itemIndex]];//startIndex + (x / itemSize)];
    return [self.axisFormatterTime stringFromDate:d];
}

- (double) markZoneBetween:(CGFloat) x0 And:(CGFloat) x1 HLOC0:(out HLOCV *) hloc0 HLOC1:(out HLOCV *) hloc1 Dates:(out NSArray **) dates {
    self.mark = mmMarkZone;
    [self getTimeForX:x0 ItemUsed:&zoneIndex0];
    [self getTimeForX:x1 ItemUsed:&zoneIndex1];
    self.leftPrice = nil;
    self.rightPrice = nil;
    zoneT0 = times[zoneIndex0];
    zoneT1 = times[zoneIndex1];
    NSDate *d0 = [NSDate dateWithTimeIntervalSinceReferenceDate:zoneT0];
    NSDate *d1 = [NSDate dateWithTimeIntervalSinceReferenceDate:zoneT1];
    switch (candleItemInterval) {
        case ciMinute:
        case ci10Minutes:
        case ciHour:
            if (zoneT1 - zoneT0 > 60*60*24)
                *dates = @[[NSString stringWithFormat:@"%@ %@", [self.axisFormatterDateBig stringFromDate:d0], [self.axisFormatterTime stringFromDate:d0]], [NSString stringWithFormat:@"%@ %@", [self.axisFormatterDateBig stringFromDate:d1], [self.axisFormatterTime stringFromDate:d1]]];
            else
                *dates = @[[self.axisFormatterTime stringFromDate:d0], [self.axisFormatterTime stringFromDate:d1]];
            break;
        default:
            *dates = @[[self.axisFormatterDateBig stringFromDate:d0], [self.axisFormatterDateBig stringFromDate:d1]];
            break;
    }
    *hloc0 = hlocs[zoneIndex0];
    *hloc1 = hlocs[zoneIndex1];
    return (*hloc1).close - (*hloc0).close;
}

- (void) hintOn:(CGFloat) x HLOC:(out NSArray **) data Date:(out NSString **) date {
    self.mark = mmHint;
    [self getTimeForX:x ItemUsed:&zoneIndex0];
    self.leftPrice = nil;
    zoneT0 = times[zoneIndex0];
    *data = @[[NSString stringWithFormat:self.decimalledFormat, hlocs[zoneIndex0].close]];
    NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[zoneIndex0]];
    switch (candleItemInterval) {
        case ciMinute:
        case ci10Minutes:
        case ciHour:
            *date = [NSString stringWithFormat:@"%@ %@", [self.axisFormatterDateBig stringFromDate:d], [self.axisFormatterTime stringFromDate:d]];
            return;
        default:
            *date = [self.axisFormatterDateBig stringFromDate:d];
            return;
    }
}

- (int) hlocsOfMask:(int) mask Since:(NSUInteger) item0 Till:(NSUInteger) countLimit {
    int timesCount = 0;
    for (int item = item0; item < countLimit; item++)
        if (hlocs[item].tag & mask)
            timesCount++;
    return timesCount;
}

//- (CSLabelComposition *) arrangePriceLabelsForMinimalWidth:(float) minW IntFrame:(CGRect) intFrame Previous:(CSLabelComposition *)previousComposition {
//    if (previousComposition) {
//
//    } else {
//        if (_badScale) return nil;
//        previousComposition = [[CSLabelComposition alloc] initFor:candleItemInterval MinLabelWidth:minW];
//        lastStartIndex = startIndex;
//        lastTimeAxisLength = intFrame.size.width;
//        lastItemSize = itemSize;
//        [self getTimeForX:0 ItemUsed:&lastUsedItemNo];
//    }
//    int countLimit = MIN(lastUsedItemNo + (intFrame.size.width / itemSize), count);
//    [previousComposition updateWith:lastUsedItemNo CountLimit:countLimit];
//}

- (void) enumerateVerticalGridUsingBlock:(GridEnum)block ForMinimalWidth:(float)minW IntFrame:(CGRect)intFrame {
    NSUInteger lastUsedItemNo;
//    if (intFrame.origin.x < -itemSize)
//        intFrame.origin.x = fmod(intFrame.origin.x, itemSize);
    NSString *time;
    [self getTimeForX:intFrame.origin.x ItemUsed:&lastUsedItemNo];
    if (_badScale) return;
    NSString *date;// = [self getDateForItemUsed:lastUsedItemNo];
    int mask1 = mask0;
    int maxTimesCount = (intFrame.size.width / minW) + 1;
    int countLimit = MIN(lastUsedItemNo + (intFrame.size.width / itemSize), count);
    int lastGoodMask = 0;
    NSUInteger lastGoodCount = 0;
    for (; mask1 < hYearChange; mask1 <<= 1) {
        NSUInteger icnt = [self hlocsOfMask:mask1 Since:lastUsedItemNo Till:countLimit];
        if (icnt) {
            if (icnt > lastGoodCount) {
                lastGoodCount = icnt;
                lastGoodMask = mask1;
            }
            if (icnt > maxTimesCount)
                continue;
        } else
            if (lastGoodCount > 0)
                mask1 = lastGoodMask;
        break;
    }
    NSDateFormatter *m1f = self.axisFormatterTime;
    NSDateFormatter *m2f = self.axisFormatterDateBig;
    int mask2 = hYearChange;
    switch (mask1) {
        case hYearChange:
            mask1 = 0;
            m1f = nil;
            m2f = self.axisFormatterYear;
            break;
        case hMonthChange:
        case hWeekChange:
            mask2 = hYearChange;
            m1f = self.axisFormatterDate;
            if (mask1 == hMonthChange)
                [m1f setDateFormat:@"MMM"];
            else
                [m1f setDateFormat:@"MM dd"];
            m2f = self.axisFormatterYear;
            break;
        case hDayChange:
            mask2 = hMonthChange;
            m1f = self.axisFormatterDay;
            m2f = self.axisFormatterDate;
            [m2f setDateFormat:@"MMM yyyy"];
            break;
        default:
            mask2 = hDayChange;
            break;
    }
    if (m1f) {
        int idx = 0;
        int hadDate = NO;
        for (int item = lastUsedItemNo; item < count; item++) {
            if (hlocs[item].tag & (mask1 | mask2)) {
                float labelX = ((item - startIndex)*itemSize);
                if (labelX < 0) continue;
                if (labelX > intFrame.size.width) break;
                NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[item]];//startIndex + (x / itemSize)];
                if (!hadDate || ((hlocs[item].tag & mask2) != 0)) {
                    date = [m2f stringFromDate:d];
                    hadDate = YES;
                } else
                    date = nil;
                if (mask1 && ((hlocs[item].tag & mask1) != 0)) {
                    time = [m1f stringFromDate:d];
                } else
                    time = nil;
                block(idx++, time, date, intFrame.origin.x + labelX);
            }
        }
        if (!hadDate) {
            NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[lastUsedItemNo]];
            block(idx, nil, [m2f stringFromDate:d], intFrame.origin.x);
        }
    } else { // 'year' labels only
        int idx = 0;
        for (int item = lastUsedItemNo; item < count; item++) {
            if ((hlocs[item].tag & mask2) || (item == lastUsedItemNo)) {
                float labelX = ((item - startIndex)*itemSize);
                if (labelX > intFrame.size.width) break;
                NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:times[item]];//startIndex + (x / itemSize)];
                date = [m2f stringFromDate:d];
                block(idx++, nil, date, intFrame.origin.x + labelX);
            }
        }
    }
}

- (void) dropData {
    count = 0;
    _badScale = YES;
    if (capacity > 0) {
        CFAbsoluteTime t = -1;
        vDSP_vfillD(&t, times, 1, capacity);
        feedingTime.hour = -1;
        feedingTime.day = -1;
        dayOfWeek = -1;
        feedingTime.month = -1;
        feedingTime.year = -1;
    }
}

- (void) incCapacity {
    if (capacity) {
        capacity *= 2;
        CFAbsoluteTime *tempt = malloc(sizeof(CFAbsoluteTime) * capacity);
        HLOCV *temph = malloc(sizeof(HLOCV) * capacity);
        if (count > 0) {
            memcpy(tempt, times, sizeof(CFAbsoluteTime) * count);
            memcpy(temph, hlocs, sizeof(HLOCV) * count);
        }
        CFAbsoluteTime *tempT = times;
        times = tempt;
        free(tempT);
        HLOCV *tempH = hlocs;
        hlocs = temph;
        free(tempH);
    } else {
        capacity = 16;
        times = malloc(sizeof(CFAbsoluteTime) * capacity);
        hlocs = malloc(sizeof(HLOCV) * capacity);
    }
}

- (void) incPacketCapacity {
    packetCapacity *= 2;
    CFAbsoluteTime *tempt = malloc(sizeof(CFAbsoluteTime) * packetCapacity);
    HLOCV *temph = malloc(sizeof(HLOCV) * packetCapacity);
    if (packetCount > 0) {
        memcpy(tempt, packetTimes, sizeof(CFAbsoluteTime) * packetCount);
        memcpy(temph, packetHLOCs, sizeof(HLOCV) * packetCount);
    }
    CFAbsoluteTime *tempT = packetTimes;
    packetTimes = tempt;
    free(tempT);
    HLOCV *tempH = packetHLOCs;
    packetHLOCs = temph;
    free(tempH);
}

- (BOOL) extendDataByOnePseudoHLOC:(CFAbsoluteTime) newTime GD:(CFGregorianDate) newGD {
    if (packetCount == 0) return NO;
    packetTimes[packetCount] = newTime;//packetTimes[packetCount - 1] + addT;
    packetHLOCs[packetCount].high =
    packetHLOCs[packetCount].low =
    packetHLOCs[packetCount].open =
    packetHLOCs[packetCount].close = packetHLOCs[packetCount - 1].close;
    packetHLOCs[packetCount].tag = hPseudo | [self changeMaskForTime:newTime GD:newGD];
    packetHLOCs[packetCount].value = -1.0;
    packetCount++;
    if (packetCapacity == packetCount) [self incPacketCapacity];
    return YES;
}

- (void) beginFeeding:(NSUInteger) minimumCapacity {
    if (count > 0) {
        minimumCapacity++;
        packetCount = 1;
    } else
        packetCount = 0;
    if (packetCapacity < minimumCapacity) {
        if (packetCapacity) {
            free(packetHLOCs);
            free(packetTimes);
        }
        packetCapacity = minimumCapacity;
        packetTimes = malloc(sizeof(CFAbsoluteTime) * packetCapacity);
        packetHLOCs = malloc(sizeof(HLOCV) * packetCapacity);
    }
    if (count > 0) {
        packetTimes[0] = times[count - 1];
        packetHLOCs[0] = hlocs[count - 1];
    }
}

- (BOOL) dowChange:(CFAbsoluteTime) t {
    int dow = CFAbsoluteTimeGetDayOfWeek(t, defZone);
    if (dow < dayOfWeek) {
        dayOfWeek = dow;
        return YES;
    }
    return NO;
}

- (int) intradayDayChangeMaskForTime:(CFAbsoluteTime) t {
    if ([self dowChange:t])
        return hWeekChange | hDayChange | h4HourChange | hHOURChange | h30MinChange | h15MinChange | h10MinChange;
    return hDayChange | h4HourChange | hHOURChange | h30MinChange | h15MinChange | h10MinChange;
}

- (int) daylyChangeMaskForTime:(CFAbsoluteTime) t {
    if ([self dowChange:t])
        return hWeekChange | hDayChange;
    return hDayChange;
}

- (int) changeMaskForTime:(CFAbsoluteTime) t GD:(CFGregorianDate) gd{
    if (gd.year == feedingTime.year) {
        if (candleItemInterval > ciWeek) // ciMonth
            return hMonthChange | hDayChange;
        if (gd.month == feedingTime.month) {
            switch (candleItemInterval) {
                case ciMinute:
                case ci10Minutes:
                case ciHour:
                    if (gd.day == feedingTime.day) {
                        if (gd.hour == feedingTime.hour) {
                            if (gd.minute % 30) {
                                if (gd.minute % 15) {
                                    if (gd.minute % 10)
                                        return 0;
                                    return h10MinChange;
                                }
                                return h15MinChange;
                            }
                            return h30MinChange | h15MinChange | h10MinChange;
                        }
                        feedingTime.hour = gd.hour;
                        if (gd.hour % 4)
                            return hHOURChange | h30MinChange | h15MinChange | h10MinChange;
                        return h4HourChange | hHOURChange | h30MinChange | h15MinChange | h10MinChange;
                    }
                    feedingTime.day = gd.day;
                    feedingTime.hour = gd.hour;
                    return [self intradayDayChangeMaskForTime:t];
                case ciDay:  return [self daylyChangeMaskForTime:t];
                case ciWeek: return hWeekChange;
                default:
                    return 0;
            }
        } else { // new month
            feedingTime = gd;
            switch (candleItemInterval) {
                    //case ciMonth:
                case ciWeek: return hMonthChange | hWeekChange;
                case ciDay:  return hMonthChange | [self daylyChangeMaskForTime:t];
                default: // < ciDay
                    return hMonthChange | [self intradayDayChangeMaskForTime:t];
            }
        }
    } else { // new year
        feedingTime = gd;
        switch (candleItemInterval) {
            case ciMonth:    return hYearChange | hMonthChange;
            case ciWeek:     return hYearChange | hMonthChange | hWeekChange;
            case ciDay:      return hYearChange | hMonthChange | [self daylyChangeMaskForTime:t];
            default: // < ciDay
                return hYearChange | hMonthChange | [self intradayDayChangeMaskForTime:t];
        }
    }
}

- (void) extendDataByMultiplePseudoHLOCsAdding:(CFGregorianUnits) gdadd Till:(CFAbsoluteTime) t {
    CFAbsoluteTime gapTime = CFAbsoluteTimeAddGregorianUnits(packetTimes[packetCount - 1], defZone, gdadd);
    CFGregorianDate gapGD = CFAbsoluteTimeGetGregorianDate(gapTime, defZone);
    while ((t - gapTime > maxCurrentCandleInterval) && [self extendDataByOnePseudoHLOC:gapTime GD:gapGD]) {
        gapTime = CFAbsoluteTimeAddGregorianUnits(gapTime, defZone, gdadd);
        gapGD = CFAbsoluteTimeGetGregorianDate(gapTime, defZone);
    }
}

- (void) feedH:(float)h L:(float)l O:(float)o C:(float)c V:(float)v AtTime:(CFAbsoluteTime)t {
    if (packetCapacity == packetCount) [self incPacketCapacity];
    CFGregorianDate gd = CFAbsoluteTimeGetGregorianDate(t, defZone);
    if (packetCount > 0) {
        double delta = t - packetTimes[packetCount - 1];
        if (delta < 0) return;
        if (delta < currentCandleInterval - 0.1)
            packetCount--; // replace last candle
        else { // filling gaps if necessary
            if (delta > maxCurrentCandleInterval) { // looks like we have a gap
                switch (candleItemInterval) {
                    case ci10Minutes:
                    case ciHour: {
                        CFGregorianUnits gdadd = {0, 0, 1, 0, 0, 0};
                        CFAbsoluteTime nextDay = CFAbsoluteTimeAddGregorianUnits(packetTimes[packetCount - 1], defZone, gdadd);
                        CFGregorianDate nextDayGD = CFAbsoluteTimeGetGregorianDate(nextDay, defZone);
                        if ((nextDayGD.month < gd.month) || ((nextDayGD.month == gd.month) && (nextDayGD.day <= gd.day))) {
                            // skipping gap, since the new day starts
                        } else {
                            gdadd.days = 0;
                            if (candleItemInterval == ciHour)
                                gdadd.hours = 1;
                            else
                                gdadd.minutes = 10;
                            [self extendDataByMultiplePseudoHLOCsAdding:gdadd Till:t];
                        }
                        break;
                    }
                    case ciDay: {
                        CFGregorianUnits gdadd = {0, 0, 1, 0, 0, 0};
                        CFAbsoluteTime gapTime = CFAbsoluteTimeAddGregorianUnits(packetTimes[packetCount - 1], defZone, gdadd);
                        CFGregorianDate gapGD = CFAbsoluteTimeGetGregorianDate(gapTime, defZone);
                        if ((feedingTime.year == gd.year) && (feedingTime.month == gd.month)) {
                            if (gd.day > feedingTime.day + 1) { // skipping gap of more than two days
                            } else
                                [self extendDataByOnePseudoHLOC:gapTime GD:gapGD];
                        } else {
                            if ((gd.year == gapGD.year) && // next year or same year
                                (gd.month == gapGD.month) && // next month
                                (gd.day == gapGD.day + 1))
                                [self extendDataByOnePseudoHLOC:gapTime GD:gapGD];
                        }
                        break;
                    }
                    case ciWeek: {
                        CFGregorianUnits gdadd = {0, 0, 7, 0, 0, 0};
                        [self extendDataByMultiplePseudoHLOCsAdding:gdadd Till:t];
                        break;
                    }
                    case ciMonth: {
                        CFGregorianUnits gdadd = {0, 1, 0, 0, 0, 0};
                        [self extendDataByMultiplePseudoHLOCsAdding:gdadd Till:t];
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
    packetTimes[packetCount] = t;
    packetHLOCs[packetCount].high = h;
    packetHLOCs[packetCount].low = l;
    packetHLOCs[packetCount].open = o;
    packetHLOCs[packetCount].close = c;
    packetHLOCs[packetCount].value = v;
    packetHLOCs[packetCount].tag = hNormal | [self changeMaskForTime:t GD:gd];
    packetCount++;
}

- (FeedingResult) feedingDone {
    if (packetCount > 0) {
        if (count > 0) {
            NSUInteger newCount = count + packetCount - 1;
            while (newCount > capacity)
                [self incCapacity];
            memcpy(&(times[count - 1]), packetTimes, sizeof(CFAbsoluteTime)*packetCount);
            memcpy(&(hlocs[count - 1]), packetHLOCs, sizeof(HLOCV)*packetCount);
            if (count == newCount)
                return frLastCandleUpdateOnly;
            count = newCount;
            return frNewCandlesAdded;
        } else {
            while (packetCount > capacity)
                [self incCapacity];
            memcpy(times, packetTimes, sizeof(CFAbsoluteTime)*packetCount);
            memcpy(hlocs, packetHLOCs, sizeof(HLOCV)*packetCount);
            count = packetCount;
            return frCompletelyNewData;
        }
    } else {
        return frBad;
    }
}


- (double) priceAtY:(float) y {
    if (_badScale) return 0;
    return [self.representation priceAtY:y];
}

- (float) yForPrice:(double) p {
    if (_badScale) return 0;
    return [self.representation yForPrice:p];
}

- (double) startIndexMod:(int) modFactor {
    return fmod(startIndex, modFactor);
}

- (double) lastClose {
    if (!_badScale && (count))
        return hlocs[count - 1].close;
    return 0;
}

- (NSUInteger) getData:(HLOCV **)ahlocs Times:(CFAbsoluteTime **)time {
    *ahlocs = hlocs;
    *time = times;
    return count;
}

@end

