//
//  CSScaleRepresentationLine.m
//  chartster
//
//  Created by Danila Parhomenko on 4/17/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSScaleRepresentationLine.h"
#import <Accelerate/Accelerate.h>


@implementation CSScaleRepresentationLine

- (void) dealloc {
    if (lineCapacity) {
        free(line);
        free(animline);
    }
}

- (void) setLineLength:(int) theLineLength HLOCVs:(HLOCV *)hlocs ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect)rVolume {
    lineLength = theLineLength;
    if (lineCapacity < lineLength + 2) {
        if (line) free(line);
        if (animline) free(animline);
        line = malloc(sizeof(CGPoint) * (lineLength + 2));
        animline = malloc(sizeof(CGPoint) * lineLength);
        lineCapacity = lineLength;
    }
    float *startPtr = &(hlocs->close);
    vDSP_maxv(startPtr, HLOCVStride, &maxP, lineLength);
    vDSP_minv(startPtr, HLOCVStride, &minP, lineLength);
    float margin = (maxP - minP) * 0.10; // 10% margin above max and below min
    maxP += margin;
    minP -= margin;
    startPtr = &(hlocs->value);
    vDSP_maxv(startPtr, HLOCVStride, &maxValue, lineLength);
    maxValue *= 1.10; // 10% margin
    float startX = rFrame.origin.x;
    float itemSize = curItemSize;
    line[0] = CGPointMake(rFrame.origin.x, rFrame.origin.y + rFrame.size.height);
    vDSP_vramp(&(startX), &(itemSize), &(line[1].x), 2, lineLength);
    line[lineLength + 1] = CGPointMake(rFrame.origin.x + rFrame.size.width, rFrame.origin.y + rFrame.size.height);
 
    [super setLineLength:theLineLength HLOCVs:hlocs ItemSize:curItemSize RepresentationFrame:rFrame VolumeFrame:rVolume];
    startPtr = &(hlocs->close);
    vDSP_vsmsa(startPtr, HLOCVStride, &rescale, &offset, &(line[1].y), 2, lineLength);
               
    memcpy(animline, line, sizeof(CGPoint) * lineLength);
}
                
- (void) drawPresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add {
    [self useMainColorOn:context];
    CGContextSaveGState(context);
    CGContextClipToRect(context, priceFrame);
        vDSP_vsmsa(&(line[1].y), 2, &drawen, &add, &(animline[0].y), 2, lineLength * drawen);
        CGContextAddLines(context, animline, lineLength * drawen);
        CGContextStrokePath(context);
    CGContextRestoreGState(context);

    [super drawPresentationInContext:context Drawen:drawen Addendum:add];
    CGContextSetRGBStrokeColor(context, 0, 0, 0, 1.0);
}

- (void) drawInContext:(CGContextRef)context {
    [self useMainColorOn:context];
    CGContextSaveGState(context);
    CGContextClipToRect(context, priceFrame);
        CGContextAddLines(context, line + 1, lineLength);
        CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    [super drawInContext:context];
    CGContextSetRGBStrokeColor(context, 0, 0, 0, 1.0);
}

- (void) addChartSkyLineTo:(CGContextRef) context {
    CGContextAddLines(context, line, lineLength + 2);
}

@end

@implementation CSScaleRepresentationSubLine

- (void) setLineLength:(int) theLineLength HLOCVs:(HLOCV *)hlocs ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect)rVolume {
    [super setLineLength:theLineLength HLOCVs:hlocs ItemSize:curItemSize RepresentationFrame:rFrame VolumeFrame:rVolume];
    float add = _xoffset;
    vDSP_vsadd(&(volumeBodies->origin.x), 4, &add, &(volumeBodies->origin.x), 4, volumeCount);
    add = -0.5;
    vDSP_vsadd(&(volumeBodies->size.width), 4, &add, &(volumeBodies->size.width), 4, volumeCount);
}

- (void) prepareGraphicsL:(NSString *)il R:(NSString *)ir {
    UIImage *i = [UIImage imageNamed:il];
    
    // tag image parts:
    //   /|      |\
    //  ( | 10.2 | >--o
    //   \|      |/
    //
    // tag1 tagx  tag0
    tagLeft1 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(0, 0, TAGMARGIN*2, (TAGHEIGHT)*2));
    tagLeftx = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(TAGMARGIN*2, 0, 1, TAGHEIGHT*2));
    tagLeft0 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(TAGMARGIN*2 + 1, 0, (TAGWIDTH - TAGMARGIN)*2 - 1, TAGHEIGHT*2));
    i = [UIImage imageNamed:ir];
    
    // tag image parts:
    //     /|      |\
    // o--< | 88.9 | )
    //     \|      |/
    //
    // tag0  tagx  tag1
    tagRight0 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(0, 0, (TAGWIDTH - TAGMARGIN)*2, (TAGHEIGHT)*2));
    tagRightx = CGImageCreateWithImageInRect(i.CGImage, CGRectMake((TAGWIDTH - TAGMARGIN)*2 - 1, 0, 1, TAGHEIGHT*2));
    tagRight1 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake((TAGWIDTH - TAGMARGIN)*2, 0, TAGMARGIN*2, TAGHEIGHT*2));
}

- (void) drawLeftTagOn:(CGContextRef)context P:(CGPoint)p Text:(NSString *)price {
    CGFloat ext = self.lastTagWidth;
    p.x += 5 - ext; // to match center of dot
    p.y -= 0.5 + (TAGHEIGHT/2);
    CGContextDrawImage(context, CGRectMake(p.x + 1, p.y, TAGMARGIN, TAGHEIGHT), tagLeft1);
    CGContextDrawImage(context, CGRectMake(p.x + TAGMARGIN + 0.5, p.y, ext - TAGWIDTH, TAGHEIGHT), tagLeftx);
    CGContextDrawImage(context, CGRectMake(p.x + ext + TAGMARGIN - TAGWIDTH, p.y, TAGWIDTH - TAGMARGIN, TAGHEIGHT), tagLeft0);
    CGContextShowTextAtPoint(context, p.x + TAGMARGIN, p.y + 3 + 0.5 + (TAGHEIGHT/2), [price cStringUsingEncoding:NSMacOSRomanStringEncoding], price.length);
}

- (void) drawRightTagOn:(CGContextRef) context P:(CGPoint)p Text:(NSString *)price {
    CGFloat ext = self.lastTagWidth;
    p.x -= 6; // to match center of dot
    p.y -= 0.5 + (TAGHEIGHT/2);
    CGContextDrawImage(context, CGRectMake(p.x, p.y, TAGWIDTH - TAGMARGIN, TAGHEIGHT), tagRight0);
    CGContextDrawImage(context, CGRectMake(p.x + TAGWIDTH - TAGMARGIN - 0.5, p.y, ext - TAGWIDTH, TAGHEIGHT), tagRightx);
    CGContextDrawImage(context, CGRectMake(p.x + ext - TAGMARGIN - 1.0, p.y, TAGMARGIN, TAGHEIGHT), tagRight1);
    CGContextShowTextAtPoint(context, p.x + TAGWIDTH - TAGMARGIN, p.y + 3 + 0.5 + (TAGHEIGHT/2), [price cStringUsingEncoding:NSMacOSRomanStringEncoding], price.length);

}

- (id) initWithScaler:(id<SmoothScalerProtocol>)sc ColorIndex:(NSUInteger) uidx {
    self = [super initWithScaler:sc];
    if (self) {
        self.uniqueIdx = uidx;
        [self prepareGraphicsL:[NSString stringWithFormat:@"cursor%d_left2@x.png", uidx + 2] R:[NSString stringWithFormat:@"cursor%d_right2@x.png", uidx + 2]];
    }
    return self;
}

@end
