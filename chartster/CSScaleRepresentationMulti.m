//
//  CSScaleRepresentationMulti.m
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSScaleRepresentationMulti.h"
#import <Accelerate/Accelerate.h>

@interface CSScaleRepresentationMulti () {
    float maxMultiChartEdge;
    float minMultiChartEdge;
    BOOL calculatingMain;
    
    CGImageRef tagLeft0, tagLeftx, tagLeft1;
    CGImageRef tagRight0, tagRightx, tagRight1;
}

@property (nonatomic, strong) NSMutableDictionary *subs;
@property (nonatomic, weak) CSSubContainer *currentObj;

@property (nonatomic, weak) id<SmoothScalerProtocol> superScaler;

@end

@implementation CSScaleRepresentationMulti

- (id) initWithScaler:(id<SmoothScalerProtocol>)sc {
    self = [super initWithScaler:nil];
    if (self) {
        self.scaler = self;
        self.superScaler = sc;
        [self prepareGraphicsL:@"cursor_blue_left2@x.png" R:@"cursor_blue_right2@x.png"];
    }
    return self;
}

- (void) dealloc {
    CGImageRelease(tagLeft0);
    CGImageRelease(tagLeftx);
    CGImageRelease(tagLeft1);
    CGImageRelease(tagRight0);
    CGImageRelease(tagRightx);
    CGImageRelease(tagRight1);
}

- (void) prepareGraphicsL:(NSString *)il R:(NSString *)ir {
    UIImage *i = [UIImage imageNamed:il];
    
    // tag image parts:
    //   /|      |\
    //  ( | 10.2 | >--o
    //   \|      |/
    //
    // tag1 tagx  tag0
    tagLeft1 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(0, 0, TAGMARGIN*2, (TAGHEIGHT)*2));
    tagLeftx = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(TAGMARGIN*2, 0, 1, TAGHEIGHT*2));
    tagLeft0 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(TAGMARGIN*2 + 1, 0, (TAGWIDTH - TAGMARGIN)*2 - 1, TAGHEIGHT*2));
    i = [UIImage imageNamed:ir];
    
    // tag image parts:
    //     /|      |\
    // o--< | 88.9 | )
    //     \|      |/
    //
    // tag0  tagx  tag1
    tagRight0 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake(0, 0, (TAGWIDTH - TAGMARGIN)*2, (TAGHEIGHT)*2));
    tagRightx = CGImageCreateWithImageInRect(i.CGImage, CGRectMake((TAGWIDTH - TAGMARGIN)*2 - 1, 0, 1, TAGHEIGHT*2));
    tagRight1 = CGImageCreateWithImageInRect(i.CGImage, CGRectMake((TAGWIDTH - TAGMARGIN)*2, 0, TAGMARGIN*2, TAGHEIGHT*2));
}
#ifdef mywarns
#warning move this ^ method into parent
#endif

- (void) killUselessSubsWith:(NSArray *)usedScaleIds {
    for (NSString *s in _subs.allKeys)
        if ([usedScaleIds indexOfObject:s] == NSNotFound) {
            [_subs setValue:nil forKey:s];
            [self killUselessSubsWith:usedScaleIds];
            break;
        }
}

- (NSUInteger) firstUnusedIdx {
    NSMutableIndexSet *is = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 6)];
    if (_subs) {
        [_subs enumerateKeysAndObjectsUsingBlock:^(id key, CSScaleRepresentationSubLine *obj, BOOL *stop) {
            [is removeIndex:obj.uniqueIdx];
        }];
    }
    if (is.count > 0)
        return [is firstIndex];
    return NSNotFound;
}

- (void) setLineLengthForContainers:(NSDictionary *)hlocs MainScale:(NSString *)mainScaleId ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect) rVolume {
    if (self.subs == nil)
        self.subs = [[NSMutableDictionary alloc] init];
    CSSubContainer *c = [hlocs valueForKey:mainScaleId];
    HLOCV *ahlocs = nil;
    CFAbsoluteTime *t = nil;
    NSUInteger count = [c getData:&ahlocs Times:&t];
    if (count == 0) {
        [super setLineLength:c.lastEffectiveLineLength HLOCVs:&(ahlocs[c.lastEffectiveStartIndex]) ItemSize:curItemSize RepresentationFrame:rFrame VolumeFrame:rVolume];
        return;
    }
    [self killUselessSubsWith:hlocs.allKeys];
    if (hlocs.allKeys.count > _subs.allKeys.count)
        [hlocs enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
            if ([_subs valueForKey:key] == nil) {
                if ([key isEqualToString:mainScaleId])
                    [_subs setValue:self forKey:key];
                else {
                    NSUInteger i = [self firstUnusedIdx];
                    if (i == NSNotFound) i = 0;
                    CSScaleRepresentationSubLine *subLine = [[CSScaleRepresentationSubLine alloc] initWithScaler:self ColorIndex:i];
//    if (v) {
//        NSDictionary *s = ((CSScaleRepresentationMulti *)self.representation).subs;§
//        [s.allKeys enumerateObjectsUsingBlock:^(id key, NSUInteger idx, BOOL *stop) {
//            if ([v count] <= idx)
//                *stop = YES;
//            else {
//                NSDictionary *p = v[idx];
//                CSScaleRepresentationLine *l = [s valueForKey:key];
//                l.mainColor = [p valueForKey:kPropertyKeyColor];
//                l.lineWidth = [p valueForKey:kPropertyKeyWidth];
//            }
//        }];
//    }
                    if (_subAttributes) {
                        subLine.mainColor = [_subAttributes[subLine.uniqueIdx] valueForKey:kPropertyKeyColor];
                        subLine.lineWidth = [_subAttributes[subLine.uniqueIdx] valueForKey:kPropertyKeyWidth];
                    } else {
                        subLine.mainColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5];
                        subLine.lineWidth = @1;
                    }
                    [_subs setValue:subLine forKey:key];
                }
            }
        }];

    __block float maxPercentEdge = -1000000;
    __block float minPercentEdge =  1000000; // quite a lot (in percents!)
    __block float delta = 0;
    [hlocs enumerateKeysAndObjectsUsingBlock:^(NSString *scaleId, CSSubContainer *obj, BOOL *stop) {
        HLOCV *ahlocs;
        CFAbsoluteTime *t;
        NSUInteger subCount = [obj getData:&ahlocs Times:&t];
        int ll = c.lastEffectiveLineLength;
        int si = c.lastEffectiveStartIndex;
        if (si + ll > subCount) {
            if (si >= subCount) {
                c.lastEffectivePercentMax = -1;
                c.lastEffectivePercentMin = -1;
                ll = 0;
            } else
                ll = subCount - si;
        }
        if (ll > 0) {
            float *startPtr = &(ahlocs[c.lastEffectiveStartIndex].close);
            float maxSubEdge;
            float minSubEdge;
            float close = ahlocs[ll - 1].close;
            obj.lastEffectiveClose = close;
            vDSP_maxv(startPtr, HLOCVStride, &maxSubEdge, ll);
            if (ABS(close) > EPSILON) {
                maxSubEdge /= close;
                obj.lastEffectivePercentMax = maxSubEdge;
                vDSP_minv(startPtr, HLOCVStride, &minSubEdge, ll);
                minSubEdge /= close;
                obj.lastEffectivePercentMin = minSubEdge;
            } else { // zero close
                maxSubEdge += 1.0; // super hack! if you find it pretend it never happend
                obj.lastEffectivePercentMax = maxSubEdge;
                vDSP_minv(startPtr, HLOCVStride, &minSubEdge, ll);
                minSubEdge += 1.0;
                obj.lastEffectivePercentMin = minSubEdge;
            }

            if ([scaleId isEqualToString:mainScaleId]) {
                maxPercentEdge = maxSubEdge;
                minPercentEdge = minSubEdge;
                delta = (maxPercentEdge - minPercentEdge);
            }
        }
    }];
    float center = (maxPercentEdge + minPercentEdge)*0.5;
    [hlocs enumerateKeysAndObjectsUsingBlock:^(id key, CSSubContainer *obj, BOOL *stop) {
        float percentSubDelta = obj.lastEffectivePercentMax - obj.lastEffectivePercentMin;
        if (delta < percentSubDelta) {
            delta = percentSubDelta;
            maxPercentEdge = center + (delta * 0.5);
            minPercentEdge = center - (delta * 0.5);
        }
    }];
    maxMultiChartEdge = maxPercentEdge + delta * 0.1; // 10% margin
    minMultiChartEdge = minPercentEdge - delta * 0.1;
    if (self.superScaler)
        [self.superScaler correctMax:&maxMultiChartEdge Min:&minMultiChartEdge];
    _currentObj = [hlocs valueForKey:mainScaleId];
    calculatingMain = YES;
    [super setLineLength:c.lastEffectiveLineLength HLOCVs:&(ahlocs[c.lastEffectiveStartIndex]) ItemSize:curItemSize RepresentationFrame:rFrame VolumeFrame:rVolume];
    float add = -0.5;
    vDSP_vsadd(&(volumeBodies->size.width), 4, &add, &(volumeBodies->size.width), 4, volumeCount);

    calculatingMain = NO;
    __block float xIncrementalOffset = 0.5;
    [hlocs enumerateKeysAndObjectsUsingBlock:^(NSString *scaleId, CSSubContainer *obj, BOOL *stop) {
        if (![scaleId isEqualToString:mainScaleId]) {
            HLOCV *ahlocs = nil;
            CFAbsoluteTime *t = nil;
            //NSUInteger count =
            [obj getData:&ahlocs Times:&t];
            CSScaleRepresentationSubLine *subLine = [self.subs valueForKey:scaleId];
            if ((obj.lastEffectivePercentMax - obj.lastEffectivePercentMin) > EPSILON) {
                _currentObj = obj;
                subLine.xoffset = xIncrementalOffset;
                [subLine setLineLength:c.lastEffectiveLineLength HLOCVs:&(ahlocs[c.lastEffectiveStartIndex]) ItemSize:curItemSize RepresentationFrame:rFrame VolumeFrame:rVolume];
                xIncrementalOffset += 0.5;
            }
        }
    }];
    _currentObj = nil;

}

- (void) correctMax:(inout CGFloat *)amaxP Min:(inout CGFloat *)aminP{
    if (_currentObj) {
        if (calculatingMain) {
            *amaxP = maxMultiChartEdge;
            *aminP = minMultiChartEdge;
        } else {
            float realSubChartPercentCenter = (*amaxP + *aminP)*0.5/_currentObj.lastEffectiveClose;
            float overallHalfDelta = (maxMultiChartEdge - minMultiChartEdge)*0.5;
            *amaxP = (realSubChartPercentCenter + overallHalfDelta);
            *aminP = (realSubChartPercentCenter - overallHalfDelta);
        }
        *amaxP *= _currentObj.lastEffectiveClose;
        *aminP *= _currentObj.lastEffectiveClose;
    }
}

- (void) drawPresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add {
    [_subs enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self isEqual:obj])
            [super drawPresentationInContext:context Drawen:drawen Addendum:add];
        else
            [((CSScaleRepresentationLine *)obj) drawPresentationInContext:context Drawen:drawen Addendum:add];
        CGContextStrokePath(context);
    }];
}
// zero fix for simple percent chart still pending and 'correctMax' here

- (void) drawInContext:(CGContextRef)context {
    [_subs enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self isEqual:obj])
            [super drawInContext:context];
        else
            [((CSScaleRepresentationLine *)obj) drawInContext:context];
        CGContextStrokePath(context);
    }];
}

- (void) drawParameter:(NSString *) param PresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add {
    [_subs enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([key isEqual:param]) {
            if ([self isEqual:obj])
                [super drawPresentationInContext:context Drawen:drawen Addendum:add];
            else
                [obj drawPresentationInContext:context Drawen:drawen Addendum:add];
        } else {
            if ([self isEqual:obj])
                [super drawInContext:context];
            else
                [obj drawInContext:context];
        }
        CGContextStrokePath(context);
    }];
}

- (void) drawLeftTagOn:(CGContextRef) context P:(CGPoint)p Text:(NSString *)price {
    CGFloat ext = self.lastTagWidth;
    p.x += 5 - ext; // to match center of dot
    p.y -= 0.5 + (TAGHEIGHT/2);
    CGContextDrawImage(context, CGRectMake(p.x + 1, p.y, TAGMARGIN, TAGHEIGHT), tagLeft1);
    CGContextDrawImage(context, CGRectMake(p.x + TAGMARGIN + 0.5, p.y, ext - TAGWIDTH, TAGHEIGHT), tagLeftx);
    CGContextDrawImage(context, CGRectMake(p.x + ext + TAGMARGIN - TAGWIDTH, p.y, TAGWIDTH - TAGMARGIN, TAGHEIGHT), tagLeft0);
    CGContextShowTextAtPoint(context, p.x + TAGMARGIN, p.y + 3 + 0.5 + (TAGHEIGHT/2), [price cStringUsingEncoding:NSMacOSRomanStringEncoding], price.length);
}

- (void) drawRightTagOn:(CGContextRef) context P:(CGPoint)p Text:(NSString *)price {
    CGFloat ext = self.lastTagWidth;
    p.x -= 6; // to match center of dot
    p.y -= 0.5 + (TAGHEIGHT/2);
    CGContextDrawImage(context, CGRectMake(p.x, p.y, TAGWIDTH - TAGMARGIN, TAGHEIGHT), tagRight0);
    CGContextDrawImage(context, CGRectMake(p.x + TAGWIDTH - TAGMARGIN - 0.5, p.y, ext - TAGWIDTH, TAGHEIGHT), tagRightx);
    CGContextDrawImage(context, CGRectMake(p.x + ext - TAGMARGIN - 1.0, p.y, TAGMARGIN, TAGHEIGHT), tagRight1);
    CGContextShowTextAtPoint(context, p.x + TAGWIDTH - TAGMARGIN, p.y + 3 + 0.5 + (TAGHEIGHT/2), [price cStringUsingEncoding:NSMacOSRomanStringEncoding], price.length);
}
#ifdef mywarns
#warning move this ^ method into parent
#endif

- (void) drawHint:(NSString *) hint On:(CGContextRef) context At:(CGPoint) p Font:(UIFont *)font ScaleKey:(NSString *)mkey  {
    CSScaleRepresentationLine *subLine = [_subs valueForKey:mkey];
    if (font)
        subLine.lastTagWidth = [hint sizeWithFont:font].width + TAGWIDTH;
    if ([subLine isKindOfClass:[CSScaleRepresentationSubLine class]]) {
        if (p.x > subLine.lastTagWidth - 2)
            [((CSScaleRepresentationSubLine *)subLine) drawLeftTagOn:context P:p Text:hint];
        else
            [((CSScaleRepresentationSubLine *)subLine) drawRightTagOn:context P:p Text:hint];
    } else {
        if (p.x > subLine.lastTagWidth - 2)
            [self drawLeftTagOn:context P:p Text:hint];
        else
            [self drawRightTagOn:context P:p Text:hint];
        
    }
    CGContextFillPath(context);
}

@end
