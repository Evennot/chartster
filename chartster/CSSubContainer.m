//
//  CSSubContainer.m
//  GIConnector
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "CSSubContainer.h"

#ifdef mywarns
#warning ^^ not used
#endif



@implementation CSSubContainer

- (id) initWithPacket:(CSSubContainer *)prev Zone:(CFTimeZoneRef) zone CI:(CandleInterval)theCandleItemInterval DecFormat:(NSString *)xdecimalledFormat {
    self = [super init];
    if (self) {
        self.decimalledFormat = xdecimalledFormat;
        packetCapacity = 16;
        packetHLOCs = malloc(packetCapacity * sizeof(HLOCV));
        packetTimes = malloc(packetCapacity * sizeof(CFAbsoluteTime));
        defZone = zone;
        
        switch (theCandleItemInterval) {
            case ciMinute:
                currentCandleInterval = intervalLengths[0];
                break;
            case ciHour:
                currentCandleInterval = intervalLengths[3];
                break;
            case ciDay:
                currentCandleInterval = intervalLengths[5];
                break;
            case ciWeek:
                currentCandleInterval = intervalLengths[6];
                break;
            case ciMonth:
                currentCandleInterval = intervalLengths[7];
                break;
            default: // 10 min
                currentCandleInterval = intervalLengths[1];
                break;
        }
        maxCurrentCandleInterval = currentCandleInterval + 0.1;

        if (prev) {
            HLOCV *x;
            CFAbsoluteTime *t;
            NSUInteger count = [prev getData:&x Times:&t];
            if (count) {
                packetCount = 1;
                packetHLOCs[0] = x[count - 1];
                packetTimes[0] = t[count - 1];
            }
        }
    }
    return self;
}

- (NSUInteger) getData:(HLOCV **)hlocs Times:(CFAbsoluteTime **) times {
    if (hlocs)
        *hlocs = packetHLOCs;
    if (times)
        *times = packetTimes;
    return packetCount;
}

- (void) incPacketCapacity {
    if (packetCapacity)
        packetCapacity *= 2;
    else
        packetCapacity = 16;
    CFAbsoluteTime *tempt = malloc(sizeof(CFAbsoluteTime) * packetCapacity);
    HLOCV *temph = malloc(sizeof(HLOCV) * packetCapacity);
    if (packetCount > 0) {
        memcpy(tempt, packetTimes, sizeof(CFAbsoluteTime) * packetCount);
        memcpy(temph, packetHLOCs, sizeof(HLOCV) * packetCount);
    }
    CFAbsoluteTime *tempT = packetTimes;
    packetTimes = tempt;
    free(tempT);
    HLOCV *tempH = packetHLOCs;
    packetHLOCs = temph;
    free(tempH);
}

- (BOOL) extendDataByOnePseudoHLOC:(CFAbsoluteTime) newTime GD:(CFGregorianDate) newGD {
    if (packetCount == 0) return NO;
    packetTimes[packetCount] = newTime;//packetTimes[packetCount - 1] + addT;
    packetHLOCs[packetCount].high =
    packetHLOCs[packetCount].low =
    packetHLOCs[packetCount].open =
    packetHLOCs[packetCount].close = packetHLOCs[packetCount - 1].close;
    packetHLOCs[packetCount].tag = hPseudo | [self changeMaskForTime:newTime GD:newGD];
    packetHLOCs[packetCount].value = -1.0;
    packetCount++;
    if (packetCapacity == packetCount) [self incPacketCapacity];
    return YES;
}

- (BOOL) dowChange:(CFAbsoluteTime) t {
    int dow = CFAbsoluteTimeGetDayOfWeek(t, defZone);
    if (dow < dayOfWeek) {
        dayOfWeek = dow;
        return YES;
    }
    return NO;
}

- (int) intradayDayChangeMaskForTime:(CFAbsoluteTime) t {
    if ([self dowChange:t])
        return hWeekChange | hDayChange | h4HourChange | hHOURChange | h30MinChange | h15MinChange | h10MinChange;
    return hDayChange | h4HourChange | hHOURChange | h30MinChange | h15MinChange | h10MinChange;
}

- (int) daylyChangeMaskForTime:(CFAbsoluteTime) t {
    if ([self dowChange:t])
        return hWeekChange | hDayChange;
    return hDayChange;
}

- (int) changeMaskForTime:(CFAbsoluteTime) t GD:(CFGregorianDate) gd{
    if (gd.year == feedingTime.year) {
        if (candleItemInterval > ciWeek) // ciMonth
            return hMonthChange | hDayChange;
        if (gd.month == feedingTime.month) {
            switch (candleItemInterval) {
                case ciMinute:
                case ci10Minutes:
                case ciHour:
                    if (gd.day == feedingTime.day) {
                        if (gd.hour == feedingTime.hour) {
                            if (gd.minute % 30) {
                                if (gd.minute % 15) {
                                    if (gd.minute % 10)
                                        return 0;
                                    return h10MinChange;
                                }
                                return h15MinChange;
                            }
                            return h30MinChange | h15MinChange | h10MinChange;
                        }
                        feedingTime.hour = gd.hour;
                        if (gd.hour % 4)
                            return hHOURChange | h30MinChange | h15MinChange | h10MinChange;
                        return h4HourChange | hHOURChange | h30MinChange | h15MinChange | h10MinChange;
                    }
                    feedingTime.day = gd.day;
                    feedingTime.hour = gd.hour;
                    return [self intradayDayChangeMaskForTime:t];
                case ciDay:  return [self daylyChangeMaskForTime:t];
                case ciWeek: return hWeekChange;
                default:
                    return 0;
            }
        } else { // new month
            feedingTime = gd;
            switch (candleItemInterval) {
                    //case ciMonth:
                case ciWeek: return hMonthChange | hWeekChange;
                case ciDay:  return hMonthChange | [self daylyChangeMaskForTime:t];
                default: // < ciDay
                    return hMonthChange | [self intradayDayChangeMaskForTime:t];
            }
        }
    } else { // new year
        feedingTime = gd;
        switch (candleItemInterval) {
            case ciMonth:    return hYearChange | hMonthChange;
            case ciWeek:     return hYearChange | hMonthChange | hWeekChange;
            case ciDay:      return hYearChange | hMonthChange | [self daylyChangeMaskForTime:t];
            default: // < ciDay
                return hYearChange | hMonthChange | [self intradayDayChangeMaskForTime:t];
        }
    }
}

- (void) extendDataByMultiplePseudoHLOCsAdding:(CFGregorianUnits) gdadd Till:(CFAbsoluteTime) t {
    CFAbsoluteTime gapTime = CFAbsoluteTimeAddGregorianUnits(packetTimes[packetCount - 1], defZone, gdadd);
    CFGregorianDate gapGD = CFAbsoluteTimeGetGregorianDate(gapTime, defZone);
    while ((t - gapTime > maxCurrentCandleInterval) && [self extendDataByOnePseudoHLOC:gapTime GD:gapGD]) {
        gapTime = CFAbsoluteTimeAddGregorianUnits(gapTime, defZone, gdadd);
        gapGD = CFAbsoluteTimeGetGregorianDate(gapTime, defZone);
    }
}

- (void) feedH:(float)h L:(float)l O:(float)o C:(float)c V:(float)v AtTime:(CFAbsoluteTime)t {
    if (packetCapacity == packetCount) [self incPacketCapacity];
    CFGregorianDate gd = CFAbsoluteTimeGetGregorianDate(t, defZone);
    if (packetCount > 0) {
        double delta = t - packetTimes[packetCount - 1];
        if (delta < 0) return;
        if (delta < currentCandleInterval - 0.1)
            packetCount--; // replace last candle
        else { // filling gaps if necessary
            if (delta > maxCurrentCandleInterval) { // looks like we have a gap
                switch (candleItemInterval) {
                    case ci10Minutes:
                    case ciHour: {
                        CFGregorianUnits gdadd = {0, 0, 1, 0, 0, 0};
                        CFAbsoluteTime nextDay = CFAbsoluteTimeAddGregorianUnits(packetTimes[packetCount - 1], defZone, gdadd);
                        CFGregorianDate nextDayGD = CFAbsoluteTimeGetGregorianDate(nextDay, defZone);
                        if ((nextDayGD.month <= gd.month) && (nextDayGD.day <= gd.day)) {
                            // skipping gap, since the new day starts
                        } else {
                            gdadd.days = 0;
                            if (candleItemInterval == ciHour)
                                gdadd.hours = 1;
                            else
                                gdadd.minutes = 10;
                            [self extendDataByMultiplePseudoHLOCsAdding:gdadd Till:t];
                        }
                        break;
                    }
                    case ciDay: {
                        CFGregorianUnits gdadd = {0, 0, 1, 0, 0, 0};
                        CFAbsoluteTime gapTime = CFAbsoluteTimeAddGregorianUnits(packetTimes[packetCount - 1], defZone, gdadd);
                        CFGregorianDate gapGD = CFAbsoluteTimeGetGregorianDate(gapTime, defZone);
                        if ((feedingTime.year == gd.year) && (feedingTime.month == gd.month)) {
                            if (gd.day > feedingTime.day + 1) { // skipping gap of more than two days
                            } else
                                [self extendDataByOnePseudoHLOC:gapTime GD:gapGD];
                        } else {
                            if ((gd.year == gapGD.year) && // next year or same year
                                (gd.month == gapGD.month) && // next month
                                (gd.day == gapGD.day + 1))
                                [self extendDataByOnePseudoHLOC:gapTime GD:gapGD];
                        }
                        break;
                    }
                    case ciWeek: {
                        CFGregorianUnits gdadd = {0, 0, 7, 0, 0, 0};
                        [self extendDataByMultiplePseudoHLOCsAdding:gdadd Till:t];
                        break;
                    }
                    case ciMonth: {
                        CFGregorianUnits gdadd = {0, 1, 0, 0, 0, 0};
                        [self extendDataByMultiplePseudoHLOCsAdding:gdadd Till:t];
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
    packetTimes[packetCount] = t;
    packetHLOCs[packetCount].high = h;
    packetHLOCs[packetCount].low = l;
    packetHLOCs[packetCount].open = o;
    packetHLOCs[packetCount].close = c;
    packetHLOCs[packetCount].value = v;
    packetHLOCs[packetCount].tag = hNormal | [self changeMaskForTime:t GD:gd];
    packetCount++;
}

- (FeedingResult) appendSkeletonData:(id<HLOCDataProvider>)nextPacket {
    HLOCV *x;
    CFAbsoluteTime *t;
    NSUInteger nextCount = [nextPacket getData:&x Times:&t];
    if (nextCount > 0) {
        if (packetCount > 0) {
            NSUInteger newCount = nextCount + packetCount - 1;
            while (newCount > packetCapacity)
                [self incPacketCapacity];
            memcpy(&(packetTimes[packetCount - 1]), t, sizeof(CFAbsoluteTime)*nextCount);
            memcpy(&(packetHLOCs[packetCount - 1]), x, sizeof(HLOCV)*nextCount);
            if (packetCount == newCount)
                return frLastCandleUpdateOnly;
            packetCount = newCount;
            return frNewCandlesAdded;
        } else {
            while (nextCount > packetCapacity)
                [self incPacketCapacity];
            memcpy(packetTimes, t, sizeof(CFAbsoluteTime)*nextCount);
            memcpy(packetHLOCs, x, sizeof(HLOCV)*nextCount);
            packetCount = nextCount;
            return frCompletelyNewData;
        }
    } else
        return frBad;
}

- (void) insertPseudoHLOCAtIndex:(NSUInteger)index WithTime:(CFAbsoluteTime)t Tag:(int) tag {
    if (index) {
        if (packetCount == packetCapacity)
            [self incPacketCapacity];
        if (index < packetCount) {
            memcpy(&(packetTimes[index + 1]), &(packetTimes[index]), (packetCount - index)*sizeof(CFAbsoluteTime));
            memcpy(&(packetHLOCs[index + 1]), &(packetHLOCs[index]), (packetCount - index)*sizeof(HLOCV));
        }
        packetTimes[index] = t;
        packetHLOCs[index].high =
        packetHLOCs[index].low =
        packetHLOCs[index].open =
        packetHLOCs[index].close = packetHLOCs[index - 1].close;
        packetHLOCs[index].tag = hPseudo | tag;
        packetHLOCs[index].value = -1.0;
        packetCount++;
    }
}

- (MergeResult) tryMerge:(CSSubContainer *)sub InsertionCount:(NSUInteger *)insCount { // returns TRUE if fully merged
    *insCount = 0;
    if (packetCount == 0) return mrGenericBad;
    HLOCV *x;
    CFAbsoluteTime *t;
    NSUInteger count = [sub getData:&x Times:&t];
    if (count == 0) return mrGenericBad;
    int myIndex = 0;
    int myTranslationIndex = 0;
    int subIndex = 0;
    int subTranslationIndex = 0;
    CFTimeInterval dt = t[0] - packetTimes[0];
    CFTimeInterval dt1;
    if (dt < -EPSILON) { // started before my data
        dt1 = t[count - 1] - packetTimes[0];
        if (dt1 < -EPSILON)
            return mrCantMergeYetB;
        for (; subTranslationIndex < count; subTranslationIndex++) {
            dt1 = t[subTranslationIndex] - packetTimes[0];
            if (dt1 > -EPSILON) {
                if (dt1 < EPSILON) // equals!
                    break;
                else { // bigger than my first time
                    [sub insertPseudoHLOCAtIndex:subTranslationIndex WithTime:packetTimes[0] Tag:packetHLOCs[0].tag];
                    //(*insCount)++;
                    count = [sub getData:&x Times:&t];
                }
                break;
            }
        }
    } else {
        if (dt > EPSILON) { // started after my data start
            dt1 = t[0] - packetTimes[packetCount - 1];
            if (dt1 > -EPSILON) // started after my data ends
                return mrCantMergeYetA;
            for (; myTranslationIndex < packetCount; myTranslationIndex++) {
                dt1 = t[0] - packetTimes[myTranslationIndex];
                if (dt1 < EPSILON) // equals
                    break;
            }
        }
    }
    while (true) {
        int i = myIndex + myTranslationIndex;
        int j = subIndex + subTranslationIndex;
        if (i >= packetCount) {
            if (j >= count)
                return mrIdeal;
            return mrMergeOverlappedPost;
        } else
            if (j >= count)
                return mrMergeOverlappedPre;
        dt = t[j] - packetTimes[i];
        // In following comments 'x' = real data, ' ' = missing data, '.' = pseudo data inserted for merging
        if (dt < -EPSILON) {
            //  my: xx x? -> xx.x?
            // sub: xxx?? -> xxx??
            [self insertPseudoHLOCAtIndex:i WithTime:t[j] Tag:x[j].tag];
            (*insCount)++;
        } else {
            if (dt < EPSILON) { // equals
                myIndex++;
                subIndex++;
            } else {
                //  my: xxx?? -> xxx??
                // sub: xx x? -> xx.x?
                [sub insertPseudoHLOCAtIndex:j WithTime:packetTimes[i] Tag:packetHLOCs[i].tag];
                //(*insCount)++;
                count = [sub getData:&x Times:&t];
            }
        }
    }

}

@end
