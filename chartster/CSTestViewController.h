//
//  CSTestViewController.h
//  chartster
//
//  Created by Itheme on 4/2/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSView.h"

@interface CSTestViewController : UIViewController
@property (strong, nonatomic) IBOutlet CSView *chart;

- (IBAction)loadFirst:(id)sender;
- (IBAction)loadUpdate:(id)sender;
@end
