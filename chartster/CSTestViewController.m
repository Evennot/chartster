//
//  CSTestViewController.m
//  chartster
//
//  Created by Itheme on 4/2/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSTestViewController.h"

@interface CSTestViewController ()

@end

@implementation CSTestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.chart.decimals = 2;
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    //[self.chart present];
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

/*- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;*/
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.chart realign];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id) jsonWithName:(NSString *)name {
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"json"];
    if (path == nil) return nil;
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSError *error = nil;
    if (error == nil)
        return [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    return nil;
}

- (IBAction)loadFirst:(id)sender {
    self.chart.candleItemInterval = ci10Minutes;
    for (int i = 0; true; i+=5) {
        id json = [self jsonWithName:[NSString stringWithFormat:@"c%d00", i]];
        if (json) {
            if (i == 0)
                [self.chart feedFirstData:json];
            else
                [self.chart feedUpdate:json];		
        } else
            break;
    }
}

- (IBAction)loadUpdate:(id)sender {
//    [self.chart feedUpdate:[self jsonWithName:@"update"]];
}
@end
