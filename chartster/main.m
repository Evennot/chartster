//
//  main.m
//  chartster
//
//  Created by Itheme on 4/2/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CSAppDelegate class]));
    }
}
